using Kinetic, Plots, LinearAlgebra, JLD2
using ProgressMeter: @showprogress

include("func_new.jl")

cd(@__DIR__)
D = Dict{Symbol,Any}()
begin
    D[:matter] = "gas"
    D[:case] = "cavity"
    D[:space] = "2d1f2v"
    D[:flux] = "kfvs"
    D[:collision] = "bgk"
    D[:nSpecies] = 1
    D[:interpOrder] = 1 #2
    D[:limiter] = "vanleer"
    D[:boundary] = "maxwell"
    D[:cfl] = 0.5
    D[:maxTime] = 1.0

    D[:x0] = -1.5
    D[:x1] = 1.5
    D[:nx] = 40
    D[:y0] = -1.5
    D[:y1] = 1.5
    D[:ny] = 40
    D[:pMeshType] = "uniform"
    D[:nxg] = 0
    D[:nyg] = 0

    D[:umin] = -5.0
    D[:umax] = 5.0
    D[:nu] = 24
    D[:vmin] = -5.0
    D[:vmax] = 5.0
    D[:nv] = 24
    D[:vMeshType] = "rectangle"
    D[:nug] = 0
    D[:nvg] = 0

    D[:knudsen] = 0.5
    D[:mach] = 0.0
    D[:prandtl] = 1.0
    D[:inK] = 0.0
    D[:omega] = 0.81
    D[:alphaRef] = 1.0
    D[:omegaRef] = 0.5

    D[:uLid] = 0.15
    D[:vLid] = 0.0
    D[:tLid] = 1.0
end

ks = SolverSet(D)
# parts of ctr, a1face and a2face are replaced later with vectors which don't have the same structure
ctr, a1face, a2face = init_fvm(ks, ks.pSpace) #(ks, ks.ps, :dynamic_array; structarray = true)

s2 = 0.03^2
s1 = 0.1#4π * s2
flr = 0.0001
init_density(x, y) = max(flr, 1.0 / s1 * exp(-(x^2 + y^2) / 4.0 / s2))

#new
# s2 quadrature points
using PyCall
@pyimport numpy as np
@pyimport quadpy
scheme = quadpy.s2.get_good_scheme(9) 
u_points = scheme.points[1,:] * 5
v_points = scheme.points[2,:] * 5
scheme_weights = scheme.weights
#new end

#new
# necessary new matrices, vectors for quadrature points
contr = zeros(ks.pSpace.nx, ks.pSpace.ny, 19)
a1fc_fw = zeros(ks.pSpace.nx+1, ks.pSpace.ny, 4)
a1fc_ff = zeros(ks.pSpace.nx+1, ks.pSpace.ny, 19)
a1fc_len = zeros(ks.pSpace.nx+1, ks.pSpace.ny)
a2fc_fw = zeros(ks.pSpace.nx, ks.pSpace.ny+1, 4)
a2fc_ff = zeros(ks.pSpace.nx, ks.pSpace.ny+1, 19)
a2fc_len = zeros(ks.pSpace.nx, ks.pSpace.ny+1)
#new end

function init_field!(ks, ctr, a1face, a2face)
    for i = 1:ks.pSpace.nx, j = 1:ks.pSpace.ny #i = 1:ks.ps.nx, j = 1:ks.ps.ny
        rho0 = init_density(ks.pSpace.x[i, j], ks.pSpace.y[i, j]) #(ks.ps.x[i, j], ks.ps.y[i, j])

        ctr[i, j].prim .= [rho0, 0.0, 0.0, 1.0]
        ctr[i, j].w .= prim_conserve(ctr[i, j].prim, ks.gas.γ)
        ctr[i, j].f .= maxwellian1(ks.vSpace.u, ks.vSpace.v, ctr[i, j].prim) #(ks.vs.u, ks.vs.v, ctr[i, j].prim)
        #new
        for k = 1:19
            contr[i, j, k] = maxwellian1(u_points[k], v_points[k], ctr[i, j].prim) # new definition in function maxwellian
        end
        #new end
    end
    for i in eachindex(a1face)
        a1face[i].fw .= 0.0
        a1face[i].ff .= 0.0
    end
    for i in eachindex(a2face)
        a2face[i].fw .= 0.0
        a2face[i].ff .= 0.0
    end
    #new
    for i = 1:ks.pSpace.nx+1, j = 1:ks.pSpace.ny
        a1fc_fw[i, j, :] .= 0.0
        a1fc_ff[i, j, :] .= 0.0
        a1fc_len[i, j] = (1.5 - (-1.5)) / 40
    end
    for i = 1:ks.pSpace.nx, j = 1:ks.pSpace.ny+1
        a2fc_fw[i, j, :] .= 0.0
        a2fc_ff[i, j, :] .= 0.0
        a2fc_len[i, j] = (1.5 - (-1.5)) / 40
    end
    #new end
end

init_field!(ks, ctr, a1face, a2face)

t = 0.0
dt = timestep(ks, ctr, t)
nt = Int(ks.set.maxTime ÷ dt) + 1
res = zero(ks.ib.wL)

@showprogress for iter = 1:20#nt
    reconstruct!(ks, ctr) # apparently, this function does NOT need to be modified ?
    #evolve!(ks, ctr, a1face, a2face, dt; mode = Symbol(ks.set.flux), bc = Symbol(ks.set.boundary))
    
    # horizontal flux
    @inbounds Threads.@threads for j = 1:ks.pSpace.ny
        for i = 2:ks.pSpace.nx
            #new
            x1 = @view a1fc_fw[i, j, :]
            x2 = @view a1fc_ff[i, j, :]
            x3 = @view contr[i-1, j, :]
            x4 = @view contr[i, j, :]
            #new end
            flux_kfvs1!(          # new definition in function flux_kfvs in 1F2V
                #a1face[i, j].fw,
                #a1face[i, j].ff,
                #new
                x1,
                x2,
                #new end
                #ctr[i-1, j].f,
                #ctr[i, j].f,
                #new
                x3,
                x4,
                #new end
                #ks.vSpace.u,
                #ks.vSpace.v,
                #ks.vSpace.weights,
                #new
                u_points,
                v_points,
                scheme_weights,
                #new end
                dt,
                #a1face[i, j].len,
                #new
                a1fc_len[i, j],
                #new end
            )
        end
    end
    
    # vertical flux
    vn = ks.vSpace.v
    vt = -ks.vSpace.u
    @inbounds Threads.@threads for j = 2:ks.pSpace.ny
        for i = 1:ks.pSpace.nx #do they need to be changed?
            wL = KitBase.local_frame(ctr[i, j-1].w, 0., 1.)
            wR = KitBase.local_frame(ctr[i, j].w, 0., 1.)
            swL = KitBase.local_frame(ctr[i, j-1].sw[:, 2], 0., 1.)
            swR = KitBase.local_frame(ctr[i, j].sw[:, 2], 0., 1.)
            #new
            x1 = @view a2fc_fw[i, j, :]
            x2 = @view a2fc_ff[i, j, :]
            x3 = @view contr[i, j-1, :]
            x4 = @view contr[i, j, :]
            x5 = @view a2fc_fw[i, j, :]
            #new end

            #=KitBase.flux_kfvs!(
                a2face[i, j].fw,
                a2face[i, j].ff,
                ctr[i, j-1].f,
                ctr[i, j].f,
                vn,
                vt,
                ks.vSpace.weights,
                dt,
                a2face[i, j].len,
            )=#
            #new
            flux_kfvs1!(   # function is already changed
                x1,
                x2,
                x3,
                x4,
                v_points,
                -u_points,
                scheme_weights,
                dt,
                a2fc_len[i, j],
            )
            #new end
            #a2face[i, j].fw .= KitBase.global_frame(a2face[i, j].fw, 0., 1.)
            #new #needs to be looked at
            a2fc_fw[i, j, :] .= global_frame1(a2fc_fw[i, j, :], 0., 1.)  # new definition in function global_frame
            #new end
        end
    end
    
    # boundary flux
    @inbounds Threads.@threads for j = 1:ks.pSpace.ny
        #new
        x1 = @view a1fc_fw[1, j, :]
        x2 = @view a1fc_ff[1, j, :]
        x3 = @view contr[1, j, :]
        #new end
        #KitBase.flux_boundary_maxwell!(
        #new
        flux_boundary_maxwell1!(    # new definition in function flux_boundary_maxwell!
        #new end
            #a1face[1, j].fw,
            #a1face[1, j].ff,
            #new
            x1,
            x2,
            #new end
            ks.ib.bcL,  # ???
            #ctr[1, j].f,
            #new
            x3,
            #new end
            #ks.vSpace.u,
            #ks.vSpace.v,
            #ks.vSpace.weights,
            #new
            u_points,
            v_points,
            scheme_weights,
            #new end
            dt,
            ctr[1, j].dy,
            1.,
        )

        #new
        x4 = @view a1fc_fw[ks.pSpace.nx+1, j, :]
        x5 = @view a1fc_ff[ks.pSpace.nx+1, j, :]
        x6 = @view contr[ks.pSpace.nx, j, :]
        #new end
        #KitBase.flux_boundary_maxwell!(
        #new
        flux_boundary_maxwell1!(    # new definition in function flux_boundary_maxwell!
        #new end
            #a1face[ks.pSpace.nx+1, j].fw,
            #a1face[ks.pSpace.nx+1, j].ff,
            #new
            x4,
            x5,
            #new end
            ks.ib.bcL,
            #ctr[ks.pSpace.nx, j].f,
            #new
            x6,
            #new end
            #ks.vSpace.u,
            #ks.vSpace.v,
            #ks.vSpace.weights,
            #new
            u_points,
            v_points,
            scheme_weights,
            #new end
            dt,
            ctr[ks.pSpace.nx, j].dy,
            -1.,
        )
    end
    
    @inbounds Threads.@threads for i = 1:ks.pSpace.nx
        #new
        x7 = @view a2fc_fw[i, 1, :]
        x8 = @view a2fc_ff[i, 1, :]
        x9 = @view contr[i, 1, :]
        #new end
        #KitBase.flux_boundary_maxwell!(
        #new
        flux_boundary_maxwell1!(    # new definition in function flux_boundary_maxwell!
        #new end
            #a2face[i, 1].fw,
            #a2face[i, 1].ff,
            #new
            x7,
            x8,
            #new end
            ks.ib.bcL,
            #ctr[i, 1].f,
            #new
            x9,
            #new end
            #vn,
            #vt,
            #ks.vSpace.weights,
            #new
            v_points,
            -u_points,
            scheme_weights,
            #new end
            dt,
            ctr[i, 1].dx,
            1,
        )
        #a2face[i, 1].fw .= KitBase.global_frame(a2face[i, 1].fw, 0., 1.)
        #new #needs to be looked at
        a2fc_fw[i, 1, :] .= global_frame1(a2fc_fw[i, 1, :], 0., 1.)  # new definition in function global_frame
        #new end
        
        #new
        x10 = @view a2fc_fw[i, ks.pSpace.ny+1, :]
        x11 = @view a2fc_ff[i, ks.pSpace.ny+1, :]
        x12 = @view contr[i, ks.pSpace.ny, :]
        #new end
        #KitBase.flux_boundary_maxwell!(
        #new
        flux_boundary_maxwell1!(    # new definition in function flux_boundary_maxwell!
        #new end
            #a2face[i, ks.pSpace.ny+1].fw,
            #a2face[i, ks.pSpace.ny+1].ff,
            #new
            x10,
            x11,
            #new end
            ks.ib.bcL,
            #ctr[i, ks.pSpace.ny].f,
            #new
            x12,
            #new end
            #vn,
            #vt,
            #ks.vSpace.weights,
            #new
            v_points,
            u_points,
            scheme_weights,
            #new end
            dt,
            ctr[i, ks.pSpace.ny].dy,
            -1,
        )
        #a2face[i, ks.pSpace.ny+1].fw .= KitBase.global_frame(
        #    a2face[i, ks.pSpace.ny+1].fw,
        #    0.,
        #    1.,
        #)
        #new #needs to be looked at
        a2fc_fw[i, ks.pSpace.ny+1, :] .= global_frame1(a2fc_fw[i, ks.pSpace.ny+1, :], 0., 1.)  # new definition in function global_frame
        #new end
    end

    #update!(ks, ctr, a1face, a2face, dt, res; coll = Symbol(ks.set.collision), bc = Symbol(ks.set.boundary))
    #new
    update1!(ks, u_points, v_points, scheme_weights, ctr, contr, a1face, a2face, a1fc_fw, a1fc_ff, a2fc_fw, a2fc_ff, dt, res; coll = Symbol(ks.set.collision), bc = Symbol(ks.set.boundary))
    #new end

    global t += dt #no global
end

plot_contour(ks, ctr)
