using Kinetic, Plots, LinearAlgebra
using ProgressMeter: @showprogress

cd(@__DIR__)
include("func_new.jl")

D = Dict{Symbol,Any}()
begin
    D[:matter] = "gas"
    D[:case] = "cavity"
    D[:space] = "2d1f2v"
    D[:flux] = "kfvs"
    D[:collision] = "bgk"
    D[:nSpecies] = 1
    D[:interpOrder] = 1 #2
    D[:limiter] = "vanleer"
    D[:boundary] = "maxwell"
    D[:cfl] = 0.5
    D[:maxTime] = 1.0

    D[:x0] = -1.5
    D[:x1] = 1.5
    D[:nx] = 40
    D[:y0] = -1.5
    D[:y1] = 1.5
    D[:ny] = 40
    D[:pMeshType] = "uniform"
    D[:nxg] = 0
    D[:nyg] = 0

    D[:umin] = -5.0
    D[:umax] = 5.0
    D[:nu] = 24
    D[:vmin] = -5.0
    D[:vmax] = 5.0
    D[:nv] = 24
    D[:vMeshType] = "rectangle"
    D[:nug] = 0
    D[:nvg] = 0

    D[:knudsen] = 0.5
    D[:mach] = 0.0
    D[:prandtl] = 1.0
    D[:inK] = 0.0
    D[:omega] = 0.81
    D[:alphaRef] = 1.0
    D[:omegaRef] = 0.5

    D[:uLid] = 0.15
    D[:vLid] = 0.0
    D[:tLid] = 1.0
end

ks = SolverSet(D)
# parts of ctr, a1face and a2face are replaced later with vectors which don't have the same structure
ctr, a1face, a2face = init_fvm(ks, ks.pSpace) #(ks, ks.ps, :dynamic_array; structarray = true)

s2 = 0.03^2
s1 = 0.1#4π * s2
flr = 0.0001
init_density(x, y) = max(flr, 1.0 / s1 * exp(-(x^2 + y^2) / 4.0 / s2))

#new
# s2 quadrature points
using PyCall
@pyimport numpy as np
@pyimport quadpy
scheme = quadpy.s2.get_good_scheme(18)
u_points = scheme.points[1,:] * 5
v_points = scheme.points[2,:] * 5
scheme_weights = scheme.weights * (π * 5^2)
#new end



# let's do a test
prim = [init_density(0, 0), 0.0, 0.0, 1.0]
w = prim_conserve(prim, ks.gas.γ)
f = maxwellian1(u_points, v_points, prim)

w1 = [discrete_moments(f, u_points, scheme_weights, 0),
    discrete_moments(f, u_points, scheme_weights, 1),
    discrete_moments(f, v_points, scheme_weights, 1),
    (discrete_moments(f, u_points, scheme_weights, 2) + 
    discrete_moments(f, v_points, scheme_weights, 2)) / 2]

w
w1

#new
# necessary new matrices, vectors for quadrature points
contr = zeros(ks.pSpace.nx, ks.pSpace.ny, length(u_points))
a1fc_fw = zeros(ks.pSpace.nx+1, ks.pSpace.ny, 4)
a1fc_ff = zeros(ks.pSpace.nx+1, ks.pSpace.ny, length(u_points))
a1fc_len = zeros(ks.pSpace.nx+1, ks.pSpace.ny)
a2fc_fw = zeros(ks.pSpace.nx, ks.pSpace.ny+1, 4)
a2fc_ff = zeros(ks.pSpace.nx, ks.pSpace.ny+1, length(u_points))
a2fc_len = zeros(ks.pSpace.nx, ks.pSpace.ny+1)
#new end

function init_field!(ks, ctr, a1face, a2face)
    for i = 1:ks.pSpace.nx, j = 1:ks.pSpace.ny #i = 1:ks.ps.nx, j = 1:ks.ps.ny
        rho0 = init_density(ks.pSpace.x[i, j], ks.pSpace.y[i, j]) #(ks.ps.x[i, j], ks.ps.y[i, j])

        ctr[i, j].prim .= [rho0, 0.0, 0.0, 1.0]
        ctr[i, j].w .= prim_conserve(ctr[i, j].prim, ks.gas.γ)
        ctr[i, j].f .= maxwellian1(ks.vSpace.u, ks.vSpace.v, ctr[i, j].prim) #(ks.vs.u, ks.vs.v, ctr[i, j].prim)
        #new
        for k in axes(contr, 3)
            contr[i, j, k] = maxwellian1(u_points[k], v_points[k], ctr[i, j].prim) # new definition in function maxwellian
        end
        #new end
    end
    for i in eachindex(a1face)
        a1face[i].fw .= 0.0
        a1face[i].ff .= 0.0
    end
    for i in eachindex(a2face)
        a2face[i].fw .= 0.0
        a2face[i].ff .= 0.0
    end
    #new
    for i = 1:ks.pSpace.nx+1, j = 1:ks.pSpace.ny
        a1fc_fw[i, j, :] .= 0.0
        a1fc_ff[i, j, :] .= 0.0
        a1fc_len[i, j] = (1.5 - (-1.5)) / 40
    end
    for i = 1:ks.pSpace.nx, j = 1:ks.pSpace.ny+1
        a2fc_fw[i, j, :] .= 0.0
        a2fc_ff[i, j, :] .= 0.0
        a2fc_len[i, j] = (1.5 - (-1.5)) / 40
    end
    #new end
end

init_field!(ks, ctr, a1face, a2face)

t = 0.0
dt = timestep(ks, ctr, t) * 0.2
nt = Int(ks.set.maxTime ÷ dt) + 1
res = zero(ks.ib.wL)

@showprogress for iter = 1:1#nt
    #reconstruct!(ks, ctr) # apparently, this function does NOT need to be modified ?
    #evolve!(ks, ctr, a1face, a2face, dt; mode = Symbol(ks.set.flux), bc = Symbol(ks.set.boundary))
    
    # horizontal flux
    @inbounds Threads.@threads for j = 1:ks.pSpace.ny
        for i = 2:ks.pSpace.nx
            x1 = @view a1fc_fw[i, j, :]
            x2 = @view a1fc_ff[i, j, :]
            x3 = @view contr[i-1, j, :]
            x4 = @view contr[i, j, :]

            flux_kfvs1!(          # new definition in function flux_kfvs in 1F2V
                x1,
                x2,
                x3,
                x4,
                u_points,
                v_points,
                scheme_weights,
                dt,
                a1fc_len[i, j],
            )
        end
    end
    
    # vertical flux
    vn = ks.vSpace.v
    vt = -ks.vSpace.u
    @inbounds Threads.@threads for j = 2:ks.pSpace.ny
        for i = 1:ks.pSpace.nx #do they need to be changed?
            wL = KitBase.local_frame(ctr[i, j-1].w, 0., 1.)
            wR = KitBase.local_frame(ctr[i, j].w, 0., 1.)
            swL = KitBase.local_frame(ctr[i, j-1].sw[:, 2], 0., 1.)
            swR = KitBase.local_frame(ctr[i, j].sw[:, 2], 0., 1.)

            x1 = @view a2fc_fw[i, j, :]
            x2 = @view a2fc_ff[i, j, :]
            x3 = @view contr[i, j-1, :]
            x4 = @view contr[i, j, :]
            x5 = @view a2fc_fw[i, j, :]

            flux_kfvs1!(   # function is already changed
                x1,
                x2,
                x3,
                x4,
                v_points,
                -u_points,
                scheme_weights,
                dt,
                a2fc_len[i, j],
            )

            a2fc_fw[i, j, :] .= global_frame1(a2fc_fw[i, j, :], 0., 1.)  # new definition in function global_frame
        end
    end
    
    update1!(ks, u_points, v_points, scheme_weights, ctr, contr, a1face, a2face, a1fc_fw, a1fc_ff, a2fc_fw, a2fc_ff, dt, res; coll = Symbol(ks.set.collision), bc = Symbol(ks.set.boundary))

    global t += dt #no global
end

plot_contour(ks, ctr)
