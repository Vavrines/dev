#--------------- all the new function definitions ---------------#
#KitBase\GHBg7\src\Theory\theory_maxwellian.jl
maxwellian1(u, v, ρ, U, V, λ) = @. ρ * (λ / π) * exp(-λ * ((u - U)^2 + (v - V)^2))

maxwellian1(u, v, prim::AbstractVector{T}) where {T<:Real} =
    maxwellian1(u, v, prim[1], prim[2], prim[3], prim[end])

#KitBase\GHBg7\src\Flux\flux_kfvs.jl
function flux_kfvs1!(
    fw,
    ff,
    fL,
    fR,
    u,
    v,
    ω,
    dt,
    len,
    sfL = zero(fL),
    sfR = zero(fR),
)

    # --- upwind reconstruction ---#
    δ = heaviside.(u)

    f = @. fL * δ + fR * (1.0 - δ)
    sf = @. sfL * δ + sfR * (1.0 - δ)

    # --- calculate fluxes ---#
    fw[1] = dt * sum(ω .* u .* f) - 0.5 * dt^2 * sum(ω .* u .^ 2 .* sf)
    fw[2] = dt * sum(ω .* u .^ 2 .* f) - 0.5 * dt^2 * sum(ω .* u .^ 3 .* sf)
    fw[3] = dt * sum(ω .* v .* u .* f) - 0.5 * dt^2 * sum(ω .* v .* u .^ 2 .* sf)
    fw[4] =
        dt * 0.5 * sum(ω .* u .* (u .^ 2 .+ v .^ 2) .* f) -
        0.5 * dt^2 * 0.5 * sum(ω .* u .^ 2 .* (u .^ 2 .+ v .^ 2) .* sf)
    fw .*= len

    @. ff = (dt * u * f - 0.5 * dt^2 * u^2 * sf) * len

    return nothing

end

#KitBase\GHBg7\src\Geometry\geo_general.jl
function global_frame1(w, cosa, sina)

    if eltype(w) <: Int
        G = similar(w, Float64)
    else
        G = similar(w)
    end

    if length(w) == 2
        G[1] = w[1] * cosa - w[2] * sina
        G[2] = w[1] * sina + w[2] * cosa
    elseif length(w) == 4
        G[1] = w[1]
        G[2] = w[2] * cosa - w[3] * sina
        G[3] = w[2] * sina + w[3] * cosa
        G[4] = w[4]
    else
        throw("local -> global: dimension dismatch")
    end

    return G

end

#KitBase\GHBg7\src\Flux\flux_boundary.jl
# 2D1F2V
function flux_boundary_maxwell1!(
    fw,
    fh,
    bc,
    h,
    u,
    v,
    ω,
    dt,
    len,
    rot = 1,
)

    @assert length(bc) == 4

    δ = heaviside.(u .* rot)

    SF = sum(ω .* u .* h .* (1.0 .- δ))
    SG =
        (bc[end] / π) *
        sum(ω .* u .* exp.(-bc[end] .* ((u .- bc[2]) .^ 2 .+ (v .- bc[3]) .^ 2)) .* δ)
    prim = [-SF / SG; bc[2:end]]

    H = maxwellian1(u, v, prim)

    hWall = H .* δ .+ h .* (1.0 .- δ)

    fw[1] = discrete_moments(hWall, u, ω, 1) * len * dt
    fw[2] = discrete_moments(hWall, u, ω, 2) * len * dt
    fw[3] = discrete_moments(hWall .* u, v, ω, 1) * len * dt
    fw[4] = 0.5 * discrete_moments(hWall .* (u .^ 2 .+ v .^ 2), u, ω, 1) * len * dt

    @. fh = u * hWall * len * dt

    return nothing
end

#KitBase\GHBg7\src\Solver\solver_update.jl
# 2D1F
function update1!(
    KS,
    #new
    u,
    v,
    weight,
    #new end
    ctr,
    #new
    contr,
    #new end
    a1face,
    a2face,
    #new
    a1fc_fw,
    a1fc_ff,
    a2fc_fw,
    a2fc_ff,
    #new end
    dt,
    residual;
    coll = :bgk::Symbol,
    bc = :fix::Symbol,
) 

    sumRes = zero(KS.ib.wL)
    sumAvg = zero(KS.ib.wL)

    if ndims(sumRes) == 1

        @inbounds for j = 2:KS.pSpace.ny-1
            for i = 2:KS.pSpace.nx-1
                #new
                x1 = @view contr[i, j, :]
                x2 = @view a1fc_fw[i, j, :]
                x3 = @view a1fc_ff[i, j, :]
                x4 = @view a1fc_fw[i+1, j, :]
                x5 = @view a1fc_ff[i+1, j, :]
                x6 = @view a2fc_fw[i, j]
                x7 = @view a2fc_ff[i, j, :]
                x8 = @view a2fc_fw[i, j+1, :]
                x9 = @view a2fc_ff[i, j+1, :]
                #new end
                step1!(
                    ctr[i, j].w,
                    ctr[i, j].prim,
                    #ctr[i, j].f,
                    #new
                    x1,
                    #new end
                    #a1face[i, j].fw,
                    #a1face[i, j].ff,
                    #a1face[i+1, j].fw,
                    #a1face[i+1, j].ff,
                    #new
                    x2,
                    x3,
                    x4,
                    x5,
                    #new end
                    #a2face[i, j].fw,
                    #a2face[i, j].ff,
                    #a2face[i, j+1].fw,
                    #a2face[i, j+1].ff,
                    #new
                    x6,
                    x7,
                    x8,
                    x9,
                    #new end
                    #KS.vSpace.u,
                    #KS.vSpace.v,
                    #KS.vSpace.weights,
                    #new
                    u,
                    v,
                    weight,
                    #new end
                    KS.gas.γ,
                    KS.gas.μᵣ,
                    KS.gas.ω,
                    KS.gas.Pr,
                    ctr[i, j].dx * ctr[i, j].dy,
                    dt,
                    sumRes,
                    sumAvg,
                    coll,
                )
            end
        end

    end

    for i in eachindex(residual)
        residual[i] = sqrt(sumRes[i] * KS.pSpace.nx * KS.pSpace.ny) / (sumAvg[i] + 1.e-7)
    end

    update_boundary1!(KS, #=new=# u, v, weight, #=new end=# ctr, #=new=# contr, #=new end=# a1face, a2face, #=new=# a1fc_fw, a1fc_ff, a2fc_fw, a2fc_ff, #=new end=# dt, residual; coll = coll, bc = bc)

    return nothing

end

#KitBase\GHBg7\src\Solver\solver_step.jl
# 2D1F2V
function step1!(
    w,
    prim,
    h,
    fwL,
    fhL,
    fwR,
    fhR,
    fwD,
    fhD,
    fwU,
    fhU,
    u,
    v,
    weights,
    γ,
    μᵣ,
    ω,
    Pr,
    Δs,
    dt,
    RES,
    AVG,
    collision = :bgk,
)

    #--- store W^n and calculate shakhov term ---#
    w_old = deepcopy(w)

    if collision == :shakhov
        q = heat_flux1(h, b, prim, u, v, weights)

        MH_old = maxwellian1(u, v, prim)
        SH = shakhov1(u, v, MH_old, q, prim, Pr)
    else
        SH = zero(h)
    end

    #--- update W^{n+1} ---#
    @. w += (fwL - fwR + fwD - fwU) / Δs
    prim .= conserve_prim(w, γ)

    #--- record residuals ---#
    @. RES += (w - w_old)^2
    @. AVG += abs(w)

    #--- calculate M^{n+1} and tau^{n+1} ---#
    MH = maxwellian1(u, v, prim)
    MH .+= SH
    τ = vhs_collision_time(prim, μᵣ, ω)

    #--- update distribution function ---#
    #for j in axes(v, 2), i in axes(u, 1)
    #    h[i, j] =
    #        (
    #            h[i, j] +
    #            (fhL[i, j] - fhR[i, j] + fhD[i, j] - fhU[i, j]) / Δs +
    #            dt / τ * MH[i, j]
    #        ) / (1.0 + dt / τ)
    #end

    #new
    for i in axes(u, 1)
        h[i] =
            (
                h[i] +
                (fhL[i] - fhR[i] + fhD[i] - fhU[i]) / Δs +
                dt / τ * MH[i]
            ) / (1.0 + dt / τ)
    end
    #new end


end

#KitBase\GHBg7\src\Theory\theory_moments_pure.jl
# 2F2V
function heat_flux1(
    h,
    b,
    prim,
    u,
    v,
    ω,
)

    q = similar(h, 2)

    q[1] =
        0.5 * (
            sum(@. ω * (u - prim[2]) * ((u - prim[2])^2 + (v - prim[3])^2) * h) +
            sum(@. ω * (u - prim[2]) * b)
        )
    q[2] =
        0.5 * (
            sum(@. ω * (v - prim[3]) * ((u - prim[2])^2 + (v - prim[3])^2) * h) +
            sum(@. ω * (v - prim[3]) * b)
        )

    return q

end

#KitBase\GHBg7\src\Theory\theory_shakhov.jl
# 1F2V 
function shakhov1(
    u,
    v,
    M,
    q,
    prim,
    Pr,
)

    M_plus = @. 0.8 * (1.0 - Pr) * prim[end]^2 / prim[1] *
       ((u - prim[2]) * q[1] + (v - prim[3]) * q[2]) *
       (2.0 * prim[end] * ((u - prim[2])^2 + (v - prim[3])^2) - 5.0) *
       M

    return M_plus

end

#KitBase\GHBg7\src\Solver\solver_boundary.jl
# 2D1F
function update_boundary1!(
    KS,
    #new
    u,
    v,
    weight,
    #new end
    ctr,
    contr,
    a1face,
    a2face,
    #new
    a1fc_fw,
    a1fc_ff,
    a2fc_fw,
    a2fc_ff,
    #new end
    dt,
    residual;
    coll::Symbol,
    bc::Symbol,
)

    resL = zero(KS.ib.wL)
    avgL = zero(KS.ib.wL)
    resR = zero(KS.ib.wL)
    avgR = zero(KS.ib.wL)
    resU = zero(KS.ib.wL)
    avgU = zero(KS.ib.wL)
    resD = zero(KS.ib.wL)
    avgD = zero(KS.ib.wL)

    if bc != :fix
        @inbounds for j = 1:KS.pSpace.ny
            #new
            x1 = @view contr[1, j, :]
            x2 = @view a1fc_fw[1, j, :]
            x3 = @view a1fc_ff[1, j, :]
            x4 = @view a1fc_fw[2, j, :]
            x5 = @view a1fc_ff[2, j, :]
            x6 = @view a2fc_fw[1, j, :]
            x7 = @view a2fc_ff[1, j, :]
            x8 = @view a2fc_fw[1, j+1, :]
            x9 = @view a2fc_ff[1, j+1, :]
            #new end
            step1!(
                ctr[1, j].w,
                ctr[1, j].prim,
                #ctr[1, j].f,
                #new
                x1,
                #new end
                #a1face[1, j].fw,
                #a1face[1, j].ff,
                #a1face[2, j].fw,
                #a1face[2, j].ff,
                #new
                x2,
                x3,
                x4,
                x5,
                #new end
                #a2face[1, j].fw,
                #a2face[1, j].ff,
                #a2face[1, j+1].fw,
                #a2face[1, j+1].ff,
                #new
                x6,
                x7,
                x8,
                x9,
                #new end
                #KS.vSpace.u,
                #KS.vSpace.v,
                #KS.vSpace.weights,
                #new
                u,
                v,
                weight,
                #new end
                KS.gas.γ,
                KS.gas.μᵣ,
                KS.gas.ω,
                KS.gas.Pr,
                ctr[1, j].dx * ctr[1, j].dy,
                dt,
                resL,
                avgL,
                coll,
            )

            #new
            x10 = @view contr[KS.pSpace.nx, j, :]
            x11 = @view a1fc_fw[KS.pSpace.nx, j, :]
            x12 = @view a1fc_ff[KS.pSpace.nx, j, :]
            x13 = @view a1fc_fw[KS.pSpace.nx+1, j, :]
            x14 = @view a1fc_ff[KS.pSpace.nx+1, j, :]
            x15 = @view a2fc_fw[KS.pSpace.nx, j, :]
            x16 = @view a2fc_ff[KS.pSpace.nx, j, :]
            x17 = @view a2fc_fw[KS.pSpace.nx, j+1, :]
            x18 = @view a2fc_ff[KS.pSpace.nx, j+1, :]
            #new end
            step1!(
                ctr[KS.pSpace.nx, j].w,
                ctr[KS.pSpace.nx, j].prim,
                #ctr[KS.pSpace.nx, j].f,
                #new
                x10,
                #new end
                #a1face[KS.pSpace.nx, j].fw,
                #a1face[KS.pSpace.nx, j].ff,
                #a1face[KS.pSpace.nx+1, j].fw,
                #a1face[KS.pSpace.nx+1, j].ff,
                #new
                x11,
                x12,
                x13,
                x14,
                #new end
                #a2face[KS.pSpace.nx, j].fw,
                #a2face[KS.pSpace.nx, j].ff,
                #a2face[KS.pSpace.nx, j+1].fw,
                #a2face[KS.pSpace.nx, j+1].ff,
                #new
                x15,
                x16,
                x17,
                x18,
                #new end
                #KS.vSpace.u,
                #KS.vSpace.v,
                #KS.vSpace.weights,
                #new
                u,
                v,
                weight,
                #new end
                KS.gas.γ,
                KS.gas.μᵣ,
                KS.gas.ω,
                KS.gas.Pr,
                ctr[KS.pSpace.nx, j].dx * ctr[KS.pSpace.nx, j].dy,
                dt,
                resR,
                avgR,
                coll,
            )
        end

        @inbounds for i = 2:KS.pSpace.nx-1 # skip overlap
            #new
            x1 = @view contr[i, 1, :]
            x2 = @view a1fc_fw[i, 1, :]
            x3 = @view a1fc_ff[i, 1, :]
            x4 = @view a1fc_fw[i+1, 1, :]
            x5 = @view a1fc_ff[i+1, 1, :]
            x6 = @view a2fc_fw[i, 1, :]
            x7 = @view a2fc_ff[i, 1, :]
            x8 = @view a2fc_fw[i, 2, :]
            x9 = @view a2fc_ff[i, 2, :]
            #new end
            step1!(
                ctr[i, 1].w,
                ctr[i, 1].prim,
                #ctr[i, 1].f,
                #new
                x1,
                #new end
                #a1face[i, 1].fw,
                #a1face[i, 1].ff,
                #a1face[i+1, 1].fw,
                #a1face[i+1, 1].ff,
                #new
                x2,
                x3,
                x4,
                x5,
                #new end
                #a2face[i, 1].fw,
                #a2face[i, 1].ff,
                #a2face[i, 2].fw,
                #a2face[i, 2].ff,
                #new
                x6,
                x7,
                x8,
                x9,
                #new end
                #KS.vSpace.u,
                #KS.vSpace.v,
                #KS.vSpace.weights,
                #new
                u,
                v,
                weight,
                #new end
                KS.gas.γ,
                KS.gas.μᵣ,
                KS.gas.ω,
                KS.gas.Pr,
                ctr[i, 1].dx * ctr[i, 1].dy,
                dt,
                resD,
                avgD,
                coll,
            )

            #new
            x10 = @view contr[i, KS.pSpace.ny, :]
            x11 = @view a1fc_fw[i, KS.pSpace.ny, :]
            x12 = @view a1fc_ff[i, KS.pSpace.ny, :]
            x13 = @view a1fc_fw[i+1, KS.pSpace.ny, :]
            x14 = @view a1fc_ff[i+1, KS.pSpace.ny, :]
            x15 = @view a2fc_fw[i, KS.pSpace.ny, :]
            x16 = @view a2fc_ff[i, KS.pSpace.ny, :]
            x17 = @view a2fc_fw[i, KS.pSpace.ny+1, :]
            x18 = @view a2fc_ff[i, KS.pSpace.ny+1, :]
            #new end
            step1!(
                ctr[i, KS.pSpace.ny].w,
                ctr[i, KS.pSpace.ny].prim,
                #ctr[i, KS.pSpace.ny].f,
                #new
                x10,
                #new end
                #a1face[i, KS.pSpace.ny].fw,
                #a1face[i, KS.pSpace.ny].ff,
                #a1face[i+1, KS.pSpace.ny].fw,
                #a1face[i+1, KS.pSpace.ny].ff,
                #new
                x11,
                x12,
                x13,
                x14,
                #new end
                #a2face[i, KS.pSpace.ny].fw,
                #a2face[i, KS.pSpace.ny].ff,
                #a2face[i, KS.pSpace.ny+1].fw,
                #a2face[i, KS.pSpace.ny+1].ff,
                #new
                x15,
                x16,
                x17,
                x18,
                #new end
                #KS.vSpace.u,
                #KS.vSpace.v,
                #KS.vSpace.weights,
                #new
                u,
                v,
                weight,
                #new end
                KS.gas.γ,
                KS.gas.μᵣ,
                KS.gas.ω,
                KS.gas.Pr,
                ctr[i, KS.pSpace.ny].dx * ctr[i, KS.pSpace.ny].dy,
                dt,
                resU,
                avgU,
                coll,
            )
        end
    end

    for i in eachindex(residual)
        residual[i] +=
            sqrt((resL[i] + resR[i] + resU[i] + resD[i]) * 2) /
            (avgL[i] + avgR[i] + avgU[i] + avgD[i] + 1.e-7)
    end

    ngx = 1 - first(eachindex(KS.pSpace.x[:, 1]))
    ngy = 1 - first(eachindex(KS.pSpace.y[1, :]))
    if bc == :extra
        for i = 1:ngx, j = 1:KS.pSpace.ny
            ctr[1-i, j].w .= ctr[1, j].w
            ctr[1-i, j].prim .= ctr[1, j].prim
            ctr[KS.pSpace.nx+i, j].w .= ctr[KS.pSpace.nx, j].w
            ctr[KS.pSpace.nx+i, j].prim .= ctr[KS.pSpace.nx, j].prim

            #ctr[1-i, j].f .= ctr[1, j].f
            #ctr[KS.pSpace.nx+i, j].f .= ctr[KS.pSpace.nx, j].f
            #new
            contr[1-i, j, :] .= contr[1, j, :]
            contr[KS.pSpace.nx+i, j, :] .= contr[KS.pSpace.nx, j, :]
            #new end
        end

        for i = 1:KS.pSpace.nx, j = 1:ngy
            ctr[i, 1-j].w .= ctr[i, 1].w
            ctr[i, 1-j].prim .= ctr[i, 1].prim
            ctr[i, KS.pSpace.ny+j].w .= ctr[i, KS.pSpace.ny].w
            ctr[i, KS.pSpace.ny+j].prim .= ctr[i, KS.pSpace.ny].prim

            #ctr[i, 1-j].f .= ctr[i, 1].f
            #ctr[i, KS.pSpace.ny+j].f .= ctr[i, KS.pSpace.ny].f
            #new
            contr[i, 1-j, :] .= contr[i, 1, :]
            contr[i, KS.pSpace.ny+j, :] .= contr[i, KS.pSpace.ny, :]
            #new end
        end
    elseif bc == :period

    elseif bc == :balance

    end

end

#------------------ end of function definitions ------------------#