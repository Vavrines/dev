"""
1D test of particle distribution function
"""

using FFTW
using FINUFFT
using Plots
using KitBase

# uniform velocity
vs = VSpace1D()
N = vs.nu
f = maxwellian(vs.u, [1., 0., 1.])
fs = fft(f)

x = [i/N for i = 0:N-1] .|> Float64
s = [i for i = 0:N-1] .|> Float64
fss = nufft1d3(2pi .* x, f[1:end] .+ 0.0im, -1, 1e-10, s)

plot(real(fs |> fftshift))
plot!(real(fss |> fftshift), line=:dash)

# nonuniform velocity
_u = [vs.u1 / (N)^3 * (-N + 2 * (i - 1))^3 for i = 1:N+1]
u1 = (_u[1:end-1] .+ _u[2:end]) ./ 2
f1 = maxwellian(u1, [1., 0., 1.])

δu = (u1[end] - u1[1]) / (N - 1)
x1 = zeros(N)
for i = 1:N
    x1[i] = (u1[i] - u1[1]) / (δu * N)
end

fs1 = nufft1d3(2pi .* x1, f1[1:end] .+ 0.0im, -1, 1e-10, s)

plot(real(fs1 |> fftshift))

#=
x2 = zeros(N)
δu2 = (vs.u[end] - vs.u[1]) / (N - 1)
for i = 1:N
    x2[i] = (vs.u[i] - vs.u[1]) / (δu2 * N)
end
fs2 = nufft1d3(2pi .* x2, f[1:end] .+ 0.0im, -1, 1e-10, s)
plot!(real(fs2 |> fftshift))=#
