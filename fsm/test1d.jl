using KitBase
using KitBase.FFTW
using KitBase.SpecialFunctions
using FINUFFT
using Plots

nx = 100
xmin = 0
xmax = 1

x0 = linspace(xmin, xmax, nx)
f0 = sin.(2pi * x0)
begin
    _nx = nx + 1
    _x = [xmax / (_nx - 1)^3 * (-_nx + 1 + 2 * (i - 1))^3 for i = 1:_nx]
    x = (_x[1:end-1] .+ _x[2:end]) ./ 2
end
x = rand(nx)

f = sin.(2pi * x)

plot(x0, f0)
scatter!(x, f)

xj = zeros(nx)
δx = (x[end] - x[1]) / (nx - 1)
for i = 1:nx
    xj[i] = (x[i] - x[1]) / δx / nx
end
#xj = [i/nx for i = 0:nx-1] .|> Float64

s = [i for i = 0:nx-1] .|> Float64

fspec = nufft1d3(2pi .* x, f .+ 0.0im, -1, 1e-10, s)
fspec0 = fft(f0)


fspec = nufft1d1(2pi .* xj, f .+ 0.0im, -1, 1e-10, nx)


using FastTransforms


f_nft = nufft2(f .+ 0.0im, x, eps())

plot(real(f_nft))





plot(real(fspec))
plot!(real(fspec0), line=:dash)

tmp = fftshift(fspec0)
plot(real(tmp))

tmp = nufft1d3(2pi .* xj, fspec, 1, 1e-10, s) ./ 100

plot(x, real(tmp))

t1 = nufft1d2(2pi * xj, -1, 1e-10, f .+ 0im)
t2 = nufft1d2(2pi * xj, 1, 1e-10, t1) ./ 100
plot(real(t2))

y = 3*pi*(1.0 .- 2*rand(100))

ff = randn(ComplexF64, 100)

p1 = ifft(ff)
fft(p1)

p2 = nufft1d3(2pi * xj, ff, 1, 1e-10, s) ./100
nufft1d3(2pi * xj, p2, -1, 1e-10, s)

p1 = nufft1d3(2pi * xj, ff, -1, 1e-10, s)
p2 = nufft1d3(2pi * xj, ff, 1, 1e-10, s) ./100

plot(real(p1))
plot!(real(p2), line=:dash)