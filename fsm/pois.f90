! export OMP_NUM_THREADS=4
! ifort -O3 -openmp -o test  test.f90 -lfftw3 
! nohup ./test >output.log &
! ifort -O3 -o test  test.f90 -lfftw3_threads -lfftw3 -lpthread
! ifort -O2 -mcmodel medium -shared-intel -o  test_mul  test_mul.f90  -lfftw3 

program testit
use omp_lib
use ifport
implicit none
!include '/usr/include/fftw3.f'  
include '/opt/intel/oneapi/mkl/latest/include/fftw/fftw3.f'


!!!!!!!!!!!!!!!!!!!!!! set the parameters !!!!!!!!!!!!!!!!!!!!!!!!!!!!	

real*8, parameter :: delta=0.1d0, McCormack=1.4681d0
real*8, parameter :: chi_B=0.5d0, chi_A=1.0d0-chi_B
real*8, parameter :: mB=20.183d0/39.948d0
real*8, parameter :: dB=1.0d0/1.406d0
real*8, parameter :: mA=1.0d0, dA=1.0d0
real*8, parameter :: ra=2.0d0*mB/(mA+mB), rb=(mB-mA)/(mB+mA), ra2=2.0d0*mA/(mA+mB), rb2=(mA-mB)/(mB+mA)

real*8, parameter :: omega=0.5d0 	! shear viscosity: mu=T^omega	

real*8, parameter :: Asp=1 ! spatial coordinates
integer*8, parameter :: Nx=20, Ny=1

! velocity and frequency		
real*8, parameter :: vx_max =6.0d0*sqrt(mA/mB), vy_max =6.0d0*sqrt(mA/mB), vz_max=6.0d0*sqrt(mA/mB)	 ! velocity grids
real*8, parameter :: vx_max2=5.0d0*sqrt(mA/mB), vy_max2=5.0d0*sqrt(mA/mB)  

integer*8, parameter :: Nvx =64, Nvy =48, Nvz=32
integer*8, parameter :: Nvx2=48, Nvy2=32   ! frequency components
	 
integer*8, parameter :: M=6, Mq=7

real*8, parameter :: accom=1.0d0

integer*8 MM,integral_loop,M2

character(len=102) :: filename='pois1d_flow_rate.txt'
character(len=102) :: filename2='pois1d_uzqz.txt'

!!!!!!!!!!!!!!!!!!!!!! set the parameters !!!!!!!!!!!!!!!!!!!!!!!!!!!!	
real*8  Kn, Caa, Cbb, Cab
real*8  alpha, eta, Gam !interatomic potential parameters	 
real*8  time_begin, time_end
real*8, parameter :: PI=4.0d0*atan(1.0d0)
complex*16, parameter :: yii=cmplx(0.0d0,1.0d0) ! pi and i 

real*8 x(Nx), y(Ny), dXY(Nx,Ny)
real*8 bx(Nx), cx(Nx), dx(Nx),  bxn(Nx), cxn(Nx), dxn(Nx)
real*8 by(Ny), cy(Ny), dy(Ny),  byn(Ny), cyn(Ny), dyn(Ny)
! macroscopic quantities
real*8 uzA(Nx,Ny), qzA(Nx,Ny), uzA2(Nx,Ny)
real*8 uzB(Nx,Ny), qzB(Nx,Ny), uzB2(Nx,Ny)
real*8 error ! error between iteration steps

real*8 vx(1:Nvx), vy(1:Nvy), vz(1:Nvz/2)
real*8 wt_vx(1:Nvx), wt_vy(1:Nvy)
real*8 fre_vx(1:Nvx2), fre_vy(1:Nvy2), fre_vz(1:Nvz)

complex*16 fz_spec(1:Nvz)

real*8, dimension(:,:,:), allocatable:: vx3, vy3, vz3, c2, dV, mu, mug, feq, geq

complex*16, dimension(:,:,:), allocatable:: f_spec, g_spec, f_spec1, g_spec1, f_spec2, g_spec2
complex*16, dimension(:,:), allocatable::  f_spec_temp, g_spec_temp, f_spec1_temp, g_spec1_temp
real*8, dimension(:,:), allocatable:: mu_temp
complex*16, dimension(:,:), allocatable:: fre_fx, fre_bx, fre_fy, fre_by

real*8, dimension(:,:,:,:,:), allocatable:: f,g ! distribution functions
complex*16, dimension(:,:,:), allocatable:: fc1,fc2,f_temp, f_spec_eq, g_spec_eq
real*8, dimension(:,:,:,:), allocatable:: phi, psi, conv_ext, alphaC1, alphaC2, alphaC3, alphaC4
complex*16, dimension(:,:,:,:), allocatable:: fggain1, fggain2, gfgain1, gfgain2, ffgain1, ffgain2, gggain1, gggain2
real*8, dimension(:), allocatable:: jd, wt

real*8 ff(1:Nvx,1:Nvy,1:Nvz/2), fluz 

real*8, dimension(:,:,:,:), allocatable:: finx, finy

!!!!!! paramters for the kernel modes
real*8 R ! Support of the distribution funtion	
integer*8, parameter:: quad_num=164  ! Gauss-Legendre quadrature
real*8 abscissa(quad_num), weight(quad_num), abscissa2(Mq), weight2(Mq)

real*8 :: phipsi(1:Nvx2,1:Nvy2,1:Nvz)=0.0d0
real*8 int_temp, int_temp2
real*8 theta, theta2, s, s2
real*8 bessel
!
integer*8 i, j, k, loop, loop2, idx, id, iteration_step, lx, ly, ix, iy, lf, lv

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!///// fft plans		  
integer*8 plan_fzb, plan_fzf, plan_conv_f1, plan_conv_f2, plan_de_conv, iret

allocate(f(Nvx,Nvy,Nvz/2,Nx,Ny))
allocate(g(Nvx,Nvy,Nvz/2,Nx,Ny))
allocate(mu(Nvx,Nvy,Nvz/2))
allocate(mug(Nvx,Nvy,Nvz/2))

allocate(fc1(Nvx2,Nvy2,Nvz))
allocate(fc2(Nvx2,Nvy2,Nvz))
allocate(f_temp(Nvx2,Nvy2,Nvz))
allocate(f_spec_eq(Nvx2,Nvy2,Nvz))
allocate(g_spec_eq(Nvx2,Nvy2,Nvz))

allocate(feq(Nvx,Nvy,Nvz/2))
allocate(geq(Nvx,Nvy,Nvz/2))

allocate(finx(1:Nvx/2,1:Nvy,1:Nvz/2,Ny))
allocate(finy(1:Nvx/2,1:Nvy,1:Nvz/2,Nx))


allocate(fre_fx(Nvx,Nvx2))
allocate(fre_bx(Nvx,Nvx2))

allocate(fre_fy(Nvy,Nvy2))
allocate(fre_by(Nvy,Nvy2))

allocate(f_spec(Nvx2,Nvy,Nvz/2))
allocate(g_spec(Nvx2,Nvy,Nvz/2))
allocate(f_spec_temp(Nvy,Nvz/2))
allocate(g_spec_temp(Nvy,Nvz/2))

allocate(f_spec1(Nvx2,Nvy2,Nvz/2))
allocate(g_spec1(Nvx2,Nvy2,Nvz/2))
allocate(f_spec1_temp(Nvx2,Nvz/2))
allocate(g_spec1_temp(Nvx2,Nvz/2))

allocate(f_spec2(Nvx2,Nvy2,Nvz))
allocate(g_spec2(Nvx2,Nvy2,Nvz))

allocate(mu_temp(Nvy,Nvz/2))


allocate(vx3(Nvx,Nvy,Nvz/2))
allocate(vy3(Nvx,Nvy,Nvz/2))
allocate(vz3(Nvx,Nvy,Nvz/2))
allocate(c2(Nvx,Nvy,Nvz/2))
allocate(dV(Nvx,Nvy,Nvz/2))


!!!!!!!!!!!!!!!!!!!!!! allocate the kernel modes
MM=(M/2)*2
M2=MM/2
integral_loop=MM/2*MM/2
allocate(jd(MM))
allocate(wt(MM))


allocate(phi(Nvx2,Nvy2,Nvz,integral_loop))
allocate(psi(Nvx2,Nvy2,Nvz,integral_loop))
allocate( conv_ext(Nvx2,Nvy2,Nvz,integral_loop*(2*Mq+1)) )

allocate(alphaC1(Nvx2,Nvy2,Nvz,integral_loop*2*Mq))
allocate(alphaC2(Nvx2,Nvy2,Nvz,integral_loop*2*Mq))
allocate(alphaC3(Nvx2,Nvy2,Nvz,integral_loop*2*Mq))
allocate(alphaC4(Nvx2,Nvy2,Nvz,integral_loop*2*Mq))


allocate(fggain1(Nvx2,Nvy2,Nvz,integral_loop*2*Mq))
allocate(fggain2(Nvx2,Nvy2,Nvz,integral_loop*2*Mq))
allocate(gfgain1(Nvx2,Nvy2,Nvz,integral_loop*2*Mq))
allocate(gfgain2(Nvx2,Nvy2,Nvz,integral_loop*2*Mq))

allocate(ffgain1(Nvx2,Nvy2,Nvz,integral_loop))
allocate(ffgain2(Nvx2,Nvy2,Nvz,integral_loop))
allocate(gggain1(Nvx2,Nvy2,Nvz,integral_loop))
allocate(gggain2(Nvx2,Nvy2,Nvz,integral_loop))


	
! integer*8, parameter :: nthreads=1     
!call dfftw_init_threads(iret)
!call dfftw_plan_with_nthreads(nthreads)		 

call dfftw_plan_dft_1d(plan_fzb,Nvz,fz_spec,fz_spec,FFTW_BACKWARD,FFTW_MEASURE)
call dfftw_plan_dft_1d(plan_fzf,Nvz,fz_spec,fz_spec,FFTW_FORWARD, FFTW_MEASURE)
  
call dfftw_plan_dft_3d(plan_conv_f1,Nvx2,Nvy2,Nvz,fc1,fc1,FFTW_FORWARD, FFTW_MEASURE)
call dfftw_plan_dft_3d(plan_conv_f2,Nvx2,Nvy2,Nvz,fc2,fc2,FFTW_FORWARD, FFTW_MEASURE)
call dfftw_plan_dft_3d(plan_de_conv,Nvx2,Nvy2,Nvz,f_temp,f_temp,FFTW_BACKWARD,FFTW_MEASURE)
  

!!!!///////////////////////////////// nonuniform velocity domain
lv=3;
do lx=1,Nvx
	s=-Nvx/2+lx-0.5d0      	
	vx(lx)=vx_max2*(s**lv)/(Nvx/2.0-0.5d0)**lv
	wt_vx(lx)=lv*s**(lv-1.0)/(Nvx/2.0-0.5d0)**lv*vx_max2
enddo
!!!!!!!!!!!!!!!
lv=1;
do lx=1,Nvy
	s=-Nvy/2+lx-0.5d0
	vy(lx)=vy_max2*s**lv/(Nvy/2-0.5d0)**lv
	wt_vy(lx)=vy_max2*lv*s**(lv-1.0)/(Nvy/2-0.5d0)**lv
enddo
!!!!!!!!!!!!!!!!!!!!!! vz, uniform
do lx=1,Nvz/2
	vz(lx)=2.0d0*vz_max/(Nvz-1.0d0)*(lx-0.5)
enddo

! generate 3d velocity grids
call ndgrid(Nvx,Nvy,Nvz/2,vx,vy,vz,vx3,vy3,vz3) 

! weight in velocity space, numerical quadrature
forall(i=1:Nvx,j=1:Nvy,k=1:Nvz/2)
	dV(i,j,k)=2.0d0*wt_vx(i)*wt_vy(j)*2.0d0*vz_max/(Nvz-1.0d0)
end forall



!!!!!!!!!!! spatial discretization
do lx=1,Nx
	s=1.0d0/(2.0*Nx-2.0d0)* (lx-1.0d0)
	x(lx) =s*s*s*(10.0d0-15.0d0*s+6.0d0*s*s) 
	!x(lx)=0.5d0/(Nx-1)*(lx-1)
enddo

y(1)=1;
!
dx(1)=(x(2)-x(1))/2.0d0;
dx(Nx)=(x(Nx)-x(Nx-1))/2.0d0;
do lx=2,Nx-1
	dx(lx)=(x(lx+1)-x(lx-1))/2.0d0	
enddo

! weight
forall(lx=1:Nx,ly=1:Ny)
	dXY(lx,ly)=dx(lx)
end forall

!!!!
bx(2)=1.0d0/(x(2)-x(1))
dx(2)=bx(2)
do lx=3,Nx
	s=(x(lx-2)-x(lx))*(x(lx-1)-x(lx))*(x(lx-2)-x(lx-1))
	dx(lx)=((x(lx-1)-x(lx))**2.0d0-(x(lx-2)-x(lx))**2.0d0) /s
	bx(lx)=(x(lx-2)-x(lx))**2.0d0  /s
	cx(lx)=(x(lx-1)-x(lx))**2.0d0  /s  
enddo
!!!!
bxn(Nx-1)=-1.0d0/(x(Nx)-x(Nx-1))
dxn(Nx-1)=bxn(Nx-1)
do lx=1,Nx-2
	s=(x(lx+2)-x(lx))*(x(lx+1)-x(lx))*(x(lx+2)-x(lx+1))
    dxn(lx)=((x(lx+1)-x(lx))**2.0d0-(x(lx+2)-x(lx))**2.0d0) /s
    bxn(lx)=(x(lx+2)-x(lx))**2.0d0 /s   
    cxn(lx)=(x(lx+1)-x(lx))**2.0d0 /s 
enddo



  
!!!!///////////////////////////////// calculate the kernel mode	 	
alpha=2.0d0*(1.0d0-omega);
Gam=0.0d0	 ! SYMMETRY

! Support 
R=sqrt(2.0d0) *2.0d0*max( vx_max,vy_max,vz_max )/( 2.0d0+sqrt(2.0d0) ) ! sqrt(2)*Support

!!!! frequency domain
do lx=-Nvx2/2,Nvx2/2-1
	fre_vx(lx+Nvx2/2+1)=lx*2.0d0*PI/(Nvx*2.0d0*vx_max/(Nvx-1.0d0))
enddo

do lx=-Nvy2/2,Nvy2/2-1
	fre_vy(lx+Nvy2/2+1)=lx*2.0d0*PI/(Nvy*2.0d0*vy_max/(Nvy-1.0d0))
enddo

do lx=-Nvz/2,Nvz/2-1
	fre_vz(lx+Nvz/2+1) =lx*2.0d0*PI/(Nvz*2.0d0*vz_max/(Nvz-1.0d0))
enddo


	! Gauss-Legendre quadrature
	call lgwt(quad_num, 0.0d0, R, abscissa,  weight)	 
	call lgwt(Mq,       0.0d0, R, abscissa2, weight2)	
	call lgwt(M, 0.0d0, PI, jd, wt)	 
	
    idx=0
	do loop=1,M2
		theta=jd(loop)
	do loop2=1,M2
		theta2=jd(loop2)
		idx=idx+1
		
		do k=1,Nvz
		do j=1,Nvy2
		do i=1,Nvx2  				    
			s=fre_vx(i)*sin(theta)*cos(theta2)+fre_vy(j)*sin(theta)*sin(theta2)+fre_vz(k)*cos(theta)
			!phi  
			int_temp=0.0d0
			do id=1,quad_num
				int_temp=int_temp+2.0d0*weight(id)* cos(s*abscissa(id)) *(abscissa(id)**(alpha+Gam)) 
			enddo
			!psi  
			s2=fre_vx(i)*fre_vx(i)+fre_vy(j)*fre_vy(j)+fre_vz(k)*fre_vz(k)-s*s
			if(s2<=0.0d0) s2=0 !	
			s2=sqrt(s2)			
			int_temp2=0
			do id=1,quad_num
				bessel= bessel_jn(0,s2*abscissa(id))	
				int_temp2=int_temp2+2.0d0*PI*weight(id)* bessel *(abscissa(id)**(1.0d0-Gam))*sin(theta)*wt(loop)*wt(loop2)
			enddo
			!phipsi
			phipsi(i,j,k)=phipsi(i,j,k)+int_temp*int_temp2	
			phi(i,j,k,idx)=int_temp
			psi(i,j,k,idx)=int_temp2
			!!!!!!!!!!!!		
		    do lx=1,Mq
		        !% Q(f,g)
		        alphaC1(i,j,k,(idx-1)*2*Mq+lx)   = 2*abscissa2(lx)**alpha*cos(ra*abscissa2(lx)*s)*weight2(lx);
		        alphaC2(i,j,k,(idx-1)*2*Mq+lx)   =                        cos(rb*abscissa2(lx)*s)*int_temp2;
		        alphaC1(i,j,k,(idx-1)*2*Mq+lx+Mq)=-2*abscissa2(lx)**alpha*sin(ra*abscissa2(lx)*s)*weight2(lx);
		        alphaC2(i,j,k,(idx-1)*2*Mq+lx+Mq)=                        sin(rb*abscissa2(lx)*s)*int_temp2;
		       ! % Q(g,f)
		        alphaC3(i,j,k,(idx-1)*2*Mq+lx)   = 2*abscissa2(lx)**alpha*cos(ra2*abscissa2(lx)*s)*weight2(lx);
		        alphaC4(i,j,k,(idx-1)*2*Mq+lx)   =                        cos(rb2*abscissa2(lx)*s)*int_temp2;
		        alphaC3(i,j,k,(idx-1)*2*Mq+lx+Mq)=-2*abscissa2(lx)**alpha*sin(ra2*abscissa2(lx)*s)*weight2(lx);
		        alphaC4(i,j,k,(idx-1)*2*Mq+lx+Mq)=                        sin(rb2*abscissa2(lx)*s)*int_temp2;
		    enddo      
			!!!!!!!!!	  
		enddo
		enddo
		enddo
		
	enddo
	enddo


	!!!! nonuniform fft	
	forall(lf=1:Nvx2,lv=1:Nvx)
		fre_bx(lv,lf)=exp( yii*fre_vx(lf)*vx(lv))
		fre_fx(lv,lf)=exp(-yii*fre_vx(lf)*vx(lv))
	end forall

	!
	forall(lf=1:Nvy2,lv=1:Nvy)
		fre_by(lv,lf)=exp( yii*fre_vy(lf)*vy(lv))
		fre_fy(lv,lf)=exp(-yii*fre_vy(lf)*vy(lv))	
	end forall










	!///////////////////////////////////////////////////////////////////////////////////////

	Kn=4.0d0*sqrt(2.0d0)*PI/delta/McCormack
	Caa=4.0d0*(dA/dA)**2.0d0/Kn 
	Cbb=4.0d0*(dB/dA)**2.0d0/Kn  
	Cab=    (1+dB/dA)**2.0d0/Kn

	feq=(mA/PI)**1.5d0*chi_A*exp( -mA*(vx3*vx3+vy3*vy3+vz3*vz3) )
	geq=(mB/PI)**1.5d0*chi_B*exp( -mB*(vx3*vx3+vy3*vy3+vz3*vz3) )

    !//////////////////////////// generate uniform spectrum /////////////////////////////////////
	!$OMP PARALLEL do PRIVATE(lf,lv,f_spec_temp,g_spec_temp) 
	do lf=1,Nvx2 ! nonuniform ifft in vx direction
		f_spec_temp=cmplx(0.0,0.0)
		g_spec_temp=cmplx(0.0,0.0)
		do lv=1,Nvx
			f_spec_temp=f_spec_temp+wt_vx(lv)*feq(lv,:,:)*fre_bx(lv,lf) 
		    g_spec_temp=g_spec_temp+wt_vx(lv)*geq(lv,:,:)*fre_bx(lv,lf)
		enddo
		f_spec(lf,:,:)=f_spec_temp/vx_max/2.0*(Nvx-1.0)/Nvx
		g_spec(lf,:,:)=g_spec_temp/vx_max/2.0*(Nvx-1.0)/Nvx
	enddo
	!$OMP END PARALLEL do	
			
	!$OMP PARALLEL do PRIVATE(lf,lv,f_spec1_temp,g_spec1_temp) 	
	do lf=1,Nvy2 ! nonuniform ifft in vy direction
		f_spec1_temp=cmplx(0.0,0.0)
		g_spec1_temp=cmplx(0.0,0.0)
		do lv=1,Nvy
			f_spec1_temp=f_spec1_temp+wt_vy(lv)*f_spec(:,lv,:)*fre_by(lv,lf) 
		    g_spec1_temp=g_spec1_temp+wt_vy(lv)*g_spec(:,lv,:)*fre_by(lv,lf)
		enddo
		f_spec1(:,lf,:)=f_spec1_temp/vy_max/2.0*(Nvy-1.0)/Nvy
		g_spec1(:,lf,:)=g_spec1_temp/vy_max/2.0*(Nvy-1.0)/Nvy
	enddo
	!$OMP END PARALLEL do	
			
	!$OMP PARALLEL do PRIVATE(lf,lv,fz_spec) 				
	do lv=1,Nvy2 ! uniform ifft in vz direction
	do lf=1,Nvx2			
		fz_spec(Nvz/2+1:Nvz)=f_spec1(lf,lv,:)
		fz_spec(Nvz/2:1:-1) =f_spec1(lf,lv,:)
		call fftshift1d(Nvz,fz_spec)
		call dfftw_execute_dft(plan_fzb, fz_spec, fz_spec) 
		call fftshift1d(Nvz,fz_spec)
		f_spec2(lf,lv,:)=fz_spec/Nvz
	enddo
	enddo
	!$OMP END PARALLEL do

	!$OMP PARALLEL do PRIVATE(lf,lv,fz_spec) 				
	do lv=1,Nvy2 ! uniform ifft in vz direction
	do lf=1,Nvx2			
		fz_spec(Nvz/2+1:Nvz)=g_spec1(lf,lv,:)
		fz_spec(Nvz/2:1:-1) =g_spec1(lf,lv,:)
		call fftshift1d(Nvz,fz_spec)
		call dfftw_execute_dft(plan_fzb, fz_spec, fz_spec) 
		call fftshift1d(Nvz,fz_spec)
		g_spec2(lf,lv,:)=fz_spec/Nvz
	enddo
	enddo
	!$OMP END PARALLEL do

	!////////////////////// loss f  ////////////////////////////
	f_temp=(Caa*f_spec2+Cab*g_spec2)*phipsi
	
	! uniform fft in f_vz direction
	!$OMP PARALLEL do PRIVATE(lf,lv,fz_spec) 	
	do lv=1,Nvy2
	do lf=1,Nvx2			
		fz_spec=f_temp(lf,lv,:)		        
		call fftshift1d(Nvz,fz_spec) ! fftshift
		call dfftw_execute_dft(plan_fzf, fz_spec, fz_spec) 
		call fftshift1d(Nvz,fz_spec) ! fftshift
		f_spec1(lf,lv,:)=( fz_spec(Nvz/2+1:Nvz)+fz_spec(Nvz/2:1:-1) )
	enddo
	enddo
	!$OMP END PARALLEL do

	! nonuniform fft in f_vy direction
	!$OMP PARALLEL do PRIVATE(lf,lv) 	
	do lv=1,Nvy
		f_spec(:,lv,:)=cmplx(0.0)
		do lf=1,Nvy2        
			f_spec(:,lv,:)=f_spec(:,lv,:)+f_spec1(:,lf,:)*fre_fy(lv,lf)! *exp(-yii*fre_vy(lf)*vy(lv))
		enddo
	enddo
	!$OMP END PARALLEL do
	
	! nonuniform fft in f_vx direction
	!$OMP PARALLEL do PRIVATE(lf,lv,mu_temp) 	
	do lv=1,Nvx
		mu_temp=0.0
		do lf=1,Nvx2
			mu_temp=mu_temp+real(f_spec(lf,:,:)*fre_fx(lv,lf) )!*exp(-yii*fre_vx(lf)*vx(lv)) 
		enddo
		mu(lv,:,:)=mu_temp
	enddo	
	!$OMP END PARALLEL do		
	mu=mu+mu(Nvx:1:-1,:,:)	! symmetry	

	!////////////////////// loss g  ////////////////////////////
	f_temp=(Cab*f_spec2+Cbb*g_spec2)*phipsi
	!!!!! uniform fft in f_vz direction
	!$OMP PARALLEL do PRIVATE(lf,lv,fz_spec) 	
	do lv=1,Nvy2
	do lf=1,Nvx2			
		fz_spec=f_temp(lf,lv,:)		        
		call fftshift1d(Nvz,fz_spec) ! fftshift
		call dfftw_execute_dft(plan_fzf, fz_spec, fz_spec) 
		call fftshift1d(Nvz,fz_spec) ! fftshift
		f_spec1(lf,lv,:)=( fz_spec(Nvz/2+1:Nvz)+fz_spec(Nvz/2:1:-1) )
	enddo
	enddo
	!$OMP END PARALLEL do
	
	! nonuniform fft in f_vy direction
	!$OMP PARALLEL do PRIVATE(lf,lv) 	
	do lv=1,Nvy
		f_spec(:,lv,:)=cmplx(0.0)
		do lf=1,Nvy2        
			f_spec(:,lv,:)=f_spec(:,lv,:)+f_spec1(:,lf,:)*fre_fy(lv,lf)! *exp(-yii*fre_vy(lf)*vy(lv))
		enddo
	enddo
	!$OMP END PARALLEL do
	
	! nonuniform fft in f_vx direction
	!$OMP PARALLEL do PRIVATE(lf,lv,mu_temp) 	
	do lv=1,Nvx
		mu_temp=0.0
		do lf=1,Nvx2
			mu_temp=mu_temp+real(f_spec(lf,:,:)*fre_fx(lv,lf) )!*exp(-yii*fre_vx(lf)*vx(lv)) 
		enddo
		mug(lv,:,:)=mu_temp
	enddo	
	!$OMP END PARALLEL do		
	mug=mug+mug(Nvx:1:-1,:,:)	! symmetry	

	! spectrum of the spectrum of the equilibrium VDF
	fc1=f_spec2
	call dfftw_execute_dft(plan_conv_f1, fc1, fc1)	
	f_spec_eq=fc1

	fc1=g_spec2
	call dfftw_execute_dft(plan_conv_f1, fc1, fc1)	
	g_spec_eq=fc1

	do i=1,integral_loop 
		fc1=f_spec2*phi(:,:,:,i)
		call dfftw_execute_dft(plan_conv_f1, fc1, fc1)	
		ffgain1(:,:,:,i)=fc1

		fc1=g_spec2*phi(:,:,:,i)
		call dfftw_execute_dft(plan_conv_f1, fc1, fc1)	
		gggain1(:,:,:,i)=fc1

		fc1=f_spec2*psi(:,:,:,i)
		call dfftw_execute_dft(plan_conv_f1, fc1, fc1)	
		ffgain2(:,:,:,i)=fc1

		fc1=g_spec2*psi(:,:,:,i)
		call dfftw_execute_dft(plan_conv_f1, fc1, fc1)	
		gggain2(:,:,:,i)=fc1
	enddo	
	
	do i=1,integral_loop*2*Mq 
		fc1=f_spec2*alphaC1(:,:,:,i)
		call dfftw_execute_dft(plan_conv_f1, fc1, fc1)	
		fggain1(:,:,:,i)=fc1

		fc1=g_spec2*alphaC2(:,:,:,i)
		call dfftw_execute_dft(plan_conv_f1, fc1, fc1)	
		fggain2(:,:,:,i)=fc1

		fc1=g_spec2*alphaC3(:,:,:,i)
		call dfftw_execute_dft(plan_conv_f1, fc1, fc1)	
		gfgain1(:,:,:,i)=fc1

		fc1=f_spec2*alphaC4(:,:,:,i)
		call dfftw_execute_dft(plan_conv_f1, fc1, fc1)	
		gfgain2(:,:,:,i)=fc1
	enddo	

	!$OMP PARALLEL do PRIVATE(lx,ly,c2) 
	do ly=1,Ny	
	do lx=1,Nx	
		f(:,:,:,lx,ly)=feq
		g(:,:,:,lx,ly)=geq
	
		uzA(lx,ly) =sum(f(:,:,:,lx,ly)*vz3*dV)	
		uzB(lx,ly) =sum(g(:,:,:,lx,ly)*vz3*dV)	
		c2=vx3**2.0+vy3**2.0+vz3**2.0-2.5
		qzA(lx,ly)=sum(f(:,:,:,lx,ly)*c2*vz3*dV)
		qzB(lx,ly)=sum(g(:,:,:,lx,ly)*c2*vz3*dV)
	enddo		
	enddo
	!$OMP END PARALLEL do




	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	!!!!!!!!!!!!!!!!!!!!  main 
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  

    error=1.0
    write(*,*) delta, chi_B

	do iteration_step=1,10000
		call cpu_time(time_begin)	
		finx=f(1:Nvx/2,:,:,1,:)
		ly=1;
		do lx=1,Nx
			!//////////////////// generate uniform spectrum /////////////////////////////////
			
		    !$OMP PARALLEL do PRIVATE(lf,lv,f_spec_temp,g_spec_temp) 
			do lf=1,Nvx2 ! nonuniform ifft in vx direction
				f_spec_temp=cmplx(0.0,0.0)
		        g_spec_temp=cmplx(0.0,0.0)
				do lv=1,Nvx
					f_spec_temp=f_spec_temp+wt_vx(lv)*f(lv,:,:,lx,ly)*fre_bx(lv,lf) !exp(yii*fre_vx(lf)*vx(lv))
		            g_spec_temp=g_spec_temp+wt_vx(lv)*g(lv,:,:,lx,ly)*fre_bx(lv,lf)
				enddo
				f_spec(lf,:,:)=f_spec_temp/vx_max/2.0*(Nvx-1.0)/Nvx
		        g_spec(lf,:,:)=g_spec_temp/vx_max/2.0*(Nvx-1.0)/Nvx
			enddo
			!$OMP END PARALLEL do	
	
			!$OMP PARALLEL do PRIVATE(lf,lv,f_spec1_temp,g_spec1_temp) 	
			do lf=1,Nvy2 ! nonuniform ifft in vy direction
				f_spec1_temp=cmplx(0.0,0.0)
		        g_spec1_temp=cmplx(0.0,0.0)
				do lv=1,Nvy
					f_spec1_temp=f_spec1_temp+wt_vy(lv)*f_spec(:,lv,:)*fre_by(lv,lf) !exp(yii*fre_vy(lf)*vy(lv))
		            g_spec1_temp=g_spec1_temp+wt_vy(lv)*g_spec(:,lv,:)*fre_by(lv,lf)
				enddo
				f_spec1(:,lf,:)=f_spec1_temp/vy_max/2.0*(Nvy-1.0)/Nvy
		        g_spec1(:,lf,:)=g_spec1_temp/vy_max/2.0*(Nvy-1.0)/Nvy
			enddo
			!$OMP END PARALLEL do	
	
			!$OMP PARALLEL do PRIVATE(lf,lv,fz_spec) 				
			do lv=1,Nvy2 ! uniform ifft in vz direction
			do lf=1,Nvx2			
				fz_spec(Nvz/2+1:Nvz)=f_spec1(lf,lv,:)
				fz_spec(Nvz/2:1:-1) =-f_spec1(lf,lv,:)
				call fftshift1d(Nvz,fz_spec)
				call dfftw_execute_dft(plan_fzb, fz_spec, fz_spec) 
				call fftshift1d(Nvz,fz_spec)
				f_spec2(lf,lv,:)=fz_spec/Nvz
			enddo
			enddo
			!$OMP END PARALLEL do

		    !$OMP PARALLEL do PRIVATE(lf,lv,fz_spec) 				
			do lv=1,Nvy2 ! uniform ifft in vz direction
			do lf=1,Nvx2			
				fz_spec(Nvz/2+1:Nvz)=g_spec1(lf,lv,:)
				fz_spec(Nvz/2:1:-1) =-g_spec1(lf,lv,:)
				call fftshift1d(Nvz,fz_spec)
				call dfftw_execute_dft(plan_fzb, fz_spec, fz_spec) 
				call fftshift1d(Nvz,fz_spec)
				g_spec2(lf,lv,:)=fz_spec/Nvz
			enddo
			enddo
			!$OMP END PARALLEL do
			
			
			
			!////////////////////////// convolution, gain, f /////////////////////////////////
			f_temp=cmplx(0.0d0)

			!$OMP PARALLEL do PRIVATE(i,fc1,fc2) 
			do i=1,integral_loop	
                fc1=cmplx(f_spec2*phi(:,:,:,i))				
				call dfftw_execute_dft(plan_conv_f2, fc1, fc1)
				fc2=cmplx(f_spec2*psi(:,:,:,i))				
				call dfftw_execute_dft(plan_conv_f2, fc2, fc2)
				conv_ext(:,:,:,i)=Caa*(ffgain1(:,:,:,i)*fc2+ffgain2(:,:,:,i)*fc1)
			enddo
			!$OMP END PARALLEL do

            !$OMP PARALLEL do PRIVATE(i,fc1,fc2) 
			do i=1,integral_loop*2*Mq	
                fc1=cmplx(f_spec2*alphaC1(:,:,:,i))				
				call dfftw_execute_dft(plan_conv_f2, fc1, fc1)
				fc2=cmplx(g_spec2*alphaC2(:,:,:,i))				
				call dfftw_execute_dft(plan_conv_f2, fc2, fc2)
				conv_ext(:,:,:,i+integral_loop)=Cab*(fggain1(:,:,:,i)*fc2+fggain2(:,:,:,i)*fc1)
			enddo
			!$OMP END PARALLEL do
				
			f_temp=sum(conv_ext,dim=4)
			fc2=cmplx((Caa*f_spec2+Cab*g_spec2)*phipsi)	
			call dfftw_execute_dft(plan_conv_f2, fc2, fc2)
			f_temp=f_temp-f_spec_eq*fc2
		
			call dfftw_execute_dft(plan_de_conv,f_temp,f_temp)	
			f_temp=f_temp/(Nvx2*Nvy2*Nvz) 
			call fftshift3d(Nvx2, Nvy2, Nvz, f_temp)  
			!!!!! uniform fft in f_vz direction
			!$OMP PARALLEL do PRIVATE(lf,lv,fz_spec) 	
			do lv=1,Nvy2
			do lf=1,Nvx2			
				fz_spec=f_temp(lf,lv,:)		        
				call fftshift1d(Nvz,fz_spec) ! fftshift
				call dfftw_execute_dft(plan_fzf, fz_spec, fz_spec) 
				call fftshift1d(Nvz,fz_spec) ! fftshift
				f_spec1(lf,lv,:)=( fz_spec(Nvz/2+1:Nvz)-fz_spec(Nvz/2:1:-1) )
			enddo
			enddo
			!$OMP END PARALLEL do
		
			! nonuniform fft in f_vy direction
			!$OMP PARALLEL do PRIVATE(lf,lv) 	
			do lv=1,Nvy
				f_spec(:,lv,:)=cmplx(0.0)
				do lf=1,Nvy2        
					f_spec(:,lv,:)=f_spec(:,lv,:)+f_spec1(:,lf,:)*fre_fy(lv,lf)! *exp(-yii*fre_vy(lf)*vy(lv))
				enddo
			enddo
			!$OMP END PARALLEL do
			! nonuniform fft in f_vx direction
			!$OMP PARALLEL do PRIVATE(lf,lv,mu_temp) 	
			do lv=1,Nvx
				mu_temp=0.0
				do lf=1,Nvx2
					mu_temp=mu_temp+real(f_spec(lf,:,:)*fre_fx(lv,lf) )!*exp(-yii*fre_vx(lf)*vx(lv)) 
				enddo
				f(lv,:,:,lx,ly)=mu_temp
			enddo	
			!$OMP END PARALLEL do	
			f(:,:,:,lx,ly)=f(:,:,:,lx,ly)+f(:,Nvy:1:-1,:,lx,ly)+vz3*feq
			
						
				
			!////////////////////////// convolution, gain, g /////////////////////////////////
			f_temp=cmplx(0.0d0)

			!$OMP PARALLEL do PRIVATE(i,fc1,fc2) 
			do i=1,integral_loop	
                fc1=cmplx(g_spec2*phi(:,:,:,i))				
				call dfftw_execute_dft(plan_conv_f2, fc1, fc1)
				fc2=cmplx(g_spec2*psi(:,:,:,i))				
				call dfftw_execute_dft(plan_conv_f2, fc2, fc2)
				conv_ext(:,:,:,i)=Cbb*(gggain1(:,:,:,i)*fc2+gggain2(:,:,:,i)*fc1)
			enddo
			!$OMP END PARALLEL do

            !$OMP PARALLEL do PRIVATE(i,fc1,fc2) 
			do i=1,integral_loop*2*Mq	
                fc1=cmplx(g_spec2*alphaC3(:,:,:,i))				
				call dfftw_execute_dft(plan_conv_f2, fc1, fc1)
				fc2=cmplx(f_spec2*alphaC4(:,:,:,i))				
				call dfftw_execute_dft(plan_conv_f2, fc2, fc2)
				conv_ext(:,:,:,i+integral_loop)=Cab*(gfgain1(:,:,:,i)*fc2+gfgain2(:,:,:,i)*fc1)
			enddo
			!$OMP END PARALLEL do
				
			f_temp=sum(conv_ext,dim=4)
			fc2=cmplx((Cbb*g_spec2+Cab*f_spec2)*phipsi)	
			call dfftw_execute_dft(plan_conv_f2, fc2, fc2)
			f_temp=f_temp-g_spec_eq*fc2
		
			call dfftw_execute_dft(plan_de_conv,f_temp,f_temp)	
			f_temp=f_temp/(Nvx2*Nvy2*Nvz) 
			call fftshift3d(Nvx2, Nvy2, Nvz, f_temp)  
			!!!!! uniform fft in f_vz direction
			!$OMP PARALLEL do PRIVATE(lf,lv,fz_spec) 	
			do lv=1,Nvy2
			do lf=1,Nvx2			
				fz_spec=f_temp(lf,lv,:)		        
				call fftshift1d(Nvz,fz_spec) ! fftshift
				call dfftw_execute_dft(plan_fzf, fz_spec, fz_spec) 
				call fftshift1d(Nvz,fz_spec) ! fftshift
				f_spec1(lf,lv,:)=( fz_spec(Nvz/2+1:Nvz)-fz_spec(Nvz/2:1:-1) )
			enddo
			enddo
			!$OMP END PARALLEL do
		
			! nonuniform fft in f_vy direction
			!$OMP PARALLEL do PRIVATE(lf,lv) 	
			do lv=1,Nvy
				f_spec(:,lv,:)=cmplx(0.0)
				do lf=1,Nvy2        
					f_spec(:,lv,:)=f_spec(:,lv,:)+f_spec1(:,lf,:)*fre_fy(lv,lf)! *exp(-yii*fre_vy(lf)*vy(lv))
				enddo
			enddo
			!$OMP END PARALLEL do
			! nonuniform fft in f_vx direction
			!$OMP PARALLEL do PRIVATE(lf,lv,mu_temp) 	
			do lv=1,Nvx
				mu_temp=0.0
				do lf=1,Nvx2
					mu_temp=mu_temp+real(f_spec(lf,:,:)*fre_fx(lv,lf) )!*exp(-yii*fre_vx(lf)*vx(lv)) 
				enddo
				g(lv,:,:,lx,ly)=mu_temp
			enddo	
			!$OMP END PARALLEL do
			g(:,:,:,lx,ly)=g(:,:,:,lx,ly)+g(:,Nvy:1:-1,:,lx,ly)+vz3*geq
		
		enddo
		
		!!!!!!!!!!!!!!!!!!!!!! wall boundary
		! left wall
		f(Nvx/2+1:Nvx,:,:,1,1)=0!(1.0d0-accom)*finx(Nvx/2:1:-1,:,:,ly)
		g(Nvx/2+1:Nvx,:,:,1,1)=0	
		
		! streaming, second-order implicit scheme
		call streaming(Nvx,Nvy,Nvz,Nx,Ny,bx,cx,dx,bxn,cxn,dxn,vx3,vy3,mu,f)
	    call streaming(Nvx,Nvy,Nvz,Nx,Ny,bx,cx,dx,bxn,cxn,dxn,vx3,vy3,mug,g)
		
		!!!! macroscopic quantities
		!$OMP PARALLEL do PRIVATE(lx,ly,c2) 
		do ly=1,Ny
		do lx=1,Nx
			uzA2(lx,ly) =sum(f(:,:,:,lx,ly)*vz3*dV)/chi_A*4*sqrt(chi_A+chi_B*mB)	
			uzB2(lx,ly) =sum(g(:,:,:,lx,ly)*vz3*dV)/chi_B*4*sqrt(chi_A+chi_B*mB)
			c2=mA*(vx3**2.0+vy3**2.0+vz3**2.0)-2.5
			qzA(lx,ly)=sum(f(:,:,:,lx,ly)*c2*vz3*dV)*4*sqrt(chi_A+chi_B*mB)
			c2=mB*(vx3**2.0+vy3**2.0+vz3**2.0)-2.5
			qzB(lx,ly)=sum(g(:,:,:,lx,ly)*c2*vz3*dV)*4*sqrt(chi_A+chi_B*mB)
		enddo
		enddo
		!$OMP END PARALLEL do

		!!!! numerical error between two consective iteration steps
		error= max( sqrt( sum( (uzA2-uzA)**2.0*dXY ) / sum( uzA2**2.0*dXY ) ),  &
	                sqrt( sum( (uzB2-uzB)**2.0*dXY ) / sum( uzB2**2.0*dXY ) ) )
		!!!! update macroscopic quantities
		uzA=uzA2
		uzB=uzB2  	 

		!!!! display error, macroscopic quantities, etc
		call cpu_time(time_end) ! computation time
		write(*,"(I4,1x,f8.4,1x,ES10.1E2,1x,f8.3,1x,f8.3,1x,f8.3,1x,f8.3,ES10.1E2)") &
		iteration_step, delta, error,sum(uzB*dXY),sum(uzA*dXY),sum(qzB*dXY),sum(qzA*dXY),time_end-time_begin
		
		!!! solution converged
		if (error*delta<5E-5) then
			open(1,file=filename, access='append')
			write(1,'(I5,5f10.3)') iteration_step, delta, sum(uzB*dXY),sum(uzA*dXY),sum(qzB*dXY),sum(qzA*dXY)
			close(1)
			!
			open(2,file=filename2,access='append')
			
			ly=1
			do lx=1,Nx
			write(2,'(ES13.5E3,1x,f8.4,1x,f8.4,1x,f8.4)') x(lx), uzB(lx,ly),uzA(lx,ly),qzB(lx,ly),qzA(lx,ly)
			enddo    
			close(2)
			exit;	
        endif
	enddo	

 

!!!!!! 
write(*,*)"clean up"
call dfftw_destroy_plan(plan_fzb)
call dfftw_destroy_plan(plan_fzf)
call dfftw_destroy_plan(plan_conv_f1)
call dfftw_destroy_plan(plan_conv_f2)
call dfftw_destroy_plan(plan_de_conv)	 
  
end program	  
     	  

! /////////////////////////////////////////////////////////////
! this subroutine returns N equally spaced grid points in [a,b] 
subroutine linspace(a, b, N, y)
  implicit none
  integer*8 N,i
  real*8 y(N), a, b, h
  h=(b-a)/( N-1.0d0 )
  do i=1,N
  	y(i)=a+(i-1.0d0)*h
  enddo
  return
end
  
  
  
  
! /////////////////////////////////////////////////////////////
! fortran version of fftshift3d in matlab 
subroutine fftshift3d(Nvx, Nvy, Nvz, f)
  implicit none
  integer*8 Nvx,Nvy,Nvz
  integer*8 i,j,k, jj,kk
  complex*16 s
  complex*16 f(Nvx,Nvy,Nvz)
  
  do k=1,Nvz
  	if (k<=Nvz/2) then
  		kk=k+Nvz/2
  	else
  		kk=k-Nvz/2
  	endif
  	do j=1,Nvy
  		if (j<=Nvy/2) then
  			jj=j+Nvy/2
  		else
  		    jj=j-Nvy/2
  		endif
  		do i=1,Nvx/2
  			s=f(i,j,k)
  			f(i,j,k)=f(i+Nvx/2,jj,kk)
  			f(i+Nvx/2,jj,kk)=s
  		enddo
  	enddo
  enddo
  
  return
end

  
  
!///////////////////////////////////////// streaming
subroutine streaming(Nvx,Nvy,Nvz,Nx,Ny,bx,cx,dx,bxn,cxn,dxn,vx3,vy3,mu,f)
implicit none
integer*8 lx, ly, ix, iy, Nvx, Nvy, Nvz, Nx, Ny
real*8 f(1:Nvx,1:Nvy,1:Nvz/2,1:Nx,1:Ny)
real*8 vx3(1:Nvx,1:Nvy,1:Nvz/2), vy3(1:Nvx,1:Nvy,1:Nvz/2), mu(1:Nvx,1:Nvy,1:Nvz/2)
real*8 bx(Nx),  cx(Nx),  dx(Nx)
real*8 bxn(Nx), cxn(Nx), dxn(Nx)



!!!!----------------------------- streaming
lx=2; ly=1;
!$OMP PARALLEL do PRIVATE(ix,iy)
do iy=1,Nvy
do ix=Nvx/2+1,Nvx
	f(ix,iy,:,lx,ly)=( f(ix,iy,:,lx,ly)+vx3(ix,iy,:)*bx(lx)*f(ix,iy,:,lx-1,ly) )&
	                        /( dx(lx)*vx3(ix,iy,:)+mu(ix,iy,:) )  
enddo
enddo
!$OMP END PARALLEL do    
!    
	
do lx=3,Nx
!$OMP PARALLEL do PRIVATE(ix,iy)
do iy=1,Nvy
do ix=Nvx/2+1,Nvx
	f(ix,iy,:,lx,ly)=( f(ix,iy,:,lx,ly)+vx3(ix,iy,:)*(-bx(lx)*f(ix,iy,:,lx-1,ly)+cx(lx)*f(ix,iy,:,lx-2,ly)) )&
	                        /( dx(lx)*vx3(ix,iy,:)+mu(ix,iy,:) )
enddo
enddo
!$OMP END PARALLEL do    
enddo




!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
f(1:Nvx/2,:,:,Nx,1)=f(Nvx:Nvx/2+1:-1,:,:,Nx,1);

!!!! streaming,  negative vx 
lx=Nx-1
!$OMP PARALLEL do PRIVATE(ix,iy)
do iy=1,Nvy
do ix=1,Nvx/2
	f(ix,iy,:,lx,ly)=( f(ix,iy,:,lx,ly)+vx3(ix,iy,:)*bxn(lx)*f(ix,iy,:,lx+1,ly) )&
	                            /( dxn(lx)*vx3(ix,iy,:)+mu(ix,iy,:) )
enddo
enddo
!$OMP END PARALLEL do 
!   

do lx=Nx-2,1,-1
!$OMP PARALLEL do PRIVATE(ix,iy)
do iy=1,Nvy
do ix=1,Nvx/2
	f(ix,iy,:,lx,ly)=( f(ix,iy,:,lx,ly)+vx3(ix,iy,:)*(-bxn(lx)*f(ix,iy,:,lx+1,ly)+cxn(lx)*f(ix,iy,:,lx+2,ly)) )&	                                  
		                 /( dxn(lx)*vx3(ix,iy,:)+mu(ix,iy,:)  )     
enddo
enddo
!$OMP END PARALLEL do 
enddo

return
end	  
  
  
  
  
  

! /////////////////////////////////////////////////////////////
! fortran version of fftshift1d in matlab 
subroutine fftshift1d(Nvx, f)
implicit none
integer*8 Nvx,i
complex*16 s, f(1:Nvx)
do i=1,Nvx/2
	s=f(i)
	f(i)=f(i+Nvx/2)
	f(i+Nvx/2)=s
enddo

return
end

  

! /////////////////////////////////////////////////////////////
! this subroutine generates the 3d grids, like ndgrid in Matlab	
subroutine ndgrid(Nvx,Nvy,Nvz,vx,vy,vz,vx3,vy3,vz3)
implicit none
integer*8 Nvx,Nvy,Nvz, i, j, k
real*8 vx(Nvx), vy(Nvy), vz(Nvz)
real*8 vx3(Nvx,Nvy,Nvz), vy3(Nvx,Nvy,Nvz), vz3(Nvx,Nvy,Nvz)

do k=1,Nvz
do j=1,Nvy
	vx3(:,j,k)=vx
enddo
enddo	  

do k=1,Nvz
do i=1,Nvx
	vy3(i,:,k)=vy
enddo
enddo
  
do j=1,Nvy
do i=1,Nvx
	vz3(i,j,:)=vz
enddo
enddo	  

return	  
end    




! /////////////////////////////////////////////////////////////
! This subroutine is for computing the Legendre-Gauss nodes 'x' and weights 'w'
! on an interval [a,b] with truncation order N

! Rose you have a continuous function f(x) which is defined on [a,b]
! which you can evaluate at any x in [a,b]. Simply evaluate it at all of
! the values contained in the x vector to obtain a vector f. Then compute
! the definite integral using sum(f*w); 	  
subroutine lgwt(N,a,b,x,w)         
implicit none
integer*8 N, N1, N2, i, k 
real*8 x(N), w(N), a, b
real*8, allocatable :: y(:), y0(:), Lp(:), L(:,:)

N1=N
N2=N+1   	 
! 
allocate(y(N1))
allocate(y0(N1))
allocate(Lp(N1))
allocate(L(N1,N2))	  
! initial guess
do i=1, N1
y(i)=cos( (2.0d0*(i-1.0d0)+1.0d0)*4.0d0*atan(1.0d0)/(2.0d0*(N-1.0d0)+2.0d0)  )  &
		    +0.27d0/N1*sin(4.0d0*atan(1.0d0)*(-1.0d0+i*2.0d0/(N1-1.0d0))*(N-1.0d0)/N2 )
y0(i)=2.0d0
enddo	  
L=0.0d0
Lp=0.0d0	  
! compute the zeros of the N+1 legendre Polynomial 
! using the recursion relation and the Newton method	  
do while(maxval(abs(y-y0))>0.0000000000001)
L(:,1)=1.0d0
L(:,2)=y
do k=2,N1
	L(:,k+1)=( (2.0d0*k-1.0d0)*y*L(:,k)-(k-1)*L(:,k-1) )/k
enddo
Lp=N2*( L(:,N1)-y*L(:,N2) )/(1.0d0-y*y)
y0=y
y=y0-L(:,N2)/Lp
enddo	  
! linear map from [-1 1] to [a,b]
x=( a*(1.0d0-y)+b*(1.0d0+y) )/2.0d0
w=N2*N2*(b-a)/( (1.0d0-y*y)*Lp*Lp ) /N1/N1
! destroy
deallocate(y0)  
deallocate(Lp)  
deallocate(L)  	  
return	  
end

  



  

