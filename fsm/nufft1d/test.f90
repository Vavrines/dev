program main
    implicit none

    integer i,ier,iflag,j,k1,ms,nj
      integer, parameter::mx=10000
      real*8 xj(mx), sk(mx)
      real*8 err,eps
      real(kind=8), parameter::pi=3.141592653589793238462643383279502884197d0
      complex*16 cj(mx),cj0(mx),cj1(mx)
      complex*16 fk0(mx),fk1(mx)

      ms = 90
      nj = 128
      do k1 = -nj/2, (nj-1)/2
         j = k1+nj/2+1
         xj(j) = pi * dcos(-pi*j/nj)
         cj(j) = dcmplx( dsin(pi*j/nj), dcos(pi*j/nj))
      enddo

      iflag = 1

       eps=1d-8


         call dirft1d1(nj,xj,cj,iflag, ms,fk0)
         call nufft1d1f90(nj,xj,cj,iflag,eps, ms,fk1,ier)


      write(*,*) fk1

















    !integer::i,ier,iflag,j,k1,ms,nj
    !integer, parameter::mx=4
    !real(kind=8)::xj(mx), sk(mx)
    !real(kind=8)::err,eps
    !real(kind=8), parameter::pi=3.141592653589793238462643383279502884197d0
    !complex*16 cj(mx),cj0(mx),cj1(mx)
    !complex*16 fk0(mx),fk1(mx)

    !nj = 4
    !cj = [dcmplx(-0.8256139158911004, 0.0), dcmplx(0.5564603728937826, 0.0), dcmplx(0.5605560968085124, 0.0), dcmplx(-1.5532287948012704, 0.0)]

    !xj = ([0.0, 0.25, 0.5, 0.75])

    !iflag = -1
    !ms = 4

    !call dirft1d2(nj,xj,cj,iflag, ms,fk0)

    !write(*,*) cj
    !write(*,*) fk0

end