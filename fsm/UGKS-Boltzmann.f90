    !--------------------------------------------------
    !>store the global variables
    !--------------------------------------------------
    module global_data
    !--------------------------------------------------
    !kind selection
    !--------------------------------------------------
    integer,parameter :: RKD = 8
    integer,parameter :: CKD = 8
    integer,parameter :: IKD = 4

    !--------------------------------------------------
    !variables to control the simulation
    !--------------------------------------------------
    integer, parameter :: M=5
    real(kind=RKD),parameter :: PI = 4.0*atan(1.0) !Pi
    real(kind=RKD),parameter :: SMV = tiny(real(1.0,8)) !small value to avoid 0/0
    real(kind=RKD),parameter :: UP = 1.0 !used in sign() function
    real(kind=RKD) :: cfl !global CFL number
    real(kind=RKD) :: dt !global time step
    real(kind=RKD) :: sim_time !current simulation time
    real(kind=RKD) :: max_time !maximum simulation time
    real(kind=RKD) :: xmid
    character(len=11),parameter :: HSTFILENAME = "8.1mfpb.hst" !history file name
    character(len=11),parameter :: RSTFILENAME = "8.1mfpb.plt" !result file name
    integer :: iter !iteration
    integer :: method_output !output the solution with normalized value or not
    integer :: finish

    !--------------------------------------------------
    !gas properties
    !--------------------------------------------------
    real(kind=RKD) :: gam !ratio of specific heat
    real(kind=RKD) :: omega,alpha,eta !temperature dependence index in HS/VHS/VSS model
    real(kind=RKD) :: pr !Prandtl number
    real(kind=RKD) :: mu_ref !viscosity coefficient in reference state
    real(kind=RKD) :: kn,kn_bzm !Knudsen number in reference state
    integer :: ck !internal degree of freedom

    !--------------------------------------------------
    !macros for a readable code
    !--------------------------------------------------
    !I/O
    integer,parameter :: HSTFILE = 20 !history file ID
    integer,parameter :: RSTFILE = 21 !result file ID
    !output method
    integer,parameter :: ORIGINAL = 0 !do not normalize the solution
    integer,parameter :: NORMALIZE = 1 !normalize the solution

    !--------------------------------------------------
    !basic derived type
    !--------------------------------------------------
    !cell center
    type :: cell_center
        !geometry
        real(kind=RKD) :: x !cell center coordinates
        real(kind=RKD) :: length !length
        !flow field
        real(kind=RKD) :: w(3) !density, x-momentum,total energy
        real(kind=RKD) :: primary(3) !density,temperature
        real(kind=RKD),allocatable,dimension(:,:,:) :: f !distribution function
        real(kind=RKD),allocatable,dimension(:,:,:) :: sf !distribution function
        real(kind=RKD),allocatable,dimension(:,:,:) :: Q !slope of distribution function
    end type cell_center

    !cell interface
    type :: cell_interface
        real(kind=RKD) :: flux(3) !mass flux, x momentum flux, energy flux
        real(kind=RKD),allocatable,dimension(:,:,:) :: flux_f!flux of distribution function
    end type cell_interface

    !--------------------------------------------------
    !flow field
    !--------------------------------------------------
    !index method
    !     ----------------
    !  (i)|      (i)     |(i+1)
    !     ----------------
    integer :: ixmin,ixmax,midnode !index range
    type(cell_center),allocatable,dimension(:) :: ctr !cell centers
    type(cell_interface),allocatable,dimension(:) :: vface !vertical interfaces

    !--------------------------------------------------
    !discrete velocity space
    !--------------------------------------------------
    integer :: unum,vnum,wnum
    real(kind=RKD) ::  du,dv,dw
    real(kind=RKD) :: umax,vmax,wmax
    real(kind=RKD),allocatable,dimension(:,:,:) :: weight
    real(kind=RKD),allocatable,dimension(:,:,:) :: uspace,vspace,wspace

    !--------------------------------------------------
    !>kernel mode in Boltzmann collision term
    !--------------------------------------------------
    real(kind=RKD),allocatable,dimension(:,:,:) :: phipsi
    real(kind=RKD),allocatable,dimension(:,:,:,:) :: phi, psi
    end module global_data

    !--------------------------------------------------
    !>define some commonly used functions/subroutines
    !--------------------------------------------------
    module tools
    use global_data
    implicit none
    contains
    !--------------------------------------------------
    !>obtain discretized Maxwellian distribution
    !>@param[out] h,b  :distribution function
    !>@param[in]  prim :primary variables
    !--------------------------------------------------
    subroutine discrete_maxwell(f,prim)
    real(kind=RKD),dimension(:,:,:),intent(out) :: f
    real(kind=RKD),intent(in) :: prim(3)

    f = prim(1)*(prim(3)/PI)**(3.0/2.0)*exp(-prim(3)*((uspace-prim(2))**2+vspace**2+wspace**2))
    end subroutine discrete_maxwell

    !--------------------------------------------------
    !>calculate the Shakhov part H^+, B^+
    !>@param[in]  H,B           :Maxwellian distribution function
    !>@param[in]  qf            :heat flux
    !>@param[in]  prim          :primary variables
    !>@param[out] H_plus,B_plus :Shakhov part
    !--------------------------------------------------
    subroutine shakhov_part(f,qf,prim,f_plus)
    real(kind=RKD),dimension(:,:,:),intent(in) :: f
    real(kind=RKD),intent(in) :: qf
    real(kind=RKD),intent(in) :: prim(3)
    real(kind=RKD),dimension(:,:,:),intent(out) :: f_plus

    f_plus = 0.8*(1-pr)*prim(3)**2/prim(1)*&
        (uspace-prim(2))*qf*(2*prim(3)*((uspace-prim(2))**2+vspace**2+wspace**2)-5)*f
    end subroutine shakhov_part

    !--------------------------------------------------
    !>convert primary variables to conservative variables
    !>@param[in] prim          :primary variables
    !>@return    get_conserved :conservative variables
    !--------------------------------------------------
    function get_conserved(prim)
    real(kind=RKD),intent(in) :: prim(3)
    real(kind=RKD) :: get_conserved(3)

    get_conserved(1) = prim(1)
    get_conserved(2) = prim(1)*prim(2)
    get_conserved(3) = 0.5*prim(1)/prim(3)/(gam-1.0)+0.5*prim(1)*prim(2)**2
    end function get_conserved

    !--------------------------------------------------
    !>convert conservative variables to primary variables
    !>@param[in] w           :conservative variables
    !>@return    get_primary :conservative variables
    !--------------------------------------------------
    function get_primary(w)
    real(kind=RKD),intent(in) :: w(3)
    real(kind=RKD) :: get_primary(3) !primary variables

    get_primary(1) = w(1)
    get_primary(2) = w(2)/w(1)
    get_primary(3) = 0.5*w(1)/(gam-1.0)/(w(3)-0.5*w(2)**2/w(1))
    end function get_primary

    !--------------------------------------------------
    !>obtain ratio of specific heat
    !>@param[in] ck        :internal degree of freedom
    !>@return    get_gamma :ratio of specific heat
    !--------------------------------------------------
    function get_gamma(ck)
    integer,intent(in) :: ck
    real(kind=RKD) :: get_gamma

    get_gamma = float(ck+5)/float(ck+3)
    end function get_gamma
    !--------------------------------------------------
    !>obtain ratio of specific heat
    !>@param[in] ck        :internal degree of freedom
    !>@return    get_gamma :ratio of specific heat
    !--------------------------------------------------
    subroutine get_potential(omega)
    real(kind=RKD),intent(in) :: omega

    !interatomic potential parameters
    if(omega==0.5d0) then
        alpha=1.0d0
    else
        eta=4.0d0/(2.0d0*omega-1.0d0)+1.0d0
        alpha=(eta-5.0d0)/(eta-1.0d0)
    endif
    end subroutine get_potential
    !--------------------------------------------------
    !>obtain speed of sound
    !>@param[in] prim    :primary variables
    !>@return    get_sos :speed of sound
    !--------------------------------------------------
    function get_sos(prim)
    real(kind=RKD),intent(in) :: prim(3)
    real(kind=RKD) :: get_sos !speed of sound

    get_sos = sqrt(0.5*gam/prim(3))
    end function get_sos

    !--------------------------------------------------
    !>calculate collision time
    !>@param[in] prim    :primary variables
    !>@return    get_tau :collision time
    !--------------------------------------------------
    function get_tau(prim)
    real(kind=RKD),intent(in) :: prim(3)
    real(kind=RKD) :: get_tau

    get_tau = mu_ref*2*prim(3)**(1-omega)/prim(1)
    end function get_tau

    !--------------------------------------------------
    !>get heat flux
    !>@param[in] h,b           :distribution function
    !>@param[in] prim          :primary variables
    !>@return    get_heat_flux :heat flux in normal and tangential direction
    !--------------------------------------------------
    function get_heat_flux(f,prim)
    real(kind=RKD),dimension(:,:,:),intent(in) :: f
    real(kind=RKD),intent(in) :: prim(3)
    real(kind=RKD) :: get_heat_flux !heat flux in normal and tangential direction

    get_heat_flux = 0.5*sum(weight*(uspace-prim(2))*((uspace-prim(2))**2+vspace**2+wspace**2)*f)
    end function get_heat_flux

    !--------------------------------------------------
    !>get temperature
    !>@param[in] h,b             :distribution function
    !>@param[in] prim            :primary variables
    !>@return    get_temperature :temperature
    !--------------------------------------------------
    function get_temperature(f,prim)
    real(kind=RKD),dimension(:,:,:),intent(in) :: f
    real(kind=RKD),intent(in) :: prim(3)
    real(kind=RKD) :: get_temperature !pressure

    get_temperature = 2.0*sum(weight*((uspace-prim(2))**2+vspace**2+wspace**2)*f)/(ck+3)/prim(1)
    end function get_temperature

    !--------------------------------------------------
    !>get the nondimensionalized viscosity coefficient
    !>@param[in] kn          :Knudsen number
    !>@param[in] alpha,omega :index related to HS/VHS/VSS model
    !>@return    get_mu      :nondimensionalized viscosity coefficient
    !--------------------------------------------------
    function get_mu(alpha,omega)
    real(kind=RKD),intent(in) :: alpha,omega
    real(kind=RKD) :: get_mu

    get_mu = 5*(alpha+1)*(alpha+2)*sqrt(PI)/(4*alpha*(5-2*omega)*(7-2*omega))*kn
    end function get_mu
    !--------------------------------------------------
    !>get the nondimensionalized viscosity coefficient
    !>@param[in] kn          :Knudsen number
    !>@param[in] alpha,omega :index related to HS/VHS/VSS model
    !>@return    get_mu      :nondimensionalized viscosity coefficient
    !--------------------------------------------------
    function get_kn_bzm(mu_ref)
    real(kind=RKD),intent(in) :: mu_ref
    real(kind=RKD) :: get_kn_bzm

    get_kn_bzm=16*pi*sqrt(2.0)**alpha*gamma((alpha+3)/2)*gamma(real(2.0,kind=RKD))*&
        (alpha+1)*(alpha+2)/(7-2*omega)/(5-2*omega)/alpha*kn
    get_kn_bzm=64*sqrt(2.0)**alpha/5.0*gamma((alpha+3)/2)*gamma(real(2.0,kind=RKD))*&
        sqrt(pi)*mu_ref
    end function get_kn_bzm
    !-----------------------------------------------------------------------------
    !>this subroutine returns N equally spaced grid points in [a,b]
    !-----------------------------------------------------------------------------
    subroutine linspace(a, b, N, y)
    implicit none
    integer :: N,i
    real(kind=RKD) ::  y(N), a, b, h
    h=(b-a)/( N-1.0d0 )
    do i=1,N
        y(i)=a+(i-1.0d0)*h
    enddo
    return
    end subroutine linspace
    !-----------------------------------------------------------------------------
    !>fortran version of fftshift3d in matlab
    !-----------------------------------------------------------------------------
    subroutine fftshift3d(f)
    implicit none
    integer(kind=RKD) ::  i,j,k, jj,kk
    complex(kind=CKD) ::  s
    complex(kind=CKD),intent(inout) ::  f(unum,vnum,wnum)

    do k=1,wnum
        if (k<=wnum/2) then
            kk=k+wnum/2
        else
            kk=k-wnum/2
        endif
        do j=1,vnum
            if (j<=vnum/2) then
                jj=j+vnum/2
            else
                jj=j-vnum/2
            endif
            do i=1,unum/2
                s=f(i,j,k)
                f(i,j,k)=f(i+unum/2,jj,kk)
                f(i+unum/2,jj,kk)=s
            enddo
        enddo
    enddo

    return
    end subroutine fftshift3d
    !-----------------------------------------------------------------------------
    !>This subroutine is for computing the Legendre-Gauss nodes 'x' and weights 'w'
    !>on an interval [a,b] with truncation order N
    !>Suppose you have a continuous function f(x) which is defined on [a,b]
    !>which you can evaluate at any x in [a,b]. Simply evaluate it at all of
    !>the values contained in the x vector to obtain a vector f. Then compute
    !>the definite integral using sum(f.*w);
    !-----------------------------------------------------------------------------
    subroutine lgwt(N,a,b,x,w)
    implicit none
    integer(kind=RKD) ::  N, N1, N2, i, k
    real(kind=RKD) ::  x(N), w(N), a, b
    real(kind=RKD),allocatable :: y(:), y0(:), Lp(:), L(:,:)

    N1=N
    N2=N+1
    !
    allocate(y(N1))
    allocate(y0(N1))
    allocate(Lp(N1))
    allocate(L(N1,N2))
    ! initial guess
    do i=1, N1
        y(i)=cos( (2.0d0*(i-1.0d0)+1.0d0)*4.0d0*atan(1.0d0)/(2.0d0*(N-1.0d0)+2.0d0)  )  &
            +0.27d0/N1*sin(4.0d0*atan(1.0d0)*(-1.0d0+i*2.0d0/(N1-1.0d0))*(N-1.0d0)/N2 )
        y0(i)=2.0d0
    enddo
    L=0.0d0
    Lp=0.0d0
    ! compute the zeros of the N+1 legendre Polynomial
    ! using the recursion relation and the Newton method
    do while(maxval(abs(y-y0))>0.0000000000001)
        L(:,1)=1.0d0
        L(:,2)=y
        do k=2,N1
            L(:,k+1)=( (2.0d0*k-1.0d0)*y*L(:,k)-(k-1)*L(:,k-1) )/k
        enddo
        Lp=N2*( L(:,N1)-y*L(:,N2) )/(1.0d0-y*y)
        y0=y
        y=y0-L(:,N2)/Lp
    enddo
    ! linear map from [-1 1] to [a,b]
    x=( a*(1.0d0-y)+b*(1.0d0+y) )/2.0d0
    w=N2*N2*(b-a)/( (1.0d0-y*y)*Lp*Lp ) /N1/N1
    ! destroy
    deallocate(y0)
    deallocate(Lp)
    deallocate(L)
    return
    end subroutine lgwt
    !--------------------------------------------------
    !--calculates the gamma function of x.
    !--------------------------------------------------
    function gamma(x)
    real(kind=RKD),intent(in) :: x
    real(kind=RKD) :: gamma,a,y
    a=1.
    y=x
    if (y < 1.) then
        a=a/y
    else
        y=y-1.
        do while (y >= 1.)
            a=a*y
            y=y-1.
        end do
    end if
    gamma=a*(1.-0.5748646*y+0.9512363*y**2-0.6998588*y**3+0.4245549*y**4-0.1010678*y**5)

    end function gamma

    end module tools

    !--------------------------------------------------
    !>flux calculation
    !--------------------------------------------------
    module flux
    use global_data
    use tools
    implicit none

    integer,parameter :: MNUM = 6 !number of normal velocity moments
    integer,parameter :: MTUM = 4 !number of tangential velocity moments

    contains
    !--------------------------------------------------
    !>calculate flux of inner interface
    !>@param[in]    cell_L :cell left to the target interface
    !>@param[inout] face   :the target interface
    !>@param[in]    cell_R :cell right to the target interface
    !--------------------------------------------------
    subroutine calc_flux_multiscale(cell_L,face,cell_R)
    type(cell_center),intent(in) :: cell_L,cell_R
    type(cell_interface),intent(inout) :: face
    real(kind=RKD),allocatable,dimension(:,:,:) :: f !distribution function at the interface
    real(kind=RKD),allocatable,dimension(:,:,:) :: g !Maxwellian distribution function
    real(kind=RKD),allocatable,dimension(:,:,:) :: g_plus !Shakhov part of the equilibrium distribution
    real(kind=RKD),allocatable,dimension(:,:,:) :: sf !slope of distribution function at the interface
    integer,allocatable,dimension(:,:,:) :: delta !Heaviside step function
    real(kind=RKD) :: w(3),prim(3) !conservative and primary variables at the interface
    real(kind=RKD) :: qf !heat flux in normal and tangential direction
    real(kind=RKD) :: sw(3) !slope of W
    real(kind=RKD) :: aL(3),aR(3),aT(3) !micro slope of Maxwellian distribution, left,right and time.
    real(kind=RKD) :: Mu(0:MNUM),Mu_L(0:MNUM),Mu_R(0:MNUM),Mxi(0:2) !<u^n>,<u^n>_{>0},<u^n>_{<0},<\xi^l>
    real(kind=RKD) :: Mau_0(3),Mau_L(3),Mau_R(3),Mau_T(3) !<u\psi>,<aL*u^n*\psi>,<aR*u^n*\psi>,<A*u*\psi>
    real(kind=RKD) :: tau !collision time
    real(kind=RKD) :: Mt(5) !some time integration terms
    integer :: i,j

    !--------------------------------------------------
    !prepare
    !--------------------------------------------------
    !allocate array
    allocate(delta(unum,vnum,wnum))
    allocate(f(unum,vnum,wnum))
    allocate(sf(unum,vnum,wnum))
    allocate(g(unum,vnum,wnum))
    allocate(g_plus(unum,vnum,wnum))

    !Heaviside step function
    delta = (sign(UP,uspace)+1)/2

    !--------------------------------------------------
    !reconstruct initial distribution
    !--------------------------------------------------
    f = (cell_L%f+0.5*cell_L%length*cell_L%sf)*delta+&
        (cell_R%f-0.5*cell_R%length*cell_R%sf)*(1-delta)
    sf = cell_L%sf*delta+cell_R%sf*(1-delta)

    !--------------------------------------------------
    !obtain macroscopic variables
    !--------------------------------------------------
    !conservative variables w_0
    w(1) = sum(weight*f)
    w(2) = sum(weight*uspace*f)
    w(3) = 0.5*(sum(weight*(uspace**2+vspace**2+wspace**2)*f))

    !convert to primary variables
    prim = get_primary(w)

    !heat flux
    qf = get_heat_flux(f,prim)

    !--------------------------------------------------
    !calculate a^L,a^R
    !--------------------------------------------------
    sw = (w-cell_L%w)/(0.5*cell_L%length) !left slope of W
    aL = micro_slope(prim,sw) !calculate a^L

    sw = (cell_R%w-w)/(0.5*cell_R%length) !right slope of W
    aR = micro_slope(prim,sw) !calculate a^R

    !--------------------------------------------------
    !calculate time slope of W and A
    !--------------------------------------------------
    !<u^n>,<\xi^l>,<u^n>_{>0},<u^n>_{<0}
    call calc_moment_u(prim,Mu,Mxi,Mu_L,Mu_R)

    Mau_L = moment_au(aL,Mu_L,Mxi,1) !<aL*u*\psi>_{>0}
    Mau_R = moment_au(aR,Mu_R,Mxi,1) !<aR*u*\psi>_{<0}

    sw = -prim(1)*(Mau_L+Mau_R) !time slope of W
    aT = micro_slope(prim,sw) !calculate A

    !--------------------------------------------------
    !calculate collision time and some time integration terms
    !--------------------------------------------------
    tau = get_tau(prim)

    Mt(4) = tau*(1.0-exp(-dt/tau))
    Mt(5) = -tau*dt*exp(-dt/tau)+tau*Mt(4)
    Mt(1) = dt-Mt(4)
    Mt(2) = -tau*Mt(1)+Mt(5)
    Mt(3) = dt**2/2.0-tau*Mt(1)

    !--------------------------------------------------
    !calculate the flux of conservative variables related to g0
    !--------------------------------------------------
    Mau_0 = moment_uv(Mu,Mxi,1,0) !<u*\psi>
    Mau_L = moment_au(aL,Mu_L,Mxi,2) !<aL*u^2*\psi>_{>0}
    Mau_R = moment_au(aR,Mu_R,Mxi,2) !<aR*u^2*\psi>_{<0}
    Mau_T = moment_au(aT,Mu,Mxi,1) !<A*u*\psi>

    face%flux = Mt(1)*prim(1)*Mau_0+Mt(2)*prim(1)*(Mau_L+Mau_R)+Mt(3)*prim(1)*Mau_T

    !--------------------------------------------------
    !calculate the flux of conservative variables related to g+ and f0
    !--------------------------------------------------
    !Maxwellian distribution H0 and B0
    call discrete_maxwell(g,prim)

    !Shakhov part H+ and B+
    call shakhov_part(g,qf,prim,g_plus)

    !macro flux related to g+ and f0
    face%flux(1) = face%flux(1)+Mt(1)*sum(weight*uspace*g_plus)+Mt(4)*sum(weight*uspace*f)-Mt(5)*sum(weight*uspace**2*sf)
    face%flux(2) = face%flux(2)+Mt(1)*sum(weight*uspace**2*g_plus)+Mt(4)*sum(weight*uspace**2*f)-Mt(5)*sum(weight*uspace**3*sf)
    face%flux(3) = face%flux(3)+&
        Mt(1)*0.5*(sum(weight*uspace*(uspace**2+vspace**2+wspace**2)*g_plus))+&
        Mt(4)*0.5*(sum(weight*uspace*(uspace**2+vspace**2+wspace**2)*f))-&
        Mt(5)*0.5*(sum(weight*uspace**2*(uspace**2+vspace**2+wspace**2)*sf))

    !--------------------------------------------------
    !calculate flux of distribution function
    !--------------------------------------------------
    face%flux_f = Mt(1)*uspace*(g+g_plus)+&
        Mt(2)*uspace**2*(aL(1)*g+aL(2)*uspace*g+0.5*aL(3)*((uspace**2+vspace**2+wspace**2)*g))*delta+&
        Mt(2)*uspace**2*(aR(1)*g+aR(2)*uspace*g+0.5*aR(3)*((uspace**2+vspace**2+wspace**2)*g))*(1-delta)+&
        Mt(3)*uspace*(aT(1)*g+aT(2)*uspace*g+0.5*aT(3)*((uspace**2+vspace**2+wspace**2)*g))+&
        Mt(4)*uspace*f-Mt(5)*uspace**2*sf

    end subroutine calc_flux_multiscale
    !--------------------------------------------------
    !>calculate flux of inner interface
    !>@param[in]    cell_L :cell left to the target interface
    !>@param[inout] face   :the target interface
    !>@param[in]    cell_R :cell right to the target interface
    !--------------------------------------------------
    subroutine calc_flux_kfvs(cell_L,face,cell_R)
    type(cell_center),intent(in) :: cell_L,cell_R
    type(cell_interface),intent(inout) :: face
    real(kind=RKD),allocatable,dimension(:,:,:) :: f
    real(kind=RKD),allocatable,dimension(:,:,:) :: sf
    integer,allocatable,dimension(:,:,:) :: delta

    allocate(delta(unum,vnum,wnum))
    allocate(f(unum,vnum,wnum))
    allocate(sf(unum,vnum,wnum))

    delta = (sign(UP,uspace)+1)/2
    f = (cell_L%f+0.5*cell_L%length*cell_L%sf)*delta+(cell_R%f-0.5*cell_R%length*cell_R%sf)*(1-delta)
    sf = cell_L%sf*delta+cell_R%sf*(1-delta)
    face%flux_f = dt*uspace*f-0.5*dt**2*uspace**2*sf
    end subroutine calc_flux_kfvs

    !--------------------------------------------------
    !>calculate micro slope of Maxwellian distribution
    !>@param[in] prim        :primary variables
    !>@param[in] sw          :slope of W
    !>@return    micro_slope :slope of Maxwellian distribution
    !--------------------------------------------------
    function micro_slope(prim,sw)
    real(kind=RKD),intent(in) :: prim(3),sw(3)
    real(kind=RKD) :: micro_slope(3)

    micro_slope(3) = 4.0*prim(3)**2/(ck+1)/prim(1)*(2.0*sw(3)-2.0*prim(2)*sw(2)+sw(1)*(prim(2)**2-0.5*(ck+1)/prim(3)))
    micro_slope(2) = 2.0*prim(3)/prim(1)*(sw(2)-prim(2)*sw(1))-prim(2)*micro_slope(3)
    micro_slope(1) = sw(1)/prim(1)-prim(2)*micro_slope(2)-0.5*(prim(2)**2+0.5*(ck+1)/prim(3))*micro_slope(3)
    end function micro_slope

    !--------------------------------------------------
    !>calculate moments of velocity
    !>@param[in] prim :primary variables
    !>@param[out] Mu        :<u^n>
    !>@param[out] Mxi       :<\xi^l>
    !>@param[out] Mu_L,Mu_R :<u^n>_{>0},<u^n>_{<0}
    !--------------------------------------------------
    subroutine calc_moment_u(prim,Mu,Mxi,Mu_L,Mu_R)
    real(kind=RKD),intent(in) :: prim(3)
    real(kind=RKD),intent(out) :: Mu(0:MNUM),Mu_L(0:MNUM),Mu_R(0:MNUM)
    real(kind=RKD),intent(out) :: Mxi(0:2)
    integer :: i

    !moments of normal velocity
    Mu_L(0) = 0.5*erfc(-sqrt(prim(3))*prim(2))
    Mu_L(1) = prim(2)*Mu_L(0)+0.5*exp(-prim(3)*prim(2)**2)/sqrt(PI*prim(3))
    Mu_R(0) = 0.5*erfc(sqrt(prim(3))*prim(2))
    Mu_R(1) = prim(2)*Mu_R(0)-0.5*exp(-prim(3)*prim(2)**2)/sqrt(PI*prim(3))

    do i=2,MNUM
        Mu_L(i) = prim(2)*Mu_L(i-1)+0.5*(i-1)*Mu_L(i-2)/prim(3)
        Mu_R(i) = prim(2)*Mu_R(i-1)+0.5*(i-1)*Mu_R(i-2)/prim(3)
    end do

    Mu = Mu_L+Mu_R

    !moments of \xi
    Mxi(0) = 1.0 !<\xi^0>
    Mxi(1) = 0.5*(ck+2)/prim(3) !<\xi^2>
    Mxi(2) = ((ck+2)**2+2.0*(ck+2))/(4.0*prim(3)**2) !<\xi^4>
    end subroutine calc_moment_u

    !--------------------------------------------------
    !>calculate <u^\alpha*\xi^\delta*\psi>
    !>@param[in] Mu        :<u^\alpha>
    !>@param[in] Mxi       :<\xi^l>
    !>@param[in] alpha     :exponential index of u
    !>@param[in] delta     :exponential index of \xi
    !>@return    moment_uv :moment of <u^\alpha*\xi^\delta*\psi>
    !--------------------------------------------------
    function moment_uv(Mu,Mxi,alpha,delta)
    real(kind=RKD),intent(in) :: Mu(0:MNUM),Mxi(0:2)
    integer,intent(in) :: alpha,delta
    real(kind=RKD) :: moment_uv(3)

    moment_uv(1) = Mu(alpha)*Mxi(delta/2)
    moment_uv(2) = Mu(alpha+1)*Mxi(delta/2)
    moment_uv(3) = 0.5*(Mu(alpha+2)*Mxi(delta/2)+Mu(alpha)*Mxi((delta+2)/2))
    end function moment_uv

    !--------------------------------------------------
    !>calculate <a*u^\alpha*\psi>
    !>@param[in] a         :micro slope of Maxwellian
    !>@param[in] Mu        :<u^\alpha>
    !>@param[in] Mxi       :<\xi^l>
    !>@param[in] alpha     :exponential index of u
    !>@return    moment_au :moment of <a*u^\alpha*\psi>
    !--------------------------------------------------
    function moment_au(a,Mu,Mxi,alpha)
    real(kind=RKD),intent(in) :: a(3)
    real(kind=RKD),intent(in) :: Mu(0:MNUM),Mxi(0:2)
    integer,intent(in) :: alpha
    real(kind=RKD) :: moment_au(3)

    moment_au = a(1)*moment_uv(Mu,Mxi,alpha+0,0)+&
        a(2)*moment_uv(Mu,Mxi,alpha+1,0)+&
        0.5*a(3)*moment_uv(Mu,Mxi,alpha+2,0)+&
        0.5*a(3)*moment_uv(Mu,Mxi,alpha+0,2)
    end function moment_au
    end module flux

    !--------------------------------------------------
    !>UGKS solver
    !--------------------------------------------------
    module solver
    use global_data
    use tools
    use flux
    implicit none
    contains
    !--------------------------------------------------
    !>calculate time step
    !--------------------------------------------------
    subroutine timestep()
    real(kind=RKD) :: tmax !max 1/dt allowed
    real(kind=RKD) :: sos !speed of sound
    real(kind=RKD) :: prim(3) !primary variables
    integer :: i

    !set initial value
    tmax = 0.0

    !$omp parallel
    !$omp do private(i,sos,prim) reduction(max:tmax)
    do i=ixmin,ixmax
        !convert conservative variables to primary variables
        prim = get_primary(ctr(i)%w)

        !sound speed
        sos = get_sos(prim)

        !maximum velocity
        prim(2) = max(umax,abs(prim(2)))+sos

        !maximum 1/dt allowed
        tmax = max(tmax,prim(2)/ctr(i)%length)
    end do
    !$omp end do
    !$omp end parallel

    !time step
    dt = cfl/tmax
    end subroutine timestep

    !--------------------------------------------------
    !>calculate the slope of distribution function
    !--------------------------------------------------
    subroutine interpolation()
    integer :: i

    call interp_boundary(ctr(ixmin),ctr(ixmin),ctr(ixmin+1))
    call interp_boundary(ctr(ixmax),ctr(ixmax-1),ctr(ixmax))

    !$omp parallel
    !$omp do
    do i=ixmin+1,ixmax-1
        call interp_inner(ctr(i-1),ctr(i),ctr(i+1))
    end do
    !$omp end do nowait
    !$omp end parallel
    end subroutine interpolation

    !--------------------------------------------------
    !>calculate the flux across the interfaces
    !--------------------------------------------------
    subroutine evolution()
    integer :: i

    !$omp parallel
    !$omp do
    do i=ixmin,ixmax+1 !with ghost cell
        call calc_flux_multiscale(ctr(i-1),vface(i),ctr(i))
    end do
    !$omp end do nowait
    !$omp do
    do i=ixmin,ixmax
        call calc_collision_term(ctr(i))
    end do
    !$omp end do nowait
    !$omp end parallel
    end subroutine evolution

    !--------------------------------------------------
    !>update cell averaged values
    !--------------------------------------------------
    subroutine update()
    real(kind=RKD),allocatable,dimension(:,:,:) :: g !equilibrium distribution at t=t^{n+1}
    real(kind=RKD) :: w_old(3) !conservative variables at t^n
    real(kind=RKD) :: prim(3) !primary variables at t^n and t^{n+1}
    real(kind=RKD) :: tau !collision time and t^n and t^{n+1}
    integer :: i

    !allocate arrays
    allocate(g(unum,vnum,wnum))

    !set initial value
    do i=ixmin,ixmax
        !--------------------------------------------------
        !update W^{n+1} and calculate H^{n+1},B^{n+1},\tau^{n+1}
        !--------------------------------------------------
        ctr(i)%w = ctr(i)%w+(vface(i)%flux-vface(i+1)%flux)/ctr(i)%length !update W^{n+1}

        prim = get_primary(ctr(i)%w)
        call discrete_maxwell(g,prim)
        tau = get_tau(prim)

        !--------------------------------------------------
        !update distribution function
        !--------------------------------------------------
        ctr(i)%f = (ctr(i)%f+(vface(i)%flux_f-vface(i+1)%flux_f)/ctr(i)%length+&
            dt*(g/tau*(1-exp(-dt/tau))+ctr(i)%Q*exp(-dt/tau)))/(1.0+dt/tau*(1-exp(-dt/tau)))
    end do
    end subroutine update

    !--------------------------------------------------
    !>one-sided interpolation of the boundary cell
    !>@param[inout] cell_N :the target boundary cell
    !>@param[inout] cell_L :the left cell
    !>@param[inout] cell_R :the right cell
    !--------------------------------------------------
    subroutine interp_boundary(cell_N,cell_L,cell_R)
    type(cell_center),intent(inout) :: cell_N
    type(cell_center),intent(inout) :: cell_L,cell_R

    cell_N%sf = (cell_R%f-cell_L%f)/(0.5*cell_R%length+0.5*cell_L%length)
    end subroutine interp_boundary

    !--------------------------------------------------
    !>interpolation of the inner cells
    !>@param[in]    cell_L :the left cell
    !>@param[inout] cell_N :the target cell
    !>@param[in]    cell_R :the right cell
    !--------------------------------------------------
    subroutine interp_inner(cell_L,cell_N,cell_R)
    type(cell_center),intent(in) :: cell_L,cell_R
    type(cell_center),intent(inout) :: cell_N
    real(kind=RKD),allocatable,dimension(:,:,:) :: sL,sR

    !allocate array
    allocate(sL(unum,vnum,wnum))
    allocate(sR(unum,vnum,wnum))

    sL = (cell_N%f-cell_L%f)/(0.5*cell_N%length+0.5*cell_L%length)
    sR = (cell_R%f-cell_N%f)/(0.5*cell_R%length+0.5*cell_N%length)
    cell_N%sf = (sign(UP,sR)+sign(UP,sL))*abs(sR)*abs(sL)/(abs(sR)+abs(sL)+SMV)

    end subroutine interp_inner
    !--------------------------------------------------
    !>obtain collision term
    !--------------------------------------------------
    subroutine calc_collision_term(cell)
    include 'fftw3.f'
    type(cell_center),intent(inout) :: cell
    complex(kind=CKD),allocatable,dimension(:,:,:):: f_spec,fc,fc1,fc2,f_temp,fc11,fc22
    integer(kind=RKD) :: plan_backward, plan_backward2, plan_forward, plan_conv_f1, plan_conv_f2, plan_conv_b1, plan_conv_b2
    integer(kind=IKD) :: i,j,k

    allocate(f_spec(unum,vnum,wnum))
    allocate(fc(unum,vnum,wnum))
    allocate(fc1(unum,vnum,wnum))
    allocate(fc2(unum,vnum,wnum))
    allocate(f_temp(unum,vnum,wnum))
    allocate(fc11(unum,vnum,wnum))
    allocate(fc22(unum,vnum,wnum))
    call dfftw_plan_dft_3d(plan_backward,unum,vnum,wnum,f_spec,f_spec,FFTW_BACKWARD,FFTW_MEASURE)
    call dfftw_plan_dft_3d(plan_backward2,unum,vnum,wnum,f_temp,f_temp,FFTW_BACKWARD,FFTW_MEASURE)
    call dfftw_plan_dft_3d(plan_forward, unum,vnum,wnum,f_spec,fc,FFTW_FORWARD, FFTW_MEASURE)
    call dfftw_plan_dft_3d(plan_conv_f1,unum,vnum,wnum,fc1,fc11,FFTW_FORWARD, FFTW_MEASURE)
    call dfftw_plan_dft_3d(plan_conv_f2,unum,vnum,wnum,fc2,fc22,FFTW_FORWARD, FFTW_MEASURE)


    f_spec=cmplx(cell%f,0.0d0);
    call dfftw_execute_dft(plan_backward, f_spec, f_spec)
    f_spec=f_spec/(unum*vnum*wnum)
    call fftshift3d(f_spec)

    f_temp=cmplx(0.0d0)
    do i=1,M*(M-1)
        fc1=cmplx(f_spec*phi(:,:,:,i))
        fc2=cmplx(f_spec*psi(:,:,:,i))
        call dfftw_execute_dft(plan_conv_f1, fc1, fc11)
        call dfftw_execute_dft(plan_conv_f2, fc2, fc22)
        f_temp=f_temp+fc11*fc22
    enddo
    fc1=cmplx(f_spec*phipsi)
    fc2=f_spec
    call dfftw_execute_dft(plan_conv_f1, fc1, fc11)
    call dfftw_execute_dft(plan_conv_f2, fc2, fc22)
    f_temp=f_temp-fc11*fc22
    cell%Q =4.0d0*PI**2/kn_bzm/M**2*real(f_temp)

    call dfftw_destroy_plan(plan_backward)
    call dfftw_destroy_plan(plan_backward2)
    call dfftw_destroy_plan(plan_forward)
    call dfftw_destroy_plan(plan_conv_f1)
    call dfftw_destroy_plan(plan_conv_f2)
    deallocate(f_spec)
    deallocate(fc)
    deallocate(fc1)
    deallocate(fc2)
    deallocate(f_temp)
    deallocate(fc11)
    deallocate(fc22)
    end subroutine calc_collision_term
    end module solver

    !--------------------------------------------------
    !>input and output
    !--------------------------------------------------
    module io
    use ifport
    use global_data
    use tools
    implicit none
    contains
    !--------------------------------------------------
    !>main initialization subroutine
    !--------------------------------------------------
    subroutine init()
    real(kind=RKD) :: init_gas(3) !initial condition
    real(kind=RKD) :: alpha_ref,omega_ref !molecule model coefficient in referece state
    real(kind=RKD) :: Ma !Mach number in front of shock
    real(kind=RKD) :: xlength !length of computational domain
    real(kind=RKD) :: umin,vmin,wmin !smallest and largest discrete velocity (for newton-cotes)
    real(kind=RKD) :: xscale !cell size/mean free path

    !control
    cfl = 0.7 !CFL number
    dt=0.05!sqrt(4/pi)*0.05=0.0564
    max_time =250 !output time interval
    finish=0
    method_output = normalize !normalize the variables by (V-V1)/(V2-V1), where V1,V2 are upstream and downstream values

    !gas
    ck = 0
    gam = get_gamma(ck)
    pr = 2.0/3.0
    kn = 1.0
    omega = 0.81
    call get_potential(omega)
    mu_ref = get_mu(alpha,omega)
    kn_bzm=get_kn_bzm(mu_ref)

    !velocity space
    unum = 72
    wnum = 48
    vnum = 48
    umin = -18.0
    umax = +18.0
    vmin = -18.0
    vmax = +18.0
    wmin = -18.0
    wmax = +18.0

    !geometry
    xlength = 89.1d0
    xscale = 8.1d0

    !Mach number
    Ma = 6.0

    call init_geometry(xlength,xscale) !initialize the geometry
    call init_velocity_newton(unum,umin,umax,vnum,vmin,vmax,wnum,wmin,wmax) !initialize discrete velocity space
    call init_allocation() !allocate arrays
    call init_flow_field(Ma) !set the initial condition
    call init_kernel_mode()
    end subroutine init

    !--------------------------------------------------
    !>initialize the geometry
    !>@param[inout] xlength :domain length
    !>@param[in]    xscale  :cell size/mean free path
    !--------------------------------------------------
    subroutine init_geometry(xlength,xscale)
    real(kind=RKD),intent(inout) :: xlength
    real(kind=RKD),intent(in) :: xscale
    real(kind=RKD) :: xnum !number of cells
    real(kind=RKD) :: dx !cell length
    integer :: i

    !adjust values
    xnum = xlength/xscale
    xlength = xnum*xscale

    !cell index range
    ixmin = 1
    midnode=7
    ixmax = xnum

    !allocation
    allocate(ctr(ixmin-1:ixmax+1)) !cell center (with ghost cell)
    allocate(vface(ixmin:ixmax+1)) !vertical and horizontal cell interface

    !cell length and area
    dx = xlength/(ixmax-ixmin+1)

    !cell center (with ghost cell)
    forall(i=ixmin-1:ixmax+1)
        ctr(i)%x = (i-0.5)*dx
        ctr(i)%length = dx
    end forall
    end subroutine init_geometry

    !--------------------------------------------------
    !>set discrete velocity space using Newton�CCotes formulas
    !>@param[inout] num_u :number of velocity points
    !>@param[in]    min_u :smallest discrete velocity
    !>@param[in]    max_u :largest discrete velocity
    !--------------------------------------------------
    subroutine init_velocity_newton(num_u,min_u,max_u,num_v,min_v,max_v,num_w,min_w,max_w)
    integer,intent(inout) :: num_u,num_v,num_w
    real(kind=RKD),intent(in) :: min_u,max_u,min_v,max_v,min_w,max_w
    integer :: i,j,k

    !modify unum if not appropriate
    !unum = (num_u/4)*4+1

    !allocate array
    allocate(uspace(unum,vnum,wnum))
    allocate(vspace(unum,vnum,wnum))
    allocate(wspace(unum,vnum,wnum))
    allocate(weight(unum,vnum,wnum))

    !spacing in u and v velocity space
    du = (max_u-min_u)/(unum-1)
    dv = (max_v-min_v)/(vnum-1)
    dw = (max_w-min_w)/(wnum-1)

    !velocity space
    forall(i=1:unum)
        uspace(i,:,:) = min_u+(i-1)*du
    end forall
    forall(j=1:vnum)
        vspace(:,j,:) = min_v+(j-1)*dv
    end forall
    forall(k=1:wnum)
        wspace(:,:,k) = min_w+(k-1)*dw
    end forall
    forall(i=1:unum,j=1:vnum,k=1:wnum)
        weight(i,j,k) = du*dv*dw
    end forall

    !maximum micro velocity
    umax = max(max_u,max_v,max_w)

    !contains
    !    !--------------------------------------------------
    !    !>calculate the coefficient for newton-cotes formula
    !    !>@param[in] idx          :index in velocity space
    !    !>@param[in] num          :total number in velocity space
    !    !>@return    newton_coeff :coefficient for newton-cotes formula
    !    !--------------------------------------------------
    !    pure function newton_coeff(idx,num)
    !        integer,intent(in) :: idx,num
    !        real(kind=RKD) :: newton_coeff
    !
    !        if (idx==1 .or. idx==num) then
    !            newton_coeff = 14.0/45.0
    !        else if (mod(idx-5,4)==0) then
    !            newton_coeff = 28.0/45.0
    !        else if (mod(idx-3,4)==0) then
    !            newton_coeff = 24.0/45.0
    !        else
    !            newton_coeff = 64.0/45.0
    !        end if
    !    end function newton_coeff
    end subroutine init_velocity_newton

    !--------------------------------------------------
    !>allocate arrays
    !--------------------------------------------------
    subroutine init_allocation()
    integer :: i

    !cell center (with ghost cell)
    do i=ixmin-1,ixmax+1
        allocate(ctr(i)%f(unum,vnum,wnum))
        allocate(ctr(i)%Q(unum,vnum,wnum))
        allocate(ctr(i)%sf(unum,vnum,wnum))
    end do

    !cell interface
    do i=ixmin,ixmax+1
        allocate(vface(i)%flux_f(unum,vnum,wnum))
    end do
    end subroutine init_allocation

    !--------------------------------------------------
    !>set the initial condition
    !>@param[in] Ma_L :Mach number in front of shock
    !--------------------------------------------------
    subroutine init_flow_field(Ma_L)
    real(kind=RKD),intent(in) :: Ma_L
    real(kind=RKD) :: Ma_R !Mach number after shock
    real(kind=RKD) :: ratio_T !T2/T1
    real(kind=RKD) :: prim_L(3), prim_R(3) !primary variables before and after shock
    real(kind=RKD) :: w_L(3), w_R(3) !conservative variables before and after shock
    real(kind=RKD),allocatable,dimension(:,:,:) :: g_L !distribution function before shock
    real(kind=RKD),allocatable,dimension(:,:,:) :: g_R !distribution function before shock
    integer :: i

    !allocation
    allocate(g_L(unum,vnum,wnum))
    allocate(g_R(unum,vnum,wnum))

    !upstream condition (before shock)
    prim_L(1) = 1.0 !density
    prim_L(2) = Ma_L*sqrt(gam/2.0) !velocity
    prim_L(3) = 1.0 !lambda=1/temperature

    !downstream condition (after shock)
    Ma_R = sqrt((Ma_L**2*(gam-1)+2)/(2*gam*Ma_L**2-(gam-1)))
    ratio_T = (1+(gam-1)/2*Ma_L**2)*(2*gam/(gam-1)*Ma_L**2-1)/(Ma_L**2*(2*gam/(gam-1)+(gam-1)/2))

    prim_R(1) = prim_L(1)*(gam+1)*Ma_L**2/((gam-1)*Ma_L**2+2)
    prim_R(2) = Ma_R*sqrt(gam/2.0)*sqrt(ratio_T)
    prim_R(3) = prim_L(3)/ratio_T

    !conservative variables and distribution function
    w_L = get_conserved(prim_L)
    w_R = get_conserved(prim_R)
    call discrete_maxwell(g_L,prim_L)
    call discrete_maxwell(g_R,prim_R)

    !initialize field (with ghost cell)
    forall(i=ixmin-1:midnode)
        ctr(i)%w = w_L
        ctr(i)%f = g_L
    end forall

    forall(i=midnode+1:ixmax+1)
        ctr(i)%w = w_R
        ctr(i)%f = g_R
    end forall

    !initialize slope of distribution function at ghost cell
    ctr(ixmin-1)%sf = 0.0
    ctr(ixmax+1)%sf = 0.0
    end subroutine init_flow_field
    !--------------------------------------------------
    !>set the initial condition
    !>@param[in] Ma_L :Mach number in front of shock
    !--------------------------------------------------
    subroutine init_kernel_mode

    ! support of the distribution funtion
    real(kind=RKD) :: supp

    ! Gauss-Legendre quadrature
    integer(kind=RKD), parameter:: quad_num=64
    integer(kind=RKD) :: i, j, k, loop, loop2, idx, id
    real(kind=RKD) :: abscissa(quad_num), gweight(quad_num)
    real(kind=RKD) :: theta, theta2,s
    real(kind=RKD) :: bessel
    real(kind=IKD) :: bel

    ! kernel modes
    real(kind=RKD) :: fre_vx(unum), fre_vy(vnum), fre_vz(wnum)
    real(kind=RKD) :: int_temp

    allocate(phi(unum,vnum,wnum,M*(M-1)))
    allocate(psi(unum,vnum,wnum,M*(M-1)))
    allocate(phipsi(unum,vnum,wnum))
    phipsi=0.0d0


    ! support
    supp=sqrt(2.0d0)*2.0d0*max(umax,vmax,wmax)/(3.0d0+sqrt(2.0d0))

    call linspace(-PI/du,(unum/2-1.0d0)*2.0d0*PI/unum/du, unum, fre_vx)
    call linspace(-PI/dv,(vnum/2-1.0d0)*2.0d0*PI/vnum/dv, vnum, fre_vy)
    call linspace(-PI/dw,(wnum/2-1.0d0)*2.0d0*PI/wnum/dw, wnum, fre_vz)

    ! Gauss-Legendre quadrature
    call lgwt(quad_num, 0.0d0, supp, abscissa, gweight)

    ! calculate the kernel modes
    do loop=1,M-1
        theta=PI/M*loop
        do loop2=1,M
            theta2=PI/M*loop2
            idx=(loop-1)*M+loop2
            do k=1,wnum
                do j=1,vnum
                    do i=1,unum
                        s=fre_vx(i)*sin(theta)*cos(theta2)+ &
                            fre_vy(j)*sin(theta)*sin(theta2)+ &
                            fre_vz(k)*cos(theta)
                        !phi
                        int_temp=0.0d0
                        do id=1,quad_num
                            int_temp=int_temp+2.0d0*gweight(id)* cos(s*abscissa(id)) *(abscissa(id)**alpha)
                        enddo
                        phi(i,j,k,idx)=int_temp*sin(theta)
                        !psi
                        s=fre_vx(i)*fre_vx(i)+fre_vy(j)*fre_vy(j)+fre_vz(k)*fre_vz(k)-s*s
                        if(s<=0.0d0) then
                            psi(i,j,k,idx)=PI*supp*supp
                        else
                            s=sqrt(s)
                            bel=supp*s
                            bessel= besjn(1,bel)
                            !call Int_Bessel(s*supp,bessel)
                            psi(i,j,k,idx)=2.0d0*PI*supp*bessel/s
                        endif
                        !phipsi
                        phipsi(i,j,k)=phipsi(i,j,k)+phi(i,j,k,idx)*psi(i,j,k,idx)
                    enddo
                enddo
            enddo
        enddo
    enddo
    end subroutine init_kernel_mode
    !--------------------------------------------------
    !>write result
    !--------------------------------------------------
    subroutine output()
    real(kind=RKD) :: rho_avg, rho_dist
    integer i
    do i=ixmin,ixmax
        ctr(i)%w(1)=sum(weight*ctr(i)%f)
        ctr(i)%w(2)=sum(weight*uspace*ctr(i)%f)
        ctr(i)%w(3)=0.5*sum(weight*(uspace**2+vspace**2+wspace**2)*ctr(i)%f)
    end do
    open(unit=100,file="distence.plt")
    open(unit=101,file="xmid.plt")
    rho_avg = 0.5*(ctr(ixmin-1)%w(1)+ctr(ixmax+1)%w(1))
    rho_dist=100
    do i=ixmin,ixmax
        rho_dist=min(rho_dist,abs(ctr(i)%w(1)-rho_avg))
        if (rho_dist<=1.0d-3) then
            xmid = ctr(i)%x
            call writeresult()
            finish=1
            exit
        elseif ((ctr(i)%w(1)-rho_avg)*(ctr(i+1)%w(1)-rho_avg)<=0) then
            xmid = ctr(i)%x+(ctr(i+1)%x-ctr(i)%x)/(ctr(i+1)%w(1)-ctr(i)%w(1))*(rho_avg-ctr(i)%w(1))
        end if
    end do
    write(100,*) rho_dist
    write(101,*) xmid
    endsubroutine output
    !--------------------------------------------------
    !>write result
    !--------------------------------------------------
    subroutine writeresult()
    real(kind=RKD) :: f_x(unum,ixmin-1:ixmax+1)
    integer :: i,j

    !--------------------------------------------------
    !preparation
    !--------------------------------------------------
    !solution (with ghost cell)
    do i=ixmin-1,ixmax+1
        ctr(i)%primary=get_primary(ctr(i)%w)
        ctr(i)%primary(3)=1/ctr(i)%primary(3)
        do j=1,unum
            f_x(j,i)=sum(weight(j,:,:)*ctr(i)%f(j,:,:))
        enddo
    end do


    !normalization
    if (method_output==NORMALIZE) then
        ctr%primary(1) = (ctr%primary(1)-ctr(ixmin-1)%primary(1))/(ctr(ixmax+1)%primary(1)-ctr(ixmin-1)%primary(1))
        ctr%primary(2) = (ctr%primary(2)-ctr(ixmax+1)%primary(2))/(ctr(ixmin-1)%primary(2)-ctr(ixmax+1)%primary(2))
        ctr%primary(3) = (ctr%primary(3)-ctr(ixmin-1)%primary(3))/(ctr(ixmax+1)%primary(3)-ctr(ixmin-1)%primary(3))
    end if

    !--------------------------------------------------
    !write to file
    !--------------------------------------------------
    !open result file and write header
    open(unit=RSTFILE,file=RSTFILENAME,status="replace",action="write")
    write(RSTFILE,*) "VARIABLES = X, RHO, VEL, T"
    write(RSTFILE,*) 'ZONE  T="Time: ',sim_time,'", I = ',ixmax-ixmin+1,', DATAPACKING=BLOCK'

    !write geometry (cell-centered)
    write(RSTFILE,"(6(ES23.16,2X))") ctr(ixmin:ixmax)%x-xmid

    !write solution (cell-centered)
    do i=1,3
        write(RSTFILE,"(6(ES23.16,2X))") ctr(ixmin:ixmax)%primary(i)
    end do

    !close file
    close(RSTFILE)

    open(unit=2,file="x.dat",status="replace",action="write")
    open(unit=3,file="pdf.dat",status="replace",action="write")
    open(unit=4,file="vel.dat",status="replace",action="write")
    open(unit=5,file="x_normalized.dat",status="replace",action="write")
    do i=ixmin,ixmax
        WRITE(2,"(1(F20.10, 2X))")  ctr(i)%x
        WRITE(3,"(72(F20.10, 2X))") f_x(:,i)
        WRITE(5,"(1(F20.10, 2X))")  ctr(i)%x-xmid
    enddo
    write(4,"(72(F20.10, 2X))") uspace(:,1,1)
    close(2)
    close(3)
    close(4)
    close(5)

    end subroutine writeresult
    end module io

    !--------------------------------------------------
    !>main program
    !--------------------------------------------------
    program main
    use global_data
    use tools
    use solver
    use flux
    use io
    implicit none

    !initialization
    call init()

    !set initial value
    iter = 1 !number of iteration
    sim_time = 0.0 !simulation time

    !open file and write header
    open(unit=HSTFILE,file=HSTFILENAME,status="replace",action="write") !open history file
    write(HSTFILE,*) "VARIABLES = iter, sim_time, dt" !write header

    !iteration
    do while(.true.)
        call timestep()
        call interpolation() !calculate the slope of distribution function
        call evolution() !calculate flux across the interfaces
        call update_ap() !update cell averaged value

        if (mod(iter,10)==0)then
            write(HSTFILE,"(I15,2E15.7)") iter,sim_time,dt
        endif

        if (iter>2000)then
            call output()
        endif

        if(sim_time>=max_time .and. sim_time<(max_time+2*dt))then
            call output()
            call writeresult()
        endif

        if(finish) exit

        iter = iter+1
        sim_time = sim_time+dt
    end do

    !close history file
    close(HSTFILE)

    end program main

    ! vim: set ft=fortran tw=0:
