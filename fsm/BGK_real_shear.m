clear; clc; tic


method=1;
Tl=2.5;   Tr=2;
ul=0;       ur=0;
rhol=1; rhor=0.125;

tau=0.01;           %%% Knudsen number
%%%%%%%%%
L=8;
Nx=100;     x=linspace(0,2,Nx);        delta_x=x(2)-x(1);
Nvx=64;     vx=linspace(-L,L,Nvx);    delta_vx=vx(2)-vx(1);
Nvy=16;     vy=linspace(-L,L,Nvy);    delta_vy=vy(2)-vy(1);
Delta=delta_vx*delta_vy;
%%
CFL=0.8;
delta_t=delta_x/max(abs(vx))*CFL; %%% time step
%%

%%% find out where the x-component velocity becomes positive
[value,index]=find(vx>0);    V_mid=index(1);
clear value index
[Vx,Vy]=meshgrid(vx,vy);
%clear vx vy vz

V_dist_l=rhol/(pi*Tl)*exp(-((Vx-ul).^2+Vy.^2)/Tl);
V_dist_r=rhor/(pi*Tr)*exp(-((Vx-ur).^2+Vy.^2)/Tr);
x_step=x(1)/2+x(end)/2;


for loop=1:Nx
    if x(loop)<=x_step
        f(:,:,loop)=V_dist_l;
    else
        f(:,:,loop)=V_dist_r; 
    end
    A1(:,:,loop)=Vx;    A2(:,:,loop)=Vy;   
end
clear V_dist_l V_dist_r F x_step
% %%%%%%%%%%%%%%%%%%%%%%%%
temp2=zeros(size(f)); %% Q_{i}-Q_{i-1}
disc_vel=zeros(size(f));
V2=Vx.^2+Vy.^2;
%%%%%%%%%%%%%%%
for loop_time=1:0.5/delta_t
    loop_time    
    temp2(:,:,2:end)=f(:,:,2:end)-f(:,:,1:end-1);
    %%% for non-negative vx
    temp3=circshift(temp2(:,V_mid:end,:),[0,0,-1]); %%% Q_{i+1}-Q_{i}
    delta_1=limiter(temp2(:,V_mid:end,:)./(temp3+1e-15),method).*temp3;
    temp3=circshift(temp2(:,V_mid:end,:),[0,0,1]); %%% Q_{i-1}-Q_{i-2}     
    temp3(:,:,1)=0;  
    delta_2=limiter(temp3./(temp2(:,V_mid:end,:)+1e-15),method).*temp2(:,V_mid:end,:);     
    d_p=A1(:,V_mid:end,:).*      ( temp2(:,V_mid:end,:) ...
             +1/2*(1-A1(:,V_mid:end,:)*delta_t/delta_x) ...
               .*(delta_1-delta_2));
           %%% for negative vx      
    temp3=circshift(temp2(:,1:V_mid-1,:),[0,0,-1]);  %%% Q_{i+1}-Q_{i}
    temp1=circshift(temp3,[0,0,-1]); %%% Q_{i+2}-Q_{i+1}
    temp1(:,:,end)=0;
    delta_1=limiter(temp1./(temp3+1e-15),method).*temp3;
    delta_2=limiter(temp3./(temp2(:,1:V_mid-1,:)+1e-15),method).*temp2(:,1:V_mid-1,:);
    d_n=A1(:,1:V_mid-1,:).*             (temp3 ...
           -1/2*(1+A1(:,1:V_mid-1,:)*delta_t/delta_x) ...
              .*(delta_1-delta_2));
     %%% discrete of velocity
    disc_vel(:,1:V_mid-1,:)=d_n;
    disc_vel(:,V_mid:end,:)=d_p;
    temp=f-delta_t/delta_x*disc_vel;    
    
    %%%%%%%%%%%%%%%%  
    rho=reshape((sum(sum(temp)))*Delta,1,Nx);     
    ux=reshape((sum(sum(A1.*temp)))*Delta,1,Nx)./rho;     
    E=reshape((sum(sum((A1.^2+A2.^2).*temp)))*Delta,1,Nx);     
    %%%%%%%%%%%%%%%%%%%%%%
     p=E-rho.*ux.^2;
     T=p./rho;
epsilon=tau/1./rho;

     for loop=1:Nx  
         
         G=rho(loop)/(pi*T(loop))*exp(-((Vx-ux(loop)).^2+Vy.^2)/T(loop));
         C1=epsilon(loop)/(epsilon(loop)+delta_t);     C2=1-C1;
         f(:,:,loop)=C1*temp(:,:,loop)+C2*G;
     end 

end
toc





