using KitBase
using KitBase.FFTW
using KitBase.OffsetArrays
using Test

# ---
# 1D FFT
# ---

# 5-point discrete series
x = randn(4)
N = length(x)
x_fft = fft(x) # note that FFTW.jl doesn't support OffsetArray
x_ift = ifft(x)

# DFT
x_spec = Array{ComplexF64}(undef, N)
x_spec = OffsetArray(x_spec, 0:N-1)
for k in eachindex(x_spec)
    x_spec[k] = [x[n+1] * exp(-im * 2.0 * π / N * n * k) for n = 0:N-1] |> sum
end
@test collect(x_spec) ≈ x_fft atol = 0.01

# IFT
x_inv = Array{ComplexF64}(undef, N)
x_inv = OffsetArray(x_inv, 0:N-1)
for k in eachindex(x_inv)
    x_inv[k] = 1 / N * [x[n+1] * exp(im * 2.0 * π / N * n * k) for n = 0:N-1] |> sum
end
@test collect(x_inv) ≈ x_ift atol = 0.01

# non-uniform FFT
using FastTransforms
p = [i / N for i = 0:N-1]

x_nft = nufft2(x .+ 0.0im, p, eps())
@test collect(x_nft) ≈ x_fft atol = 0.01

x_niv = inufft2(x .+ 0.0im, p, eps())
@test collect(x_niv) ≈ x_ift atol = 0.01

using FINUFFT

x_n3 = nufft1d3(2pi * p, x .+ 0im, -1, 1e-10, [0., 1., 2., 3.])
@test collect(x_n3) ≈ x_fft atol = 0.01

ref = complex(zeros(4))
for j=1:4
    for ss=1:4
        ref[j] += x[ss] * exp(-1im*(j-1)*2pi*p[ss])
    end
end
@test collect(x_n3) ≈ ref atol = 0.01

x_i3 = nufft1d3(2pi * p, x .+ 0.0im, 1, 1e-10, [0., 1., 2., 3.]) ./ 4
@test collect(x_i3) ≈ x_ift atol = 0.01

# ---
# 2D FFT
# ---

f = randn(4, 4)

f_fft = fft(f)
f_ift = ifft(f)

x2 = [i / N for i = 0:N-1, j = 0:N-1]
y2 = [j / N for i = 0:N-1, j = 0:N-1]
s2 = [i for i = 0:N-1, j = 0:N-1] .|> Float64
t2 = [j for i = 0:N-1, j = 0:N-1] .|> Float64

f_nft = nufft2d3(2pi .* x2[:], 2pi .* y2[:], f[:] .+ 0.0im, -1, 1e-10, s2[:], t2[:])
f_nft = reshape(f_nft, 4, 4)

@test f_nft ≈ f_fft atol = 0.01

# ---
# 3D FFT
# ---

f = randn(6, 5, 4)

f_fft = fft(f)
f_ift = ifft(f)

N1, N2, N3 = size(f)

x3 = [i / N1 for i = 0:N1-1, j = 0:N2-1, k = 0:N3-1]
y3 = [j / N2 for i = 0:N1-1, j = 0:N2-1, k = 0:N3-1]
z3 = [k / N3 for i = 0:N1-1, j = 0:N2-1, k = 0:N3-1]

s3 = [i for i = 0:N1-1, j = 0:N2-1, k = 0:N3-1] .|> Float64
t3 = [j for i = 0:N1-1, j = 0:N2-1, k = 0:N3-1] .|> Float64
u3 = [k for i = 0:N1-1, j = 0:N2-1, k = 0:N3-1] .|> Float64

f_nft = nufft3d3(2pi .* x3[:], 2pi .* y3[:], 2pi .* z3[:], f[:] .+ 0.0im, -1, 1e-10, s3[:], t3[:], u3[:])
f_nft = reshape(f_nft, N1, N2, N3)

@test f_nft ≈ f_fft atol = 0.01
