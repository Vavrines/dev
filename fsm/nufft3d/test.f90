! ifort -c abc.f
! ifort -o test test.f90 dfftpack.o dirft3d.o nufft3df90.o next235.o

program main

    implicit none

    integer i,ier,iflag,j,k1,k2,k3,ms,mt,mu,n1,n2,n3,nj,nk
    
    integer, parameter :: mx=1000000

    real(kind=8)::xj(mx),yj(mx),zj(mx)
    real(kind=8)::sk(mx),tk(mx),uk(mx)
    real(kind=8)::err,eps,salg,ealg
    real(kind=8)::t0,t1,second

    complex(kind=16)::cj(mx),cj0(mx),cj1(mx)
    complex(kind=16)::fk0(mx),fk1(mx)

    real(kind=8), parameter :: pi=3.141592653589793d0

    

    ms = 24
    mt = 16
    mu = 18
    n1 = 16
    n2 = 18
    n3 = 24
    nj = n1*n2*n3
    do k3 = -n3/2, (n3-1)/2
       do k2 = -n2/2, (n2-1)/2
          do k1 = -n1/2, (n1-1)/2
             j =  1 + (k1+n1/2+1) + (k2+n2/2)*n1 + (k3+n3/2)*n1*n2
             xj(j) = pi*dcos(-pi*k1/n1)
             yj(j) = pi*dcos(-pi*k2/n2)
             zj(j) = pi*dcos(-pi*k3/n3)
             cj(j) = dcmplx(dsin(pi*j/n1),dcos(pi*j/n2))
          enddo
       enddo
    enddo

    iflag = 1
    eps=1d-8

       call dirft3d1(nj,xj,yj,zj,cj,iflag,ms,mt,mu,fk0)

       call dirft3d2(nj,xj,yj,zj,cj0,iflag,ms,mt,mu,fk0)

end
