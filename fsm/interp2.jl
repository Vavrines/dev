using KitBase
using KitBase.PyCall
using KitBase.SpecialFunctions
using OrdinaryDiffEq
using Plots

begin
    case = "homogeneous"
    maxTime = 0.5
    tlen = 4
    u0 = -5
    u1 = 5
    nu = 48
    nug = 0
    v0 = -5
    v1 = 5
    nv = 28
    nvg = 0
    w0 = -5
    w1 = 5
    nw = 28
    nwg = 0
    vMeshType = "rectangle"
    nm = 5
    knudsen = 1
    inK = 0
    alpha = 1.0
    omega = 0.5
    nh = 8
end
tspan = (0.0, maxTime)
tran = linspace(tspan[1], tspan[2], tlen)
γ = heat_capacity_ratio(inK, 3)
vs = VSpace3D(u0, u1, nu, v0, v1, nv, w0, w1, nw, "algebra")

f0 =
    Float32.(
        0.5 * (1 / π)^1.5 .*
        (exp.(-(vs.u .- 0.99) .^ 2) .+ exp.(-(vs.u .+ 0.99) .^ 2)) .*
        exp.(-vs.v .^ 2) .* exp.(-vs.w .^ 2),
    ) |> Array
prim0 =
    conserve_prim(moments_conserve(f0, vs.u, vs.v, vs.w, vs.weights), γ)
M0 = Float32.(maxwellian(vs.u, vs.v, vs.w, prim0)) |> Array

mu_ref = ref_vhs_vis(knudsen, alpha, omega)
kn_bzm = hs_boltz_kn(mu_ref, 1.0)
τ0 = mu_ref * 2.0 * prim0[end]^(0.5) / prim0[1]

phi, psi, phipsi = kernel_mode(
    nm,
    vs.u1,
    vs.v1,
    vs.w1,
    vs.nu,
    vs.nv,
    vs.nw,
    alpha,
)

vnu = hcat(vs.u[:], vs.v[:], vs.w[:])

uuni1d = linspace(vs.u[1,1,1], vs.u[end,1,1], nu)
vuni1d = linspace(vs.v[1,1,1], vs.v[1,end,1], nv)
wuni1d = linspace(vs.w[1,1,1], vs.w[1,1,end], nw)

u13d = [uuni1d[i] for i = 1:nu, j=1:nv, k=1:nw]
v13d = [vuni1d[j] for i = 1:nu, j=1:nv, k=1:nw]
w13d = [wuni1d[k] for i = 1:nu, j=1:nv, k=1:nw]
vuni = hcat(u13d[:], v13d[:], w13d[:])

u, v, w = vs.u[:,1,1], vs.v[1,:,1], vs.w[1,1,:]

prob = ODEProblem(
    boltzmann_nuode!,
    f0,
    tspan,
    [kn_bzm, nm, phi, psi, phipsi, vs.u[:, 1, 1], vs.v[1, :, 1], vs.w[1, 1, :], vnu, uuni1d, vuni1d, wuni1d, vuni],
)
data_boltz = solve(prob, Midpoint(), saveat = tran) |> Array

plot(vs.u[:,1,1], data_boltz[:,end÷2,end÷2,end])
