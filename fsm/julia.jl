using KitBase
using KitBase.FFTW
using KitBase.SpecialFunctions
using FINUFFT
using OrdinaryDiffEq

begin
    case = "homogeneous"
    maxTime = 3
    tlen = 16
    u0 = -5
    u1 = 5
    nu = 48
    nug = 0
    v0 = -5
    v1 = 5
    nv = 28
    nvg = 0
    w0 = -5
    w1 = 5
    nw = 28
    nwg = 0
    nm = 5
    knudsen = 1
    inK = 0
    alpha = 1.0
    omega = 0.5
end
tspan = (0.0, maxTime)
tran = linspace(tspan[1], tspan[2], tlen)
γ = heat_capacity_ratio(inK, 3)
mu_ref = ref_vhs_vis(knudsen, alpha, omega)
kn_bzm = hs_boltz_kn(mu_ref, 1.0)

vs1 = VSpace3D(u0, u1, nu, v0, v1, nv, w0, w1, nw, "rectangle")
vs = VSpace3D(u0, u1, nu, v0, v1, nv, w0, w1, nw, "algebra")

f0 =
    Float32.(
        0.5 * (1 / π)^1.5 .*
        (exp.(-(vs.u .- 0.99) .^ 2) .+ exp.(-(vs.u .+ 0.99) .^ 2)) .*
        exp.(-vs.v .^ 2) .* exp.(-vs.w .^ 2),
    ) |> Array
prim0 =
    conserve_prim(moments_conserve(f0, vs.u, vs.v, vs.w, vs.weights), γ)
M0 = Float32.(maxwellian(vs.u, vs.v, vs.w, prim0)) |> Array
τ0 = mu_ref * 2.0 * prim0[end]^(0.5) / prim0[1]

f1 =
    Float32.(
        0.5 * (1 / π)^1.5 .*
        (exp.(-(vs1.u .- 0.99) .^ 2) .+ exp.(-(vs1.u .+ 0.99) .^ 2)) .*
        exp.(-vs1.v .^ 2) .* exp.(-vs1.w .^ 2),
    ) |> Array
M1 = Float32.(maxwellian(vs1.u, vs1.v, vs1.w, prim0)) |> Array

function KitBase.kernel_mode(
    M::I,
    umax::R,
    vmax::R,
    wmax::R,
    du::AbstractArray,
    dv::AbstractArray,
    dw::AbstractArray,
    unum::I,
    vnum::I,
    wnum::I,
    alpha::R;
    quad_num = 64::I,
) where {I<:Integer,R<:Real}

    supp = sqrt(2.0) * 2.0 * max(umax, vmax, wmax) / (3.0 + sqrt(2.0))

    #fre_vx = collect(range(-π, (unum ÷ 2 - 1) * 2.0 * π / unum, length = unum)) ./ du
    #fre_vy = collect(range(-π, (vnum ÷ 2 - 1) * 2.0 * π / vnum, length = vnum)) ./ dv
    #fre_vz = collect(range(-π, (wnum ÷ 2 - 1) * 2.0 * π / wnum, length = wnum)) ./ dw

    fre_vx = [i * π / umax for i = -unum÷2:unum÷2-1]
    fre_vy = [i * π / vmax for i = -vnum÷2:vnum÷2-1]
    fre_vz = [i * π / wmax for i = -wnum÷2:wnum÷2-1]

    abscissa, gweight = KitBase.lgwt(quad_num, 0, supp)

    phi = zeros(unum, vnum, wnum, M * (M - 1))
    psi = zeros(unum, vnum, wnum, M * (M - 1))
    phipsi = zeros(unum, vnum, wnum)
    for loop = 1:M-1
        theta = π / M * loop
        for loop2 = 1:M
            theta2 = π / M * loop2
            idx = (loop - 1) * M + loop2
            for k = 1:wnum, j = 1:vnum, i = 1:unum
                s =
                    fre_vx[i] * sin(theta) * cos(theta2) +
                    fre_vy[j] * sin(theta) * sin(theta2) +
                    fre_vz[k] * cos(theta)
                # phi
                int_temp = 0.0
                for id = 1:quad_num
                    int_temp +=
                        2.0 * gweight[id] * cos(s * abscissa[id]) * (abscissa[id]^alpha)
                end
                phi[i, j, k, idx] = int_temp * sin(theta)
                # psi
                s = fre_vx[i]^2 + fre_vy[j]^2 + fre_vz[k]^2 - s^2
                if s <= 0.0
                    psi[i, j, k, idx] = π * supp^2
                else
                    s = sqrt(s)
                    bel = supp * s
                    bessel = besselj(1, bel)
                    psi[i, j, k, idx] = 2.0 * π * supp * bessel / s
                end
                # phipsi
                phipsi[i, j, k] += phi[i, j, k, idx] * psi[i, j, k, idx]
            end
        end
    end

    return phi, psi, phipsi

end

phi, psi, phipsi = kernel_mode(
    nm,
    vs.u1,
    vs.v1,
    vs.w1,
    vs.du[:, 1, 1],
    vs.dv[1, :, 1],
    vs.dw[1, 1, :],
    vs.nu,
    vs.nv,
    vs.nw,
    alpha,
)

function KitBase.boltzmann_fft(
    f::X,
    Kn,
    M::I,
    ϕ::Y,
    ψ::Y,
    phipsi::Z,
    xj,
    yj,
    zj,
    sk,
    tk,
    uk,
) where {
    X<:AbstractArray{<:AbstractFloat,3},
    Y<:AbstractArray{<:AbstractFloat,4},
    Z<:AbstractArray{<:AbstractFloat,3},
    I<:Integer,
}

    N1, N2, N3 = size(f)
    f_spec = f .+ 0.0im

    f_vec = nufft3d3(2pi .* xj[:], 2pi .* yj[:], 2pi .* zj[:], f_spec[:], 1, 1e-10, sk[:], tk[:], uk[:])
    f_spec .= reshape(f_vec, N1, N2, N3)
    f_spec ./= N1 * N2 * N3
    f_spec .= fftshift(f_spec)

    #--- gain term ---#
    f_temp = zero(f_spec) .+ 0.0im
    for i = 1:M*(M-1)
        fg1 = f_spec .* ϕ[:, :, :, i]
        fg2 = f_spec .* ψ[:, :, :, i]
        fg11 = nufft3d3(2pi .* xj[:], 2pi .* yj[:], 2pi .* zj[:], fg1, -1, 1e-10, sk[:], tk[:], uk[:])
        fg22 = nufft3d3(2pi .* xj[:], 2pi .* yj[:], 2pi .* zj[:], fg2, -1, 1e-10, sk[:], tk[:], uk[:])
        f_temp .+= reshape(fg11, N1, N2, N3) .* reshape(fg22, N1, N2, N3)
    end

    #--- loss term ---#
    fl1 = f_spec .* phipsi
    fl2 = f_spec
    fl11 = nufft3d3(2pi .* xj[:], 2pi .* yj[:], 2pi .* zj[:], fl1, -1, 1e-10, sk[:], tk[:], uk[:])
    fl22 = nufft3d3(2pi .* xj[:], 2pi .* yj[:], 2pi .* zj[:], fl2, -1, 1e-10, sk[:], tk[:], uk[:])
    f_temp .-= reshape(fl11, N1, N2, N3) .* reshape(fl22, N1, N2, N3)

    Q = @. 4.0 * π^2 / Kn / M^2 * real(f_temp)

    return Q

end

s3 = [i for i = 0:vs.nu-1, j = 0:vs.nv-1, k = 0:vs.nw-1] .|> Float64
t3 = [j for i = 0:vs.nu-1, j = 0:vs.nv-1, k = 0:vs.nw-1] .|> Float64
u3 = [k for i = 0:vs.nu-1, j = 0:vs.nv-1, k = 0:vs.nw-1] .|> Float64

xj = zeros(vs.nu, vs.nv, vs.nw)
yj = zero(xj)
zj = zero(xj)

δu = (vs.u[end, 1, 1] - vs.u[1, 1, 1]) / (vs.nu - 1)
δv = (vs.v[1, end, 1] - vs.v[1, 1, 1]) / (vs.nv - 1)
δw = (vs.w[1, 1, end] - vs.w[1, 1, 1]) / (vs.nw - 1)
for i = 1:vs.nu, j = 1:vs.nv, k = 1:vs.nw
    xj[i, j, k] = (vs.u[i, 1, 1] - vs.u[1, 1, 1]) / δu / vs.nu
    yj[i, j, k] = (vs.v[1, j, 1] - vs.v[1, 1, 1]) / δv / vs.nv
    zj[i, j, k] = (vs.w[1, 1, k] - vs.w[1, 1, 1]) / δw / vs.nw
end

q1 = boltzmann_fft(f1, kn_bzm, nm, phi, psi, phipsi)

q0 = boltzmann_fft(f0, kn_bzm, nm, phi, psi, phipsi, xj, yj, zj, s3, t3, u3)

q2 = boltzmann_fft(f1, kn_bzm, nm, phi, psi, phipsi, x0, y0, z0, s3, t3, u3)


function boltzmann_ode_nonuniform!(df, f::T, p, t) where {T<:AbstractArray{<:Real,3}}
    Kn, M, phi, psi, phipsi, xj, yj, zj, sk, tk, uk = p
    df .= boltzmann_fft(f, Kn, M, phi, psi, phipsi, xj, yj, zj, sk, tk, uk)
end

prob = ODEProblem(boltzmann_ode_nonuniform!, f0, tspan, [kn_bzm, nm, phi, psi, phipsi, xj, yj, zj, s3, t3, u3])
data_boltz = solve(prob, Midpoint(), saveat = tran) |> Array

using Plots

plot(vs.u[:, 1, 1], q0[:, end÷2, end÷2])
plot!(vs1.u[:, 1, 1], q1[:, end÷2, end÷2])

#plot!(vs.u[:, 1, 1], data_boltz[:, end÷2, end÷2, end÷2, end])

plot(vs.u[:, 1, 1], f0[:, end÷2, end÷2])
plot!(vs1.u[:, 1, 1], f1[:, end÷2, end÷2])

N1, N2, N3 = size(f0)
f_spec = f0 .+ 0.0im

f_vec = nufft3d3(2pi .* xj[:], 2pi .* yj[:], 2pi .* zj[:], f0 .+ 0.0im, 1, 1e-10, s3[:], t3[:], u3[:])
f_spec .= reshape(f_vec, N1, N2, N3)
f_spec ./= N1 * N2 * N3

f_spec2 = ifft(f1.+0.0im)



plot!(real(f_spec)[:, end÷2, end÷2])
plot(real(f_spec2)[:, end÷2, end÷2])




x0 = [i / N1 for i = 0:N1-1, j = 0:N2-1, k = 0:N3-1]
y0 = [j / N2 for i = 0:N1-1, j = 0:N2-1, k = 0:N3-1]
z0 = [k / N3 for i = 0:N1-1, j = 0:N2-1, k = 0:N3-1]
f_vec = nufft3d3(2pi .* x0[:], 2pi .* y0[:], 2pi .* z0[:], f1 .+ 0.0im, 1, 1e-10, s3[:], t3[:], u3[:])
f_spec .= reshape(f_vec, N1, N2, N3)
f_spec ./= N1 * N2 * N3

plot(real(f_spec)[:, end÷2, end÷2])
scatter!(real(f_spec2)[:, end÷2, end÷2])