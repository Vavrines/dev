using OrdinaryDiffEq, Plots, KitBase
using KitBase.ProgressMeter: @showprogress

cd(@__DIR__)
include("tools.jl")

D = Dict{Symbol,Any}()
begin
    D[:matter] = "gas"
    D[:case] = "sod"
    D[:space] = "1d1f1v"
    D[:flux] = "kfvs"
    D[:collision] = "fp"
    D[:nSpecies] = 1
    D[:interpOrder] = 2
    D[:limiter] = "vanleer"
    D[:boundary] = "fix"
    D[:cfl] = 0.1
    D[:maxTime] = 0.1

    D[:x0] = 0.0
    D[:x1] = 1.0
    D[:nx] = 100
    D[:pMeshType] = "uniform"
    D[:nxg] = 1

    D[:umin] = -5.0
    D[:umax] = 5.0
    D[:nu] = 80
    D[:vMeshType] = "rectangle"
    D[:nug] = 0

    D[:knudsen] = 1e-2
    D[:mach] = 0.0
    D[:prandtl] = 1.0
    D[:inK] = 0.0
    D[:omega] = 0.81
    D[:alphaRef] = 1.0
    D[:omegaRef] = 0.5
end

ks = SolverSet(D)
ctr, face = init_fvm(ks, ks.ps, :dynamic_array; structarray = true)

t = 0.0
dt = timestep(ks, ctr, t)
nt = Int(ks.set.maxTime ÷ dt) + 1
res = zero(ctr[1].w)
@showprogress for iter = 1:5#nt
    reconstruct!(ks, ctr)

    for i in eachindex(face)
        flux_kfvs!(
            face[i].fw,
            face[i].ff,
            ctr[i-1].f .+ ctr[i-1].sf .* ks.ps.dx[i-1] / 2,
            ctr[i].f .- ctr[i].sf .* ks.ps.dx[i] / 2,
            ks.vs.u,
            ks.vs.weights,
            dt,
            ctr[i-1].sf,
            ctr[i].sf,
        )
    end
    
    #update!(ks, ctr, face, dt, res; coll = Symbol(ks.set.collision), bc = Symbol(ks.set.boundary))
    update(ks, ctr, face, dt, res)

    t += dt
end

plot(ks, ctr)
