function update(
    KS::X,
    ctr::Y,
    face::Z,
    dt,
    residual
) where {
    X<:AbstractSolverSet,
    Y<:AbstractArray{ControlVolume1D1F,1},
    Z<:AbstractArray{Interface1D1F,1},
}

    sumRes = zero(ctr[1].w)
    sumAvg = zero(ctr[1].w)

    @inbounds Threads.@threads for i = 1:KS.pSpace.nx
        fpstep!(
            face[i].fw,
            face[i].ff,
            ctr[i].w,
            ctr[i].prim,
            ctr[i].f,
            face[i+1].fw,
            face[i+1].ff,
            KS.vSpace.u,
            KS.vSpace.weights,
            KS.gas.γ,
            KS.gas.μᵣ,
            KS.gas.ω,
            KS.gas.Pr,
            KS.ps.dx[i],
            dt,
            sumRes,
            sumAvg,
        )
    end
   

    for i in eachindex(residual)
        residual[i] = sqrt(sumRes[i] * KS.pSpace.nx) / (sumAvg[i] + 1.e-7)
    end

    return nothing

end

function fpstep!(
    fwL::T1,
    ffL::T2,
    w::T3,
    prim::T3,
    f::T4,
    fwR::T1,
    ffR::T2,
    u::T5,
    weights::T5,
    γ,
    μᵣ,
    ω,
    Pr,
    dx,
    dt,
    RES,
    AVG,
    collision = :bgk::Symbol,
) where {
    T1<:AbstractArray{<:AbstractFloat,1},
    T2<:AbstractArray{<:AbstractFloat,1},
    T3<:AbstractArray{<:AbstractFloat,1},
    T4<:AbstractArray{<:AbstractFloat,1},
    T5<:AbstractArray{<:AbstractFloat,1},
}

    #--- store W^n and calculate H^n,\tau^n ---#
    w_old = deepcopy(w)

    #--- update W^{n+1} ---#
    @. w += (fwL - fwR) / dx
    prim .= conserve_prim(w, γ)

    #--- record residuals ---#
    @. RES += (w - w_old)^2
    @. AVG += abs(w)

    #--- calculate M^{n+1} and tau^{n+1} ---#
    τ = vhs_collision_time(prim, μᵣ, ω)

    fu = finite_difference(f, u; derivative = 1, truncation = 4, bc = :Dirichlet)
    #fuu = finite_difference(fu, u; bc = :Dirichlet)
    fuu = finite_difference(f, u; derivative = 2, truncation = 4, bc = :Dirichlet)

    #--- update distribution function ---#
    for i in eachindex(u)
        f[i] = f[i] + (ffL[i] - ffR[i]) / dx + dt / τ * (f[i] + (u[i] - prim[2]) * fu[i] + fuu[i] / 2 / prim[end])
    end

    return nothing

end
