using KitBase, Plots

vs = VSpace1D(-6, 6, 200)

prim = [1., 0., 1.]
M = maxwellian(vs.u, prim)
fu = finite_difference(M, vs.u; bc = :Dirichlet)
fuu = finite_difference(fu, vs.u; bc = :Dirichlet)

"""
It seems that at least 4th-order difference
is required to maintain an equilibrium
"""
function Qfp(f, u, prim)
    fu = finite_difference(f, u; derivative = 1, truncation = 4, bc = :Dirichlet)
    #fuu = finite_difference(fu, u; derivative = 1, truncation = 4, bc = :Dirichlet)
    fuu = finite_difference(M, u; derivative = 2, truncation = 4, bc = :Dirichlet)

    return @. f + (u - prim[2]) * fu + fuu / 2 / prim[end]
end

Q = Qfp(M, vs.u, prim)

plot(vs.u, Q)
