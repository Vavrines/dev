using OrdinaryDiffEq, Plots, KitBase
using KitBase.ProgressMeter: @showprogress

cd(@__DIR__)
include("tools.jl")

set = Setup(space = "1d1f1v", boundary = "period", cfl = 0.1)
ps = PSpace1D(0, 1, 100, 1)
vs = VSpace1D(-5, 5, 200)
gas = Gas(Kn = 1e-3, K = 0, γ = 3)

fw = function (x)
    n = 1.0 + 0.05 * sin(2 * π * x)
    v = 0.5
    λ = n / 2

    prim = [n, v, λ]
    return prim_conserve(prim, 3)
end
ib = IB1F(fw, vs, gas)

ks = SolverSet(set, ps, vs, gas, ib, @__DIR__)
ctr, face = init_fvm(ks, ks.ps, :dynamic_array; structarray = true)

t = 0.0
dt = timestep(ks, ctr, t)
nt = Int(ks.set.maxTime ÷ dt) + 1
res = zero(ctr[1].w)
@showprogress for iter = 1:1#nt
    reconstruct!(ks, ctr)

    for i in eachindex(face)
        flux_kfvs!(
            face[i].fw,
            face[i].ff,
            ctr[i-1].f .+ ctr[i-1].sf .* ks.ps.dx[i-1] / 2,
            ctr[i].f .- ctr[i].sf .* ks.ps.dx[i] / 2,
            ks.vs.u,
            ks.vs.weights,
            dt,
            ctr[i-1].sf,
            ctr[i].sf,
        )
    end
    
    #update!(ks, ctr, face, dt, res; coll = Symbol(ks.set.collision), bc = Symbol(ks.set.boundary))
    update(ks, ctr, face, dt, res)

    ctr[0].w .= ctr[ks.ps.nx].w
    ctr[0].prim .= ctr[ks.ps.nx].prim
    ctr[0].f .= ctr[ks.ps.nx].f
    ctr[0].sf .= ctr[ks.ps.nx].sf
    ctr[ks.ps.nx+1].w .= ctr[1].w
    ctr[ks.ps.nx+1].prim .= ctr[1].prim
    ctr[ks.ps.nx+1].f .= ctr[1].f
    ctr[ks.ps.nx+1].sf .= ctr[1].sf

    t += dt
end

plot(ks, ctr)