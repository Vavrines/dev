using OffsetArrays, ProgressMeter, Distributions
include("/home/vavrines/Coding/KitBase.jl/src/KitBase.jl")
import .KitBase
cd(@__DIR__)
include("method.jl")

begin
    cd(@__DIR__)
    D = KitBase.read_dict("config.txt")
    for key in keys(D)
        s = Symbol(key)
        @eval $s = $(D[key])
    end

    γ = KitBase.heat_capacity_ratio(inK, 1)
    set = KitBase.Setup(
        case,
        space,
        flux,
        collision,
        nSpecies,
        interpOrder,
        limiter,
        cfl,
        maxTime,
    )
    pSpace = KitBase.PSpace1D(x0, x1, nx, pMeshType, nxg)
    vSpace = KitBase.VSpace1D(umin, umax, nu, vMeshType, nug)
    μᵣ = KitBase.ref_vhs_vis(knudsen, alphaRef, omegaRef)
    gas = KitBase.Particle(
        knudsen,
        mach,
        prandtl,
        inK,
        γ,
        omega,
        alphaRef,
        omegaRef,
        μᵣ,
        mass,
        0,
    )
    wL, primL, hL, bL, bcL, wR, primR, hR, bR, bcR =
        KitBase.ib_rh(gas.Ma, gas.γ, gas.K, vSpace.u)
    ib = KitBase.IB(wL, primL, bcL, wR, primR, bcR)
    ks = KitBase.SolverSet(set, pSpace, vSpace, gas, ib, pwd())

    ctr = OffsetArray{KitBase.ControlVolumeParticle1D}(undef, eachindex(ks.pSpace.x))
    face = Array{KitBase.Interface1D}(undef, ks.pSpace.nx + 1)

    for i in eachindex(ctr)
        if i <= ks.pSpace.nx ÷ 2
            ctr[i] = KitBase.ControlVolumeParticle1D(
                ks.pSpace.x[i],
                ks.pSpace.dx[i],
                ks.ib.wL,
                ks.ib.primL,
                #KitBase.prim_conserve([1.0, 0.0, 0.5], ks.gas.γ),
                #[1.0, 0.0, 0.5],
                KitBase.vhs_collision_time(ks.ib.primL, ks.gas.μᵣ, ks.gas.ω),
            )
        else
            ctr[i] = KitBase.ControlVolumeParticle1D(
                ks.pSpace.x[i],
                ks.pSpace.dx[i],
                ks.ib.wR,
                ks.ib.primR,
                #ks.ib.wL,
                #ks.ib.primL,
                #KitBase.prim_conserve([0.8, 0.0, 0.8], ks.gas.γ),
                #[0.8, 0.0, 0.8],
                KitBase.vhs_collision_time(ks.ib.primR, ks.gas.μᵣ, ks.gas.ω),
            )
        end
    end

    for i = 1:ks.pSpace.nx+1
        face[i] = KitBase.Interface1D(ks.ib.wL)
    end

    t = 0.0
    dt = KitBase.timestep(ks, ctr, t)
    res = zeros(3)

    ptc = KitBase.init_ptc(ks, ctr)
    ptc_new = deepcopy(ptc)
    ks.gas.np = length(ptc) ÷ 2
end

#=
function solve!(ks, ctr, ptc, ptc_new, t, dt, res, nt=10)
    @showprogress for iter in 1:nt
    #for iter in 1:10
        #KitBase.reconstruct!(ks, ctr)
        
        @inbounds Threads.@threads for i = 1:ks.pSpace.nx+1
            KitBase.flux_equilibrium!(
                face[i].fw,
                ctr[i-1].w,
                ctr[i].w,
                ks.gas.K,
                ks.gas.γ,
                ks.gas.μᵣ,
                ks.gas.ω,
                ks.gas.Pr,
                dt,
                0.5 * ctr[i-1].dx,
                0.5 * ctr[i].dx,
            )
        end
        
        particle_transport!(ks, ctr, ptc, ptc_new, dt)
        particle_collision!(ks, ctr, ptc, ptc_new, face)
        
        t += dt
    end

    return t
end

t = solve!(ks, ctr, ptc, ptc_new, t, dt, res, 5)
=#

@showprogress for iter = 1:2
#for iter = 1:5
    #KitBase.reconstruct!(ks, ctr)
    
    @inbounds for i = 1:ks.pSpace.nx+1
        KitBase.flux_equilibrium!(
            face[i].fw,
            ctr[i-1].w,
            ctr[i].w,
            ks.gas.K,
            ks.gas.γ,
            ks.gas.μᵣ,
            ks.gas.ω,
            ks.gas.Pr,
            dt,
            0.5 * ctr[i-1].dx,
            0.5 * ctr[i].dx,
        )
    end
    
    np = particle_transport!(ks, ctr, ptc, ptc_new, dt)
    particle_collision!(ks, ctr, ptc, ptc_new, face, np)
    #simple_transport!(ks, ctr, ptc, ptc_new, dt)
    #simple_collision!(ks, ctr, ptc, ptc_new, face)

    t += dt
end

begin
    using Plots
    pltx = ks.pSpace.x[1:ks.pSpace.nx]
    plty = zeros(ks.pSpace.nx, 6)
    for i in eachindex(pltx)
            for j = 1:3
            #    plty[i, j] = ctr[i].w[j]
            end

        plty[i, 1:3] .= KitBase.conserve_prim(ctr[i].w, ks.gas.γ)
        plty[i, 3] = 1 / plty[i, 3]
    end
    plot(pltx, plty[:, 1:3], lw = 2, legend = :none)
end

#KitBase.plot_line(ks, ctr)

particle_transport!(ks, ctr, ptc, ptc_new, dt)
particle_collision!(ks, ctr, ptc, ptc_new, face)

#
nn = 0
w = zeros(3)
for i in 1:ks.gas.np
    if ptc[i].idx == 50
        nn+=1
        w[1] += ptc[i].m / ctr[ptc[i].idx].dx
        w[2] += ptc[i].m * ptc[i].v[1] / ctr[ptc[i].idx].dx
        w[3] += 0.5 * ptc[i].m * sum(ptc[i].v .^ 2) / ctr[ptc[i].idx].dx
    end
end
#