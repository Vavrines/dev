ptc = Array{KitBase.Particle1D}(undef, np)
for i in eachindex(ptc)
    ptc[i] = KitBase.Particle1D(m[i], x[i], v[i, :], e[i], idx[i], flag[i], tc[i])
end

function particle_transport!(KS, ptc, dt)

    x_old = deepcopy(ptc.x)

    ptc.x += ptc.v[1]*dt
    

    flag = 0
    if ptc.x < KS.pSpace.x0
        flag = 1
    elseif ptc.x > KS.pSpace.x1
        flag = 2
    end
        
    if flag != 0
        vyInit = ptc.v[2]

        ptc.v[1] = dirc[flag]*sqrt(-log(1.0-rand()))
        ptc.v[2] = KitBase.sample_velocity(lw[flag], vw[flag])
        ptc.v[3] = KitBase.sample_velocity(lw[flag])

        dtr = dt*(ptc.x-xw[flag])/(ptc.x-x_old)

        ptc.x = xw[flag] + ptc.v[1]*dtr
    end

end

@time for iter = 1:1000
    for i in 1:1000
        particle_transport!(ks, ptc[i], dt)
    end
end

function vis(KS, ctr, ptc::AbstractArray)

    for i in 1:KS.pSpace.nx
        ctr[i].w .= 0.
    end

    for i in eachindex(ptc)
        idx[i] = KitBase.find_idx(collect(LinRange(KS.pSpace.x0,KS.pSpace.x1,KS.pSpace.nx+1)), ptc[i].x, mode=:uniform)

        ctr[idx[i]].w[1] += ptc[i].m / ctr[idx[i]].dx
        ctr[idx[i]].w[2] += ptc[i].m * ptc[i].v[1] / ctr[idx[i]].dx
        ctr[idx[i]].w[3] += ptc[i].m * ptc[i].v[2] / ctr[idx[i]].dx
        ctr[idx[i]].w[4] += 0.5 * ptc[i].m * sum(ptc[i].v.^ 2) / ctr[idx[i]].dx
    end

    w = zeros(KS.pSpace.nx, 4)
    for i in 1:KS.pSpace.nx
        w[i, :] .= ctr[i].w
    end

    plot(KS.pSpace.x[1:KS.pSpace.nx], w[:, :])
end

vis(ks, ctr, ptc)