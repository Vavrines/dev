function particle_transport!(KS, ctr, particle, particle_temp, dt)

    for i = 1:KS.pSpace.nx
    #for i in axes(ctr, 1)
        ctr[i].wf .= ctr[i].w
    end

    no_particle_temp = 0

    #rand_time = rand(KS.gas.np)


    #counter = 0
    #counter_p = 0


    for i = 1:KS.gas.np
        cell_no = particle[i].idx # including ghost cell!

        #prim = KitBase.conserve_prim(ctr[cell_no].w, KS.gas.γ)
        #tau = KitBase.vhs_collision_time(prim, KS.gas.μᵣ, KS.gas.ω)

        time_c=-ctr[cell_no].τ*log(rand())
        #time_c = -ctr[cell_no].τ * log(rand_time[i])

        
        #if cell_no == 50
        #    counter += 1
        #    if time_c > dt
        #        counter_p+=1
        #    end
        #end
        

        if time_c > dt

            particle[i].x += dt * particle[i].v[1]

            #cell_no_temp = particle[i].idx


            ctr[cell_no].wf[1] -= particle[i].m / ctr[cell_no].dx
            ctr[cell_no].wf[2] -= particle[i].m * particle[i].v[1] / ctr[cell_no].dx
            ctr[cell_no].wf[3] -= 0.5 * particle[i].m * sum(particle[i].v .^ 2) / ctr[cell_no].dx
            

            if KS.pSpace.x0 < particle[i].x < KS.pSpace.x1
                no_particle_temp += 1

                particle_temp[no_particle_temp].m = particle[i].m
                particle_temp[no_particle_temp].x = particle[i].x
                particle_temp[no_particle_temp].v = particle[i].v
                
                cell_no_temp = Int(ceil((particle[i].x - KS.pSpace.x0) / ctr[cell_no].dx))
                particle_temp[no_particle_temp].idx = cell_no_temp

            end
        
        elseif time_c > KitBase.boundary_time(particle[i].x,particle[i].v,ctr[cell_no].x,ctr[cell_no].dx)
        #elseif time_c > abs(0.5 * ctr[cell_no].dx / particle[i].v[1])

            particle[i].x += dt * particle[i].v[1]
            #cell_no_temp = particle[i].idx

            #particle[i].x = particle[i].x + time_c * particle[i].v[1]


            ctr[cell_no].wf[1] -= particle[i].m / ctr[cell_no].dx
            ctr[cell_no].wf[2] -= particle[i].m * particle[i].v[1] / ctr[cell_no].dx
            ctr[cell_no].wf[3] -= 0.5 * particle[i].m * sum(particle[i].v .^ 2) / ctr[cell_no].dx

            if KS.pSpace.x0 < particle[i].x < KS.pSpace.x1
                cell_no_temp = Int(ceil((particle[i].x - KS.pSpace.x0) / ctr[cell_no].dx))
                
                ctr[cell_no_temp].wf[1] += particle[i].m / ctr[cell_no].dx
                ctr[cell_no_temp].wf[2] +=
                    particle[i].m * particle[i].v[1] / ctr[cell_no].dx
                ctr[cell_no_temp].wf[3] +=
                    0.5 * particle[i].m * sum(particle[i].v .^ 2) / ctr[cell_no].dx
            end
        
        end

    end

    #@show counter, counter_p


    for i = 1:KS.pSpace.nx
        ctr[i].w .= 0.0
    end
    for i = 1:no_particle_temp
        particle_cell_temp = particle_temp[i].idx
        ctr[particle_cell_temp].w[1] += particle_temp[i].m / ctr[particle_cell_temp].dx
        ctr[particle_cell_temp].w[2] +=
            particle_temp[i].m * particle_temp[i].v[1] / ctr[particle_cell_temp].dx
        ctr[particle_cell_temp].w[3] +=
            0.5 * particle_temp[i].m * sum(particle_temp[i].v .^ 2) /
            ctr[particle_cell_temp].dx
    end

    #KS.gas.np = no_particle_temp
    #@show no_particle_temp

    return no_particle_temp
end


function particle_collision!(KS, ctr, particle, particle_temp, face, np)

    for i = 1:KS.pSpace.nx
        for j = 1:3
            ctr[i].wf[j] += (face[i].fw[j] - face[i+1].fw[j]) / ctr[i].dx
        end

        
        if ctr[i].wf[1] < 0
            ctr[i].wf .= 0.0
            #ctr[i].wf[1] = 1e-8
        else
            ctr[i].prim = KitBase.conserve_prim(ctr[i].wf, KS.gas.γ)
            if ctr[i].prim[3] < 0
                ctr[i].prim[3] = 1.0e8
            end
        end
        

        ctr[i].w .+= ctr[i].wf

        prim = KitBase.conserve_prim(ctr[i].w, KS.gas.γ)

        
        if ctr[i].w[1] < 0
            ctr[i].w .= 0.0
            #ctr[i].w[1] = 1e-8


            #if prim[3] < 0
            #    prim[3] = 1.0e8
            #end



        else
            prim = KitBase.conserve_prim(ctr[i].w, KS.gas.γ)
            if prim[3] < 0
                prim[3] = 1.0e8
            end
            ctr[i].τ = KitBase.vhs_collision_time(prim, KS.gas.μᵣ, KS.gas.ω)
            ctr[i].w = KitBase.prim_conserve(prim, KS.gas.γ)
        end
        

    end

    no_particle_temp = np


    for i = 1:KS.pSpace.nx
        no_particle_cell_temp = Int(round(ctr[i].wf[1] * ctr[i].dx / KS.gas.m))

        for j = 1:no_particle_cell_temp
            no_particle_temp += 1

            randv = rand(2, 3)
            randx = rand()

            particle_temp[no_particle_temp].m = KS.gas.m
            @. particle_temp[no_particle_temp].v =
                sqrt(-log(randv[1, :]) / ctr[i].prim[3]) * sin(2.0 * pi * randv[2, :])

            #d = truncated(Normal(0.0, sqrt(1.0/ctr[i].prim[3])), KS.vSpace.u0, KS.vSpace.u1)
            #particle_temp[no_particle_temp].v .= rand(d, 3)

            particle_temp[no_particle_temp].v[1] += ctr[i].prim[2]
            particle_temp[no_particle_temp].x = ctr[i].x + (randx - 0.5) * ctr[i].dx
            particle_temp[no_particle_temp].idx = i

        end
    end

    for i = -1:0
        no_particle_cell_temp = Int(round(ctr[i].w[1] * ctr[i].dx / KS.gas.m))

        for j = 1:no_particle_cell_temp
            no_particle_temp += 1
            randv = rand(2, 3)
            randx = rand()
            particle_temp[no_particle_temp].m = KS.gas.m
            @. particle_temp[no_particle_temp].v =
                sqrt(-log(randv[1, :]) / ctr[i].prim[3]) * sin(2.0 * pi * randv[2, :])

            #d = truncated(Normal(0.0, sqrt(1.0/ctr[i].prim[3])), KS.vSpace.u0, KS.vSpace.u1)
            #particle_temp[no_particle_temp].v .= rand(d, 3)

            particle_temp[no_particle_temp].v[1] += ctr[i].prim[2]
            particle_temp[no_particle_temp].x = ctr[i].x + (randx - 0.5) * ctr[i].dx
            particle_temp[no_particle_temp].idx = i

        end
    end

    for i = KS.pSpace.nx+1:KS.pSpace.nx+2
        no_particle_cell_temp = Int(round(ctr[i].w[1] * ctr[i].dx / KS.gas.m))

        for j = 1:no_particle_cell_temp
            no_particle_temp += 1

            randv = rand(2, 3)
            randx = rand()

            particle_temp[no_particle_temp].m = KS.gas.m

            @. particle_temp[no_particle_temp].v =
                sqrt(-log(randv[1, :]) / ctr[i].prim[3]) * sin(2.0 * pi * randv[2, :])

            #d = truncated(Normal(0.0, sqrt(1.0/ctr[i].prim[3])), KS.vSpace.u0, KS.vSpace.u1)
            #particle_temp[no_particle_temp].v .= rand(d, 3)

            particle_temp[no_particle_temp].v[1] += ctr[i].prim[2]
            particle_temp[no_particle_temp].x = ctr[i].x + (randx - 0.5) * ctr[i].dx
            particle_temp[no_particle_temp].idx = i

        end
    end
    

    KitBase.duplicate!(particle, particle_temp, no_particle_temp)
    #KitBase.duplicate!(particle, particle_temp)
    #particle = deepcopy(particle_temp)

    KS.gas.np = no_particle_temp
end

#=
function particle_transport!(KS, ctr, particle, particle_temp, dt)

    for i = 1:KS.pSpace.nx
    #for i in axes(ctr, 1)
        ctr[i].wf .= ctr[i].w
    end

    no_particle_temp = 0

    #rand_time = rand(KS.gas.np)


    #counter = 0
    #counter_p = 0


    for i = 1:KS.gas.np
        cell_no = particle[i].idx # including ghost cell!

        #prim = KitBase.conserve_prim(ctr[cell_no].w, KS.gas.γ)
        #tau = KitBase.vhs_collision_time(prim, KS.gas.μᵣ, KS.gas.ω)

        time_c=-ctr[cell_no].τ*log(rand())
        #time_c = -ctr[cell_no].τ * log(rand_time[i])

        
        #if cell_no == 50
        #    counter += 1
        #    if time_c > dt
        #        counter_p+=1
        #    end
        #end
        

        if time_c > dt

            particle[i].x += dt * particle[i].v[1]

            #cell_no_temp = particle[i].idx


            ctr[cell_no].wf[1] -= particle[i].m / ctr[cell_no].dx
            ctr[cell_no].wf[2] -= particle[i].m * particle[i].v[1] / ctr[cell_no].dx
            ctr[cell_no].wf[3] -= 0.5 * particle[i].m * sum(particle[i].v .^ 2) / ctr[cell_no].dx
            

            if particle[i].x > KS.pSpace.x0 && particle[i].x < KS.pSpace.x1
                no_particle_temp = no_particle_temp + 1

                particle_temp[no_particle_temp].m = particle[i].m
                particle_temp[no_particle_temp].x = particle[i].x
                particle_temp[no_particle_temp].v = particle[i].v
                
                cell_no_temp = Int(ceil((particle[i].x - KS.pSpace.x0) / ctr[cell_no].dx))
                particle_temp[no_particle_temp].idx = cell_no_temp

            end
        
        elseif time_c > KitBase.boundary_time(particle[i].x,particle[i].v,ctr[cell_no].x,ctr[cell_no].dx)
        #elseif time_c > abs(0.5 * ctr[cell_no].dx / particle[i].v[1])

            #particle[i].x = particle[i].x + dt * particle[i].v[1]
            #cell_no_temp = particle[i].idx

            particle[i].x = particle[i].x + time_c * particle[i].v[1]



            ctr[cell_no].wf[1] -= particle[i].m / ctr[cell_no].dx
            ctr[cell_no].wf[2] -= particle[i].m * particle[i].v[1] / ctr[cell_no].dx
            ctr[cell_no].wf[3] -= 0.5 * particle[i].m * sum(particle[i].v .^ 2) / ctr[cell_no].dx

            if particle[i].x > KS.pSpace.x0 && particle[i].x < KS.pSpace.x1
                cell_no_temp = Int(ceil((particle[i].x - KS.pSpace.x0) / ctr[cell_no].dx))
                
                ctr[cell_no_temp].wf[1] += particle[i].m / ctr[cell_no].dx
                ctr[cell_no_temp].wf[2] +=
                    particle[i].m * particle[i].v[1] / ctr[cell_no].dx
                ctr[cell_no_temp].wf[3] +=
                    0.5 * particle[i].m * sum(particle[i].v .^ 2) / ctr[cell_no].dx
            end
        
        end

    end



    #@show counter, counter_p



    for i = 1:KS.pSpace.nx
        ctr[i].w .= 0.0
    end
    for i = 1:no_particle_temp
        particle_cell_temp = particle_temp[i].idx
        ctr[particle_cell_temp].w[1] += particle_temp[i].m / ctr[particle_cell_temp].dx
        ctr[particle_cell_temp].w[2] +=
            particle_temp[i].m * particle_temp[i].v[1] / ctr[particle_cell_temp].dx
        ctr[particle_cell_temp].w[3] +=
            0.5 * particle_temp[i].m * sum(particle_temp[i].v .^ 2) /
            ctr[particle_cell_temp].dx
    end

    #KS.gas.np = no_particle_temp
    #@show no_particle_temp

    return no_particle_temp
end


function particle_collision!(KS, ctr, particle, particle_temp, face, np)

    for i = 1:KS.pSpace.nx
        for j = 1:3
            ctr[i].wf[j] += (face[i].fw[j] - face[i+1].fw[j]) / ctr[i].dx
        end

        #=
        if ctr[i].wf[1] < 0
            ctr[i].wf .= 0.0
            #ctr[i].wf[1] = 1e-8
        else
            ctr[i].prim = KitBase.conserve_prim(ctr[i].wf, KS.gas.γ)
            if ctr[i].prim[3] < 0
                ctr[i].prim[3] = 1.0e8
            end
        end
        =#

        
        if ctr[i].wf[1] < 0
            ctr[i].wf[1] = 1e-8
        end
        if ctr[i].wf[3] < 0
            ctr[i].wf[3] = 1e-8
        end
        ctr[i].prim = KitBase.conserve_prim(ctr[i].wf, KS.gas.γ)
        if ctr[i].prim[1] < 0
            ctr[i].prim[1] = 1.0e-8
        end
        if ctr[i].prim[3] < 0
            ctr[i].prim[3] = 1.0e8
        end




        ctr[i].w .+= ctr[i].wf
        if ctr[i].w[1] < 0
            ctr[i].w[1] = 1e-8
        end
        if ctr[i].w[3] < 0
            ctr[i].w[3] = 1e-8
        end


        prim = KitBase.conserve_prim(ctr[i].w, KS.gas.γ)

        #=
        if ctr[i].w[1] < 0
            ctr[i].w .= 0.0
            #ctr[i].w[1] = 1e-8
        else
            prim = KitBase.conserve_prim(ctr[i].w, KS.gas.γ)
            if prim[3] < 0
                prim[3] = 1.0e8
            end
            ctr[i].τ = KitBase.vhs_collision_time(prim, KS.gas.μᵣ, KS.gas.ω)
            ctr[i].w = KitBase.prim_conserve(prim, KS.gas.γ)
        end
        =#

        if prim[1] < 0
            prim[1] = 1.0e-8
        end
        if prim[3] < 0
            prim[3] = 1.0e8
        end

        ctr[i].τ = KitBase.vhs_collision_time(prim, KS.gas.μᵣ, KS.gas.ω)

    end

    no_particle_temp = np


    for i = 1:KS.pSpace.nx
        no_particle_cell_temp = Int(round(ctr[i].wf[1] * ctr[i].dx / KS.gas.m))

        for j = 1:no_particle_cell_temp
            no_particle_temp += 1

            randv = rand(2, 3)
            randx = rand()

            particle_temp[no_particle_temp].m = KS.gas.m
            @. particle_temp[no_particle_temp].v =
                sqrt(-log(randv[1, :]) / ctr[i].prim[3]) * sin(2.0 * pi * randv[2, :])

            #d = truncated(Normal(0.0, sqrt(1.0/ctr[i].prim[3])), KS.vSpace.u0, KS.vSpace.u1)
            #particle_temp[no_particle_temp].v .= rand(d, 3)

            particle_temp[no_particle_temp].v[1] =
                particle_temp[no_particle_temp].v[1] + ctr[i].prim[2]
            particle_temp[no_particle_temp].x = ctr[i].x + (randx - 0.5) * ctr[i].dx
            particle_temp[no_particle_temp].idx = i

        end
    end

    for i = -1:0
        no_particle_cell_temp = Int(round(ctr[i].w[1] * ctr[i].dx / KS.gas.m))

        for j = 1:no_particle_cell_temp
            no_particle_temp = no_particle_temp + 1
            randv = rand(2, 3)
            randx = rand()
            particle_temp[no_particle_temp].m = KS.gas.m
            @. particle_temp[no_particle_temp].v =
                sqrt(-log(randv[1, :]) / ctr[i].prim[3]) * sin(2.0 * pi * randv[2, :])

            #d = truncated(Normal(0.0, sqrt(1.0/ctr[i].prim[3])), KS.vSpace.u0, KS.vSpace.u1)
            #particle_temp[no_particle_temp].v .= rand(d, 3)

            particle_temp[no_particle_temp].v[1] =
                particle_temp[no_particle_temp].v[1] + ctr[i].prim[2]
            particle_temp[no_particle_temp].x = ctr[i].x + (randx - 0.5) * ctr[i].dx
            particle_temp[no_particle_temp].idx = i

        end
    end

    for i = KS.pSpace.nx+1:KS.pSpace.nx+2
        no_particle_cell_temp = Int(round(ctr[i].w[1] * ctr[i].dx / KS.gas.m))

        for j = 1:no_particle_cell_temp
            no_particle_temp = no_particle_temp + 1

            randv = rand(2, 3)
            randx = rand()

            particle_temp[no_particle_temp].m = KS.gas.m

            @. particle_temp[no_particle_temp].v =
                sqrt(-log(randv[1, :]) / ctr[i].prim[3]) * sin(2.0 * pi * randv[2, :])

            #d = truncated(Normal(0.0, sqrt(1.0/ctr[i].prim[3])), KS.vSpace.u0, KS.vSpace.u1)
            #particle_temp[no_particle_temp].v .= rand(d, 3)

            particle_temp[no_particle_temp].v[1] =
                particle_temp[no_particle_temp].v[1] + ctr[i].prim[2]
            particle_temp[no_particle_temp].x = ctr[i].x + (randx - 0.5) * ctr[i].dx
            particle_temp[no_particle_temp].idx = i

        end
    end
    

    #KitBase.duplicate!(particle, particle_temp, no_particle_temp)
    KitBase.duplicate!(particle, particle_temp)
    #particle = deepcopy(particle_temp)

    KS.gas.np = no_particle_temp
end
=#