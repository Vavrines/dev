v = zeros(2000, 3)
for i in axes(v, 1), j in 1:3
    v[i, j] = ptc[i+90000].v[j]
end

using Plots
histogram(v[:, 1], label = "u")
histogram(v[:, 2], label = "v")
histogram(v[:, 3], label = "w")

w = zeros(3)
for i in 1:2000
    w[1]+=ks.gas.m/ctr[1].dx
    w[2]+=ks.gas.m*v[i,1]/ctr[1].dx
    w[3]+=0.5*ks.gas.m*sum(v[i,:].^2)/ctr[1].dx
end

w


