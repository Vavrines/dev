    !--------------------------------------------------
    !>store the global variables
    !--------------------------------------------------
    module global_data
    !--------------------------------------------------
    !kind selection
    !--------------------------------------------------
    integer,parameter :: RKD = 8

    !--------------------------------------------------
    !variables to control the simulation
    !--------------------------------------------------
    real(kind=RKD),parameter :: PI = 4.0*atan(1.0) !Pi
    real(kind=RKD),parameter :: SMV = tiny(real(1.0,8)) !small value to avoid 0/0
    real(kind=RKD),parameter :: UP = 1.0 !used in sign() function
    real(kind=RKD) :: cfl !global CFL number
    real(kind=RKD) :: dt !global time step
    real(kind=RKD) :: sim_time !current simulation time
    real(kind=RKD) :: max_time !maximum simulation time
    character(len=9),parameter :: HSTFILENAME = "shock.hst" !history file name
    character(len=9),parameter :: RSTFILENAME = "shock.plt" !result file name
    integer :: iter !iteration
    integer :: method_output !output the solution with normalized value or not

    !--------------------------------------------------
    !gas properties
    !--------------------------------------------------
    real(kind=RKD) :: particle_mass
    real(kind=RKD) :: gam !ratio of specific heat
    real(kind=RKD) :: omega !temperature dependence index in HS/VHS/VSS model
    real(kind=RKD) :: pr !Prandtl number
    real(kind=RKD) :: mu_ref !viscosity coefficient in reference state
    integer :: ck !internal degree of freedom
    integer :: no_particle,no_particle_temp

    !--------------------------------------------------
    !macros for a readable code
    !--------------------------------------------------
    !I/O
    integer,parameter :: HSTFILE = 20 !history file ID
    integer,parameter :: RSTFILE = 21 !result file ID
    !output method
    integer,parameter :: ORIGINAL = 0 !do not normalize the solution
    integer,parameter :: NORMALIZE = 1 !normalize the solution

    !--------------------------------------------------
    !basic derived type
    !--------------------------------------------------
    !cell center
    type :: cell_center
        !geometry
        real(kind=RKD) :: x !cell center coordinates
        real(kind=RKD) :: length !length
        !flow field
        real(kind=RKD) :: w(3),sw(3) !density, x-momentum,total energy
        real(kind=RKD) :: wg(3) !density, x-momentum,total energy
        real(kind=RKD) :: prim(3) !density, x-momentum,total energy
        real(kind=RKD) :: tau !density, x-momentum,total energy
        real(kind=RKD) :: primary(3) !density,temperature
    end type cell_center

    !cell interface
    type :: cell_interface
        real(kind=RKD) :: flux(3) !mass flux, x momentum flux, energy flux
    end type cell_interface

    !simulation particle
    type :: simulation_particle
        real(kind=RKD) :: mass
        real(kind=RKD) :: x
        real(kind=RKD) :: v(3)
        real(kind=RKD) :: cell_no
        real(kind=RKD) :: time_b
    end type simulation_particle

    !--------------------------------------------------
    !flow field
    !--------------------------------------------------
    !index method
    !     ----------------
    !  (i)|      (i)     |(i+1)
    !     ----------------
    integer :: ixmin,ixmax !index range
    real(kind=RKD) :: dx
    type(cell_center),allocatable,dimension(:) :: ctr !cell centers
    type(cell_interface),allocatable,dimension(:) :: vface !vertical interfaces
    type(simulation_particle),allocatable,dimension(:) :: particle,particle_temp
    end module global_data

    !--------------------------------------------------
    !>define some commonly used functions/subroutines
    !--------------------------------------------------
    module tools
    use global_data
    implicit none
    contains
    !--------------------------------------------------
    !>convert primary variables to conservative variables
    !>@param[in] prim          :primary variables
    !>@return    get_conserved :conservative variables
    !--------------------------------------------------
    function get_conserved(prim)
    real(kind=RKD),intent(in) :: prim(3)
    real(kind=RKD) :: get_conserved(3)

    get_conserved(1) = prim(1)
    get_conserved(2) = prim(1)*prim(2)
    get_conserved(3) = 0.5*prim(1)/prim(3)/(gam-1.0)+0.5*prim(1)*prim(2)**2
    end function get_conserved

    !--------------------------------------------------
    !>convert conservative variables to primary variables
    !>@param[in] w           :conservative variables
    !>@return    get_primary :conservative variables
    !--------------------------------------------------
    function get_primary(w)
    real(kind=RKD),intent(in) :: w(3)
    real(kind=RKD) :: get_primary(3) !primary variables

    get_primary(1) = w(1)
    get_primary(2) = w(2)/w(1)
    get_primary(3) = 0.5*w(1)/(gam-1.0)/(w(3)-0.5*w(2)**2/w(1))
    end function get_primary

    !--------------------------------------------------
    !>obtain ratio of specific heat
    !>@param[in] ck        :internal degree of freedom
    !>@return    get_gamma :ratio of specific heat
    !--------------------------------------------------
    function get_gamma(ck)
    integer,intent(in) :: ck
    real(kind=RKD) :: get_gamma

    get_gamma = float(ck+3)/float(ck+1)
    end function get_gamma

    !--------------------------------------------------
    !>obtain speed of sound
    !>@param[in] prim    :primary variables
    !>@return    get_sos :speed of sound
    !--------------------------------------------------
    function get_sos(prim)
    real(kind=RKD),intent(in) :: prim(3)
    real(kind=RKD) :: get_sos !speed of sound

    get_sos = sqrt(0.5*gam/prim(3))
    end function get_sos

    !--------------------------------------------------
    !>calculate collision time
    !>@param[in] prim    :primary variables
    !>@return    get_tau :collision time
    !--------------------------------------------------
    function get_tau(prim)
    real(kind=RKD),intent(in) :: prim(3)
    real(kind=RKD) :: get_tau

    get_tau = mu_ref*2*prim(3)**(1-omega)/prim(1)
    end function get_tau

    !--------------------------------------------------
    !>get the nondimensionalized viscosity coefficient
    !>@param[in] kn          :Knudsen number
    !>@param[in] alpha,omega :index related to HS/VHS/VSS model
    !>@return    get_mu      :nondimensionalized viscosity coefficient
    !--------------------------------------------------
    function get_mu(kn,alpha,omega)
    real(kind=RKD),intent(in) :: kn,alpha,omega
    real(kind=RKD) :: get_mu

    get_mu = 5*(alpha+1)*(alpha+2)*sqrt(PI)/(4*alpha*(5-2*omega)*(7-2*omega))*kn
    end function get_mu
    end module tools

    !--------------------------------------------------
    !>flux calculation
    !--------------------------------------------------
    module flux
    use global_data
    use tools
    implicit none

    integer,parameter :: MNUM = 6 !number of normal velocity moments
    integer,parameter :: MTUM = 4 !number of tangential velocity moments

    contains
    !--------------------------------------------------
    !>calculate flux of inner interface
    !>@param[in]    cell_L :cell left to the target interface
    !>@param[inout] face   :the target interface
    !>@param[in]    cell_R :cell right to the target interface
    !--------------------------------------------------
    subroutine calc_flux(cell_L,face,cell_R)
    type(cell_center),intent(in) :: cell_L,cell_R
    type(cell_interface),intent(inout) :: face
    real(kind=RKD) :: w(3),prim(3),w_L(3),prim_L(3),w_R(3),prim_R(3) !conservative and primary variables at the interface
    real(kind=RKD) :: qf !heat flux in normal and tangential direction
    real(kind=RKD) :: sw(3) !slope of W
    real(kind=RKD) :: aL(3),aR(3),aT(3) !micro slope of Maxwellian distribution, left,right and time.
    real(kind=RKD) :: Mu(0:MNUM),Mu_L(0:MNUM),Mu_R(0:MNUM),Mxi(0:2) !<u^n>,<u^n>_{>0},<u^n>_{<0},<\xi^l>
    real(kind=RKD) :: Mau_0(3),Mau_L(3),Mau_R(3),Mau_T(3) !<u\psi>,<aL*u^n*\psi>,<aR*u^n*\psi>,<A*u*\psi>
    real(kind=RKD) :: tau !collision time
    real(kind=RKD) :: Mt(5) !some time integration terms
    integer :: i,j

    !--------------------------------------------------
    !obtain macroscopic variables
    !--------------------------------------------------
    !conservative variables w_0
    w_L=cell_L%w+0.5*cell_L%sw*cell_L%length
    prim_L = get_primary(w_L)
    call calc_moment_u(prim_L,Mu,Mxi,Mu_L,Mu_R)
    w=prim_L(1)*moment_uv(Mu_L,Mxi,0,0)
    w_R=cell_R%w-0.5*cell_R%sw*cell_R%length
    prim_R = get_primary(w_R)
    call calc_moment_u(prim_R,Mu,Mxi,Mu_L,Mu_R)
    w=w+prim_R(1)*moment_uv(Mu_R,Mxi,0,0)

    if(.true.)then !(prim_L(3)<0 .or. prim_R(3)<0)then
        prim_L = get_primary(cell_L%w)
        call calc_moment_u(prim_L,Mu,Mxi,Mu_L,Mu_R)
        w=prim_L(1)*moment_uv(Mu_L,Mxi,0,0)
        prim_R = get_primary(cell_R%w)
        call calc_moment_u(prim_R,Mu,Mxi,Mu_L,Mu_R)
        w=w+prim_R(1)*moment_uv(Mu_R,Mxi,0,0)
        prim = get_primary(w)
        tau = get_tau(prim)

        Mt(4) = tau*(1.0-exp(-dt/tau))
        Mt(5) = -tau*dt*exp(-dt/tau)+tau*Mt(4)
        Mt(1) = dt-Mt(4)
        Mt(2) = -tau*Mt(1)+Mt(5)
        Mt(3) = dt**2/2.0-tau*Mt(1)

        !--------------------------------------------------
        !calculate the flux of conservative variables related to g0
        !--------------------------------------------------
        Mau_0 = moment_uv(Mu,Mxi,1,0) !<u*\psi>

        face%flux = Mt(1)*prim(1)*Mau_0
    else
        !convert to primary variables
        prim = get_primary(w)


        !--------------------------------------------------
        !calculate a^L,a^R
        !--------------------------------------------------
        sw = (w-cell_L%w)/(0.5*cell_L%length) !left slope of W
        aL = micro_slope(prim,sw) !calculate a^L

        sw = (cell_R%w-w)/(0.5*cell_R%length) !right slope of W
        aR = micro_slope(prim,sw) !calculate a^R

        !--------------------------------------------------
        !calculate time slope of W and A
        !--------------------------------------------------
        !<u^n>,<\xi^l>,<u^n>_{>0},<u^n>_{<0}
        call calc_moment_u(prim,Mu,Mxi,Mu_L,Mu_R)

        Mau_L = moment_au(aL,Mu_L,Mxi,1) !<aL*u*\psi>_{>0}
        Mau_R = moment_au(aR,Mu_R,Mxi,1) !<aR*u*\psi>_{<0}

        sw = -prim(1)*(Mau_L+Mau_R) !time slope of W
        aT = micro_slope(prim,sw) !calculate A

        !--------------------------------------------------
        !calculate collision time and some time integration terms
        !--------------------------------------------------
        tau = get_tau(prim)

        Mt(4) = tau*(1.0-exp(-dt/tau))
        Mt(5) = -tau*dt*exp(-dt/tau)+tau*Mt(4)
        Mt(1) = dt-Mt(4)
        Mt(2) = -tau*Mt(1)+Mt(5)
        Mt(3) = dt**2/2.0-tau*Mt(1)

        !--------------------------------------------------
        !calculate the flux of conservative variables related to g0
        !--------------------------------------------------
        Mau_0 = moment_uv(Mu,Mxi,1,0) !<u*\psi>
        Mau_L = moment_au(aL,Mu_L,Mxi,2) !<aL*u^2*\psi>_{>0}
        Mau_R = moment_au(aR,Mu_R,Mxi,2) !<aR*u^2*\psi>_{<0}
        Mau_T = moment_au(aT,Mu,Mxi,1) !<A*u*\psi>

        face%flux = Mt(1)*prim(1)*Mau_0+Mt(2)*prim(1)*(Mau_L+Mau_R)+Mt(3)*prim(1)*Mau_T
    endif
    do i=1,3
        if(isnan(face%flux(i)))then
            write(*,*) 'flux'
            stop
        endif
    enddo
    end subroutine calc_flux

    !--------------------------------------------------
    !>calculate micro slope of Maxwellian distribution
    !>@param[in] prim        :primary variables
    !>@param[in] sw          :slope of W
    !>@return    micro_slope :slope of Maxwellian distribution
    !--------------------------------------------------
    function micro_slope(prim,sw)
    real(kind=RKD),intent(in) :: prim(3),sw(3)
    real(kind=RKD) :: micro_slope(3)

    micro_slope(3) = 4.0*prim(3)**2/(ck+1)/prim(1)*(2.0*sw(3)-2.0*prim(2)*sw(2)+sw(1)*(prim(2)**2-0.5*(ck+1)/prim(3)))

    micro_slope(2) = 2.0*prim(3)/prim(1)*(sw(2)-prim(2)*sw(1))-prim(2)*micro_slope(3)
    micro_slope(1) = sw(1)/prim(1)-prim(2)*micro_slope(2)-0.5*(prim(2)**2+0.5*(ck+1)/prim(3))*micro_slope(3)
    end function micro_slope

    !--------------------------------------------------
    !>calculate moments of velocity
    !>@param[in] prim :primary variables
    !>@param[out] Mu        :<u^n>
    !>@param[out] Mxi       :<\xi^l>
    !>@param[out] Mu_L,Mu_R :<u^n>_{>0},<u^n>_{<0}
    !--------------------------------------------------
    subroutine calc_moment_u(prim,Mu,Mxi,Mu_L,Mu_R)
    real(kind=RKD),intent(in) :: prim(3)
    real(kind=RKD),intent(out) :: Mu(0:MNUM),Mu_L(0:MNUM),Mu_R(0:MNUM)
    real(kind=RKD),intent(out) :: Mxi(0:2)
    integer :: i

    !moments of normal velocity
    Mu_L(0) = 0.5*erfc(-sqrt(prim(3))*prim(2))
    Mu_L(1) = prim(2)*Mu_L(0)+0.5*exp(-prim(3)*prim(2)**2)/sqrt(PI*prim(3))
    Mu_R(0) = 0.5*erfc(sqrt(prim(3))*prim(2))
    Mu_R(1) = prim(2)*Mu_R(0)-0.5*exp(-prim(3)*prim(2)**2)/sqrt(PI*prim(3))

    do i=2,MNUM
        Mu_L(i) = prim(2)*Mu_L(i-1)+0.5*(i-1)*Mu_L(i-2)/prim(3)
        Mu_R(i) = prim(2)*Mu_R(i-1)+0.5*(i-1)*Mu_R(i-2)/prim(3)
    end do

    Mu = Mu_L+Mu_R

    !moments of \xi
    Mxi(0) = 1.0 !<\xi^0>
    Mxi(1) = 0.5*ck/prim(3) !<\xi^2>
    Mxi(2) = (ck**2+2.0*ck)/(4.0*prim(3)**2) !<\xi^4>
    end subroutine calc_moment_u

    !--------------------------------------------------
    !>calculate <u^\alpha*\xi^\delta*\psi>
    !>@param[in] Mu        :<u^\alpha>
    !>@param[in] Mxi       :<\xi^l>
    !>@param[in] alpha     :exponential index of u
    !>@param[in] delta     :exponential index of \xi
    !>@return    moment_uv :moment of <u^\alpha*\xi^\delta*\psi>
    !--------------------------------------------------
    function moment_uv(Mu,Mxi,alpha,delta)
    real(kind=RKD),intent(in) :: Mu(0:MNUM),Mxi(0:2)
    integer,intent(in) :: alpha,delta
    real(kind=RKD) :: moment_uv(3)

    moment_uv(1) = Mu(alpha)*Mxi(delta/2)
    moment_uv(2) = Mu(alpha+1)*Mxi(delta/2)
    moment_uv(3) = 0.5*(Mu(alpha+2)*Mxi(delta/2)+Mu(alpha)*Mxi((delta+2)/2))
    end function moment_uv

    !--------------------------------------------------
    !>calculate <a*u^\alpha*\psi>
    !>@param[in] a         :micro slope of Maxwellian
    !>@param[in] Mu        :<u^\alpha>
    !>@param[in] Mxi       :<\xi^l>
    !>@param[in] alpha     :exponential index of u
    !>@return    moment_au :moment of <a*u^\alpha*\psi>
    !--------------------------------------------------
    function moment_au(a,Mu,Mxi,alpha)
    real(kind=RKD),intent(in) :: a(3)
    real(kind=RKD),intent(in) :: Mu(0:MNUM),Mxi(0:2)
    integer,intent(in) :: alpha
    real(kind=RKD) :: moment_au(3)

    moment_au = a(1)*moment_uv(Mu,Mxi,alpha+0,0)+&
        a(2)*moment_uv(Mu,Mxi,alpha+1,0)+&
        0.5*a(3)*moment_uv(Mu,Mxi,alpha+2,0)+&
        0.5*a(3)*moment_uv(Mu,Mxi,alpha+0,2)
    end function moment_au
    end module flux

    !--------------------------------------------------
    !>UGKS solver
    !--------------------------------------------------
    module solver
    use global_data
    use tools
    use flux
    implicit none
    contains
    !--------------------------------------------------
    !>calculate time step
    !--------------------------------------------------
    subroutine timestep()
    real(kind=RKD) :: tmax !max 1/dt allowed
    real(kind=RKD) :: sos !speed of sound
    real(kind=RKD) :: prim(3) !primary variables
    integer :: i

    !set initial value
    tmax = 0.0

    !$omp parallel
    !$omp do private(i,sos,prim) reduction(max:tmax)
    do i=ixmin,ixmax
        !convert conservative variables to primary variables
        prim = get_primary(ctr(i)%w)

        !sound speed
        sos = get_sos(prim)

        !maximum velocity
        prim(2) = abs(prim(2))+3*sos

        !maximum 1/dt allowed
        tmax = max(tmax,prim(2)/ctr(i)%length)
    end do
    !$omp end do
    !$omp end parallel

    !time step
    dt = cfl/tmax
    end subroutine timestep

    !--------------------------------------------------
    !>calculate the slope of distribution function
    !--------------------------------------------------
    subroutine interpolation()
    integer :: i

    !$omp parallel
    !$omp do
    do i=ixmin-1,ixmax+1
        call interp_inner(ctr(i-1),ctr(i),ctr(i+1))
    end do
    !$omp end do nowait
    !$omp end parallel
    end subroutine interpolation

    !--------------------------------------------------
    !>calculate the flux across the interfaces
    !--------------------------------------------------
    subroutine evolution()
    integer :: i

    !$omp parallel
    !$omp do
    do i=ixmin,ixmax+1 !with ghost cell
        call calc_flux(ctr(i-1),vface(i),ctr(i))
    end do
    !$omp end do nowait
    !$omp end parallel
    end subroutine evolution

    !--------------------------------------------------
    !>update cell averaged values
    !--------------------------------------------------
    subroutine update_f()
    real(kind=RKD),allocatable,dimension(:) :: rand_time
    real(kind=RKD) :: time_c
    real(kind=RKD) :: particle_cell_temp
    integer :: cell_no,cell_no_temp
    integer :: i,j

    integer :: counter, counter_p

    counter = 0 
    counter_p = 0


    allocate(rand_time(no_particle))

    call RANDOM_NUMBER(rand_time)

    forall(i=ixmin:ixmax)
        ctr(i)%wg=ctr(i)%w
    endforall

    no_particle_temp=0
    !do i=1,no_particle
    !    cell_no=particle(i)%cell_no
    !    time_c=-ctr(cell_no)%tau*log(rand_time(i))
    !    if(time_c>dt)then
    !        particle(i)%x=particle(i)%x+dt*particle(i)%v(1)
    !        if(particle(i)%x>0 .and. particle(i)%x<1)then
    !            no_particle_temp=no_particle_temp+1
    !            particle_temp(no_particle_temp)%mass=particle(i)%mass
    !            particle_temp(no_particle_temp)%x=particle(i)%x
    !            particle_temp(no_particle_temp)%v=particle(i)%v
    !            cell_no_temp=ceiling(particle(i)%x/dx)
    !            particle_temp(no_particle_temp)%cell_no=cell_no_temp
    !            if(particle_temp(no_particle_temp)%v(1)<0)then
    !                particle_temp(no_particle_temp)%time_b=(ctr(cell_no_temp)%x-0.5*dx-particle_temp(no_particle_temp)%x)/particle_temp(no_particle_temp)%v(1)
    !            elseif(particle_temp(no_particle_temp)%v(1)>0)then
    !                particle_temp(no_particle_temp)%time_b=(ctr(cell_no_temp)%x+0.5*dx-particle_temp(no_particle_temp)%x)/particle_temp(no_particle_temp)%v(1)
    !            else
    !                particle_temp(no_particle_temp)%time_b=1
    !            endif
    !        endif
    !    elseif(time_c>particle(i)%time_b)then
    !        particle(i)%x=particle(i)%x+dt*particle(i)%v(1)
    !        if(particle(i)%x>0 .and. particle(i)%x<1)then
    !            cell_no_temp=ceiling(particle(i)%x/dx)
    !            ctr(cell_no_temp)%wg(1)=ctr(cell_no_temp)%wg(1)+particle(i)%mass/dx
    !            ctr(cell_no_temp)%wg(2)=ctr(cell_no_temp)%wg(2)+particle(i)%mass*particle(i)%v(1)/dx
    !            ctr(cell_no_temp)%wg(3)=ctr(cell_no_temp)%wg(3)+0.5*particle(i)%mass*sum(particle(i)%v(:)**2)/dx
    !        endif
    !    else
    !        ctr(cell_no)%wg(1)=ctr(cell_no)%wg(1)+particle(i)%mass/dx
    !        ctr(cell_no)%wg(2)=ctr(cell_no)%wg(2)+particle(i)%mass*particle(i)%v(1)/dx
    !        ctr(cell_no)%wg(3)=ctr(cell_no)%wg(3)+0.5*particle(i)%mass*sum(particle(i)%v(:)**2)/dx
    !    endif
    !enddo




    


    do i=1,no_particle
        cell_no=particle(i)%cell_no
        time_c=-ctr(cell_no)%tau*log(rand_time(i))

        if(cell_no == 50)then
            counter = counter + 1
            if(time_c > dt)then
                counter_p= counter_p + 1
            endif
        endif



        if(time_c>dt)then
            particle(i)%x=particle(i)%x+dt*particle(i)%v(1)
            cell_no_temp=particle(i)%cell_no
            ctr(cell_no)%wg(1)=ctr(cell_no)%wg(1)-particle(i)%mass/dx
            ctr(cell_no)%wg(2)=ctr(cell_no)%wg(2)-particle(i)%mass*particle(i)%v(1)/dx
            ctr(cell_no)%wg(3)=ctr(cell_no)%wg(3)-0.5*particle(i)%mass*sum(particle(i)%v(:)**2)/dx
            if(particle(i)%x>0 .and. particle(i)%x<50)then
                no_particle_temp=no_particle_temp+1
                particle_temp(no_particle_temp)%mass=particle(i)%mass
                particle_temp(no_particle_temp)%x=particle(i)%x
                particle_temp(no_particle_temp)%v=particle(i)%v
                cell_no_temp=ceiling(particle(i)%x/dx)
                particle_temp(no_particle_temp)%cell_no=cell_no_temp
                if(particle_temp(no_particle_temp)%v(1)<0)then
                    particle_temp(no_particle_temp)%time_b=(ctr(cell_no_temp)%x-&
                        0.5*dx-particle_temp(no_particle_temp)%x)/particle_temp(no_particle_temp)%v(1)
                elseif(particle_temp(no_particle_temp)%v(1)>0)then
                    particle_temp(no_particle_temp)%time_b=(ctr(cell_no_temp)%x+&
                        0.5*dx-particle_temp(no_particle_temp)%x)/particle_temp(no_particle_temp)%v(1)
                else
                    particle_temp(no_particle_temp)%time_b=1
                endif
            endif
        elseif(time_c>particle(i)%time_b)then
            particle(i)%x=particle(i)%x+dt*particle(i)%v(1)
            cell_no_temp=particle(i)%cell_no
            ctr(cell_no)%wg(1)=ctr(cell_no)%wg(1)-particle(i)%mass/dx
            ctr(cell_no)%wg(2)=ctr(cell_no)%wg(2)-particle(i)%mass*particle(i)%v(1)/dx
            ctr(cell_no)%wg(3)=ctr(cell_no)%wg(3)-0.5*particle(i)%mass*sum(particle(i)%v(:)**2)/dx
            if(particle(i)%x>0 .and. particle(i)%x<50)then
                cell_no_temp=ceiling(particle(i)%x/dx)
                ctr(cell_no_temp)%wg(1)=ctr(cell_no_temp)%wg(1)+particle(i)%mass/dx
                ctr(cell_no_temp)%wg(2)=ctr(cell_no_temp)%wg(2)+particle(i)%mass*particle(i)%v(1)/dx
                ctr(cell_no_temp)%wg(3)=ctr(cell_no_temp)%wg(3)+0.5*particle(i)%mass*sum(particle(i)%v(:)**2)/dx
            endif
        endif
    enddo


    write(*,*) counter, counter_p



    forall(i=ixmin:ixmax)
        ctr(i)%w=0
    endforall
    do i=1,no_particle_temp
        particle_cell_temp=particle_temp(i)%cell_no
        ctr(particle_cell_temp)%w(1)=ctr(particle_cell_temp)%w(1)+particle_temp(i)%mass/dx
        ctr(particle_cell_temp)%w(2)=ctr(particle_cell_temp)%w(2)+particle_temp(i)%mass*particle_temp(i)%v(1)/dx
        ctr(particle_cell_temp)%w(3)=ctr(particle_cell_temp)%w(3)+0.5*particle_temp(i)%mass*sum(particle_temp(i)%v(:)**2)/dx
    enddo
    end subroutine update_f

    !--------------------------------------------------
    !>update cell averaged values
    !--------------------------------------------------
    subroutine update_g()
    real(kind=RKD) :: randv(2,3),randx
    real(kind=RKD) :: prim(3)
    integer :: no_particle_cell_temp,cell_no_temp
    integer :: i,j

    do i=ixmin,ixmax
        ctr(i)%wg=ctr(i)%wg+(vface(i)%flux-vface(i+1)%flux)/ctr(i)%length
        do j=1,3
            if(isnan(ctr(i)%wg(j)))then
                write(*,*) 'update'
                stop
            endif
        enddo
        if(ctr(i)%wg(1).le.0)then
            ctr(i)%wg=0
        else
            ctr(i)%prim=get_primary(ctr(i)%wg)
            if(ctr(i)%prim(3)<0) ctr(i)%prim(3)=1.0d8
        endif
        
        ctr(i)%w=ctr(i)%w+ctr(i)%wg
        prim=get_primary(ctr(i)%w)
        if(ctr(i)%w(1).le.0)then
            ctr(i)%w=0
        else
            prim=get_primary(ctr(i)%w)
            if(prim(3)<0) prim(3)=1.0d8
            ctr(i)%tau=get_tau(prim)
            ctr(i)%w=get_conserved(prim)
        endif
        do j=1,3
            if(isnan(ctr(i)%w(j)))then
                write(*,*) 'update', ctr(i)%w, ctr(i)%wg,prim
                stop
            endif
        enddo
    enddo


    do i=ixmin,ixmax
        no_particle_cell_temp=nint(ctr(i)%wg(1)*dx/particle_mass)
        do j=1,no_particle_cell_temp
            no_particle_temp=no_particle_temp+1
            call RANDOM_NUMBER(randv)
            call RANDOM_NUMBER(randx)
            particle_temp(no_particle_temp)%mass=particle_mass
            particle_temp(no_particle_temp)%v=sqrt(-log(randv(1,:))/ctr(i)%prim(3))*sin(2.0d0*pi*randv(2,:))
            particle_temp(no_particle_temp)%v(1)=particle_temp(no_particle_temp)%v(1)+ctr(i)%prim(2)
            particle_temp(no_particle_temp)%x=ctr(i)%x+(randx-0.5)*dx
            particle_temp(no_particle_temp)%cell_no=i
            if(particle_temp(no_particle_temp)%v(1)<0)then
                particle_temp(no_particle_temp)%time_b=(ctr(i)%x-&
                    0.5*dx-particle_temp(no_particle_temp)%x)/particle_temp(no_particle_temp)%v(1)
            elseif(particle_temp(no_particle_temp)%v(1)>0)then
                particle_temp(no_particle_temp)%time_b=(ctr(i)%x+&
                    0.5*dx-particle_temp(no_particle_temp)%x)/particle_temp(no_particle_temp)%v(1)
            else
                particle_temp(no_particle_temp)%time_b=1
            endif
        enddo
    enddo

    do i=ixmin-2,ixmin-1
        no_particle_cell_temp=nint(ctr(i)%w(1)*dx/particle_mass)
        do j=1,no_particle_cell_temp
            no_particle_temp=no_particle_temp+1
            call RANDOM_NUMBER(randv)
            call RANDOM_NUMBER(randx)
            particle_temp(no_particle_temp)%mass=particle_mass
            particle_temp(no_particle_temp)%v=sqrt(-log(randv(1,:))/ctr(i)%prim(3))*sin(2.0d0*pi*randv(2,:))
            particle_temp(no_particle_temp)%v(1)=particle_temp(no_particle_temp)%v(1)+ctr(i)%prim(2)
            particle_temp(no_particle_temp)%x=ctr(i)%x+(randx-0.5)*dx
            particle_temp(no_particle_temp)%cell_no=i
            if(particle_temp(no_particle_temp)%v(1)<0)then
                particle_temp(no_particle_temp)%time_b=(ctr(i)%x-&
                    0.5*dx-particle_temp(no_particle_temp)%x)/particle_temp(no_particle_temp)%v(1)
            elseif(particle_temp(no_particle_temp)%v(1)>0)then
                particle_temp(no_particle_temp)%time_b=(ctr(i)%x+&
                    0.5*dx-particle_temp(no_particle_temp)%x)/particle_temp(no_particle_temp)%v(1)
            else
                particle_temp(no_particle_temp)%time_b=1
            endif
        enddo
    enddo

    do i=ixmax+1,ixmax+2
        no_particle_cell_temp=nint(ctr(i)%w(1)*dx/particle_mass)
        do j=1,no_particle_cell_temp
            no_particle_temp=no_particle_temp+1
            call RANDOM_NUMBER(randv)
            call RANDOM_NUMBER(randx)
            particle_temp(no_particle_temp)%mass=particle_mass
            particle_temp(no_particle_temp)%v=sqrt(-log(randv(1,:))/ctr(i)%prim(3))*sin(2.0d0*pi*randv(2,:))
            particle_temp(no_particle_temp)%v(1)=particle_temp(no_particle_temp)%v(1)+ctr(i)%prim(2)
            particle_temp(no_particle_temp)%x=ctr(i)%x+(randx-0.5)*dx
            particle_temp(no_particle_temp)%cell_no=i
            if(particle_temp(no_particle_temp)%v(1)<0)then
                particle_temp(no_particle_temp)%time_b=(ctr(i)%x-&
                    0.5*dx-particle_temp(no_particle_temp)%x)/particle_temp(no_particle_temp)%v(1)
            elseif(particle_temp(no_particle_temp)%v(1)>0)then
                particle_temp(no_particle_temp)%time_b=(ctr(i)%x+&
                    0.5*dx-particle_temp(no_particle_temp)%x)/particle_temp(no_particle_temp)%v(1)
            else
                particle_temp(no_particle_temp)%time_b=1
            endif
        enddo
    enddo
    no_particle=no_particle_temp
    particle=particle_temp
    end subroutine update_g

    !--------------------------------------------------
    !>interpolation of the inner cells
    !>@param[in]    cell_L :the left cell
    !>@param[inout] cell_N :the target cell
    !>@param[in]    cell_R :the right cell
    !--------------------------------------------------
    subroutine interp_inner(cell_L,cell_N,cell_R)
    type(cell_center),intent(in) :: cell_L,cell_R
    type(cell_center),intent(inout) :: cell_N
    real(kind=RKD) :: swL(3),swR(3)

    swL = (cell_N%w-cell_L%w)/(0.5*cell_N%length+0.5*cell_L%length)
    swR = (cell_R%w-cell_N%w)/(0.5*cell_R%length+0.5*cell_N%length)
    cell_N%sw = (sign(UP,swR)+sign(UP,swL))*abs(swR)*abs(swL)/(abs(swR)+abs(swL)+SMV)
    end subroutine interp_inner
    end module solver

    !--------------------------------------------------
    !>input and output
    !--------------------------------------------------
    module io
    use global_data
    use tools
    implicit none
    contains
    !--------------------------------------------------
    !>main initialization subroutine
    !--------------------------------------------------
    subroutine init()
    real(kind=RKD) :: init_gas(3) !initial condition
    real(kind=RKD) :: alpha_ref,omega_ref !molecule model coefficient in referece state
    real(kind=RKD) :: kn !Knudsen number in reference state
    real(kind=RKD) :: Ma !Mach number in front of shock
    real(kind=RKD) :: xlength !length of computational domain
    real(kind=RKD) :: umin !smallest and largest discrete velocity (for newton-cotes)
    real(kind=RKD) :: xscale !cell size/mean free path

    !control
    cfl = 0.5 !CFL number
    max_time = 1 !output time interval
    method_output = original!NORMALIZE !normalize the variables by (V-V1)/(V2-V1), where V1,V2 are upstream and downstream values

    !gasz
    particle_mass=1.0d-3
    ck = 2 !internal degree of freedom
    gam = get_gamma(ck) !ratio of specific heat
    pr = 2.0/3.0 !Prandtl number
    omega = 0.72 !temperature dependence index in VHS model
    kn = 1.0 !Knudsen number in reference state
    alpha_ref = 1.0 !coefficient in HS model
    omega_ref = 0.5 !coefficient in HS model
    mu_ref = get_mu(kn,alpha_ref,omega_ref) !reference viscosity coefficient

    !geometry
    xlength = 50.0
    xscale = 1.0

    !Mach number
    Ma = 3.0

    call init_geometry(xlength,xscale) !initialize the geometry
    call init_flow_field(Ma) !set the initial condition
    call init_particle()
    end subroutine init

    !--------------------------------------------------
    !>initialize the geometry
    !>@param[inout] xlength :domain length
    !>@param[in]    xscale  :cell size/mean free path
    !--------------------------------------------------
    subroutine init_geometry(xlength,xscale)
    real(kind=RKD),intent(inout) :: xlength
    real(kind=RKD),intent(in) :: xscale
    real(kind=RKD) :: xnum !number of cells
    integer :: i

    !adjust values
    xnum = xlength/xscale
    xlength = xnum*xscale

    !cell index range
    ixmin = 1
    ixmax = xnum

    !allocation
    allocate(ctr(ixmin-2:ixmax+2)) !cell center (with ghost cell)
    allocate(vface(ixmin:ixmax+1)) !vertical and horizontal cell interface

    !cell length and area
    dx = xlength/(ixmax-ixmin+1)

    !cell center (with ghost cell)
    forall(i=ixmin-2:ixmax+2)
        ctr(i)%x = (i-0.5)*dx
        ctr(i)%length = dx
    end forall
    end subroutine init_geometry

    !--------------------------------------------------
    !>set the initial condition
    !>@param[in] Ma_L :Mach number in front of shock
    !--------------------------------------------------
    subroutine init_flow_field(Ma_L)
    real(kind=RKD),intent(in) :: Ma_L
    real(kind=RKD) :: Ma_R !Mach number after shock
    real(kind=RKD) :: ratio_T !T2/T1
    real(kind=RKD) :: prim_L(3), prim_R(3) !primary variables before and after shock
    real(kind=RKD) :: w_L(3), w_R(3) !conservative variables before and after shock
    integer :: i

    !upstream condition (before shock)
    prim_L(1) = 1.0 !density
    prim_L(2) = Ma_L*sqrt(gam/2.0) !velocity
    prim_L(3) = 1.0 !lambda=1/temperature

    !downstream condition (after shock)
    Ma_R = sqrt((Ma_L**2*(gam-1)+2)/(2*gam*Ma_L**2-(gam-1)))
    ratio_T = (1+(gam-1)/2*Ma_L**2)*(2*gam/(gam-1)*Ma_L**2-1)/(Ma_L**2*(2*gam/(gam-1)+(gam-1)/2))

    prim_R(1) = prim_L(1)*(gam+1)*Ma_L**2/((gam-1)*Ma_L**2+2)
    prim_R(2) = Ma_R*sqrt(gam/2.0)*sqrt(ratio_T)
    prim_R(3) = prim_L(3)/ratio_T

    !conservative variables and distribution function
    w_L = get_conserved(prim_L)
    w_R = get_conserved(prim_R)

    !initialize field (with ghost cell)
    no_particle=0

    do i=ixmin-2,(ixmin+ixmax)/2
        ctr(i)%w = w_L
        ctr(i)%wg= 0
        ctr(i)%prim=prim_L
        ctr(i)%tau = get_tau(prim_L)
        no_particle=no_particle+nint(w_L(1)*dx/particle_mass)
    enddo

    do i=(ixmin+ixmax)/2+1,ixmax+2
        ctr(i)%w = w_R
        ctr(i)%wg= 0
        ctr(i)%prim=prim_R
        ctr(i)%tau = get_tau(prim_R)
        no_particle=no_particle+nint(w_R(1)*dx/particle_mass)

        !ctr(i)%w = w_L
        !ctr(i)%wg= 0
        !ctr(i)%prim=prim_L
        !ctr(i)%tau = get_tau(prim_L)
        !no_particle=no_particle+nint(w_L(1)*dx/particle_mass)
    enddo

    write(*,*) "particle number: ", no_particle

    write(*,*) "tau: ", ctr(1)%tau, ctr(50)%tau


    end subroutine init_flow_field

    !--------------------------------------------------
    !>set the initial condition
    !>@param[in] Ma_L :Mach number in front of shock
    !--------------------------------------------------
    subroutine init_particle()
    integer :: no_particle_cell
    real(kind=RKD) :: randv(2,3),randx
    integer :: i,j

    allocate(particle(2*no_particle))
    allocate(particle_temp(2*no_particle))

    no_particle_temp=0
    do i=ixmin-2,ixmax+2
        no_particle_cell=nint(ctr(i)%w(1)*dx/particle_mass)
        do j=1,no_particle_cell
            no_particle_temp=no_particle_temp+1
            call RANDOM_NUMBER(randv)
            call RANDOM_NUMBER(randx)
            particle(no_particle_temp)%mass=particle_mass
            particle(no_particle_temp)%v(:)=sqrt(-log(randv(1,:))/ctr(i)%prim(3))*sin(2.0d0*pi*randv(2,:))
            particle(no_particle_temp)%v(1)=particle(no_particle_temp)%v(1)+ctr(i)%prim(2)
            particle(no_particle_temp)%x=ctr(i)%x+(randx-0.5)*dx
            particle(no_particle_temp)%cell_no=i
            if(particle(no_particle_temp)%v(1)<0)then
                particle(no_particle_temp)%time_b=(ctr(i)%x-0.5*dx-particle(no_particle_temp)%x)/particle(no_particle_temp)%v(1)
            elseif(particle(no_particle_temp)%v(1)>0)then
                particle(no_particle_temp)%time_b=(ctr(i)%x+0.5*dx-particle(no_particle_temp)%x)/particle(no_particle_temp)%v(1)
            else
                particle(no_particle_temp)%time_b=1
            endif
        enddo
    enddo

    no_particle=no_particle_temp
    end subroutine init_particle

    !--------------------------------------------------
    !>write result
    !--------------------------------------------------
    subroutine output()
    real(kind=RKD) :: rho_avg !average density
    real(kind=RKD) :: xmid !location of average density
    real(kind=RKD) :: prim(3)
    integer :: i

    !--------------------------------------------------
    !preparation
    !--------------------------------------------------
    !solution (with ghost cell)
    do i=ixmin-1,ixmax+1
        prim=get_primary(ctr(i)%w)
        ctr(i)%primary(1) = prim(1) !density
        ctr(i)%primary(2) = prim(2) !temperature
        ctr(i)%primary(3) = 1.0/prim(3) !temperature
    end do

    !find middle location - the location of average density
    rho_avg = 0.5*(ctr(ixmin-1)%w(1)+ctr(ixmax+1)%w(1))

    do i=ixmin,ixmax
        if ((ctr(i)%w(1)-rho_avg)*(ctr(i+1)%w(1)-rho_avg)<=0) then
            xmid = ctr(i)%x+(ctr(i+1)%x-ctr(i)%x)/(ctr(i+1)%w(1)-ctr(i)%w(1))*(rho_avg-ctr(i)%w(1))
        end if
    end do

    !normalization
    !if (method_output==NORMALIZE) then
    !    ctr%primary(1) = (ctr%primary(1)-ctr(ixmin-1)%primary(1))/(ctr(ixmax+1)%primary(1)-ctr(ixmin-1)%primary(1))
    !    ctr%primary(2) = (ctr%primary(2)-ctr(ixmax+1)%primary(2))/(ctr(ixmin-1)%primary(2)-ctr(ixmax+1)%primary(2))
    !    ctr%primary(3) = (ctr%primary(3)-ctr(ixmin-1)%primary(3))/(ctr(ixmax+1)%primary(3)-ctr(ixmin-1)%primary(3))
    !end if

    !--------------------------------------------------
    !write to file
    !--------------------------------------------------
    !open result file and write header
    open(unit=RSTFILE,file=RSTFILENAME,status="replace",action="write")
    write(RSTFILE,*) "VARIABLES = X, RHO, U, T"
    do i=ixmin-1,ixmax+1
        write(RSTFILE,"(2(ES23.16,2X))") ctr(i)%x,ctr(i)%primary(1),ctr(i)%primary(2),ctr(i)%primary(3)
    enddo


    !close file
    close(RSTFILE)
    end subroutine output
    end module io

    !--------------------------------------------------
    !>main program
    !--------------------------------------------------
    program main
    use global_data
    use tools
    use solver
    use flux
    use io
    implicit none

    !initialization
    call init()
    call output()
    call timestep() !calculate time step

    write(*,*) "dt: ", dt

    !set initial value
    iter = 1 !number of iteration
    sim_time = 0.0 !simulation time

    !open file and write header
    open(unit=HSTFILE,file=HSTFILENAME,status="replace",action="write") !open history file
    write(HSTFILE,*) "VARIABLES = iter, sim_time, dt" !write header

    !iteration
    !do while(.true.)
    do iter=1,50
        !call interpolation() !calculate the slope of distribution function
        !call evolution() !calculate flux across the interfaces
        call update_f() !update cell averaged value
        !call update_g() !update cell averaged value

        !check if output
        if (sim_time>=max_time) exit

        !write iteration situation every 10 iterations
        if (mod(iter,100)==0) then
            write(*,"(A18,I15,2E15.7)") "iter,sim_time,dt:",iter,sim_time,dt
            write(HSTFILE,"(I15,2E15.7)") iter,sim_time,dt
        end if

        !iter = iter+1
        !sim_time = sim_time+dt
    end do

    write(*,*) ctr(50)%w, ctr(50)%wg

    !close history file
    close(HSTFILE)

    !output solution
    call output()
    end program main

    ! vim: set ft=fortran tw=0:
