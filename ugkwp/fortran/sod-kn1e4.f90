    !--------------------------------------------------
    !>store the global variables
    !--------------------------------------------------
    module global_data
    !--------------------------------------------------
    !kind selection
    !--------------------------------------------------
    integer,parameter :: RKD = 8

    !--------------------------------------------------
    !variables to control the simulation
    !--------------------------------------------------
    real(kind=RKD),parameter :: PI = 4.0*atan(1.0) !Pi
    real(kind=RKD),parameter :: UP = 1.0 !used in sign() function
    real(kind=RKD),parameter :: SMV = tiny(real(1.0,8)) !small value to avoid 0/0
    real(kind=RKD) :: cfl !global CFL number
    real(kind=RKD) :: dt !global time step
    real(kind=RKD) :: sim_time !current simulation time
    real(kind=RKD) :: max_time !maximum simulation time
    character(len=7),parameter :: HSTFILENAME = "sod.hst" !history file name
    character(len=7),parameter :: RSTFILENAME1 = "wp.plt" !result file name
    character(len=7),parameter :: RSTFILENAME2 = "awp.plt" !result file name
    integer :: iter !iteration
    integer :: average_index
    integer :: method_output !output the solution with normalized value or not

    !--------------------------------------------------
    !gas properties
    !--------------------------------------------------
    real(kind=RKD) :: gam !ratio of specific heat
    real(kind=RKD) :: omega !temperature dependence index in HS/VHS/VSS model
    real(kind=RKD) :: pr !Prandtl number
    real(kind=RKD) :: mu_ref !viscosity coefficient in reference state
    integer :: ck !internal degree of freedom

    !--------------------------------------------------
    !particle
    !--------------------------------------------------
    real(kind=RKD) :: ref_mass
    integer :: minimum_particle_number
    integer :: no_particle
    integer :: no_particle_noflux
    integer :: no_particle_wave

    !--------------------------------------------------
    !macros for a readable code
    !--------------------------------------------------
    !I/O
    integer,parameter :: HSTFILE = 20 !history file ID
    integer,parameter :: RSTFILE = 21 !result file ID
    !particle
    integer,parameter :: outout     = 0
    integer,parameter :: outinner   = 1
    integer,parameter :: innerinner = 2
    integer,parameter :: innerout   = 3

    !--------------------------------------------------
    !basic derived type
    !--------------------------------------------------
    !particle_type
    type :: particle_type
        real(kind=RKD) :: mass
        real(kind=RKD) :: x
        real(kind=RKD) :: v
        real(kind=RKD) :: Eyz ! y,z-direction thermal energy
        real(kind=RKD) :: tc ! time when first collision happens
        integer :: cellno,cellno_new
        integer :: flag  !flag=0 out->out; flag=1 out->in; flag=2 in->in; flag=3 in->out
    endtype particle_type

    !cell center
    type :: cell_center
        !geometry
        real(kind=RKD) :: x !cell center coordinates
        real(kind=RKD) :: length !length
        !flow field
        real(kind=RKD) :: w(3) !density, x-momentum,total energy
        real(kind=RKD) :: w_wave(3) ! wave to wave
        real(kind=RKD) :: w_particle(3) !sample particle
        real(kind=RKD) :: sw(3),sw_wave(3) !slope
        real(kind=RKD) :: prim(3)
        real(kind=RKD) :: tau
        real(kind=RKD) :: primary(4,10000)
        !net particle flux
        real(kind=RKD) :: flux_particle(3),flux_particle_wave(3) !net particle flux
        integer :: no_particle
    end type cell_center

    !cell interface
    type :: cell_interface
        real(kind=RKD) :: flux(3) !mass flux, x momentum flux, energy flux
    end type cell_interface

    !--------------------------------------------------
    !flow field
    !--------------------------------------------------
    !index method
    !     ----------------
    !  (i)|      (i)     |(i+1)
    !     ----------------
    integer :: ixmin,ixmax !index range
    real(kind=RKD) :: dx
    type(particle_type),allocatable,dimension(:) :: particle,particle_wave,particle_noflux
    type(cell_center),allocatable,dimension(:) :: ctr !cell centers
    type(cell_interface),allocatable,dimension(:) :: vface !vertical interfaces
    end module global_data

    !--------------------------------------------------
    !>define some commonly used functions/subroutines
    !--------------------------------------------------
    module tools
    use global_data
    implicit none
    contains
    !--------------------------------------------------
    !>convert primary variables to conservative variables
    !>@param[in] prim          :primary variables
    !>@return    get_conserved :conservative variables
    !--------------------------------------------------
    function get_conserved(prim)
    real(kind=RKD),intent(in) :: prim(3)
    real(kind=RKD) :: get_conserved(3)

    get_conserved(1) = prim(1)
    get_conserved(2) = prim(1)*prim(2)
    get_conserved(3) = 0.5*prim(1)/prim(3)/(gam-1.0)+0.5*prim(1)*prim(2)**2
    end function get_conserved

    !--------------------------------------------------
    !>convert conservative variables to primary variables
    !>@param[in] w           :conservative variables
    !>@return    get_primary :conservative variables
    !--------------------------------------------------
    function get_primary(w)
    real(kind=RKD),intent(in) :: w(3)
    real(kind=RKD) :: get_primary(3) !primary variables

    get_primary(1) = w(1)
    get_primary(2) = w(2)/w(1)
    get_primary(3) = 0.5*w(1)/(gam-1.0)/(w(3)-0.5*w(2)**2/w(1))
    end function get_primary

    !--------------------------------------------------
    !>obtain ratio of specific heat
    !>@param[in] ck        :internal degree of freedom
    !>@return    get_gamma :ratio of specific heat
    !--------------------------------------------------
    function get_gamma(ck)
    integer,intent(in) :: ck
    real(kind=RKD) :: get_gamma

    get_gamma = float(ck+3)/float(ck+1)
    end function get_gamma

    !--------------------------------------------------
    !>obtain speed of sound
    !>@param[in] prim    :primary variables
    !>@return    get_sos :speed of sound
    !--------------------------------------------------
    function get_sos(prim)
    real(kind=RKD),intent(in) :: prim(3)
    real(kind=RKD) :: get_sos !speed of sound

    get_sos = sqrt(0.5*gam/prim(3))
    end function get_sos

    !--------------------------------------------------
    !>calculate collision time
    !>@param[in] prim    :primary variables
    !>@return    get_tau :collision time
    !--------------------------------------------------
    function get_tau(prim)
    real(kind=RKD),intent(in) :: prim(3)
    real(kind=RKD) :: get_tau

    get_tau = mu_ref*2*prim(3)**(1-omega)/prim(1)
    end function get_tau

    !--------------------------------------------------
    !>get the nondimensionalized viscosity coefficient
    !>@param[in] kn          :Knudsen number
    !>@param[in] alpha,omega :index related to HS/VHS/VSS model
    !>@return    get_mu      :nondimensionalized viscosity coefficient
    !--------------------------------------------------
    function get_mu(kn,alpha,omega)
    real(kind=RKD),intent(in) :: kn,alpha,omega
    real(kind=RKD) :: get_mu

    get_mu = 5*(alpha+1)*(alpha+2)*sqrt(PI)/(4*alpha*(5-2*omega)*(7-2*omega))*kn
    end function get_mu
    end module tools

    !--------------------------------------------------
    !>flux calculation
    !--------------------------------------------------
    module flux
    use global_data
    use tools
    implicit none

    integer,parameter :: MNUM = 6 !number of normal velocity moments
    integer,parameter :: MTUM = 4 !number of tangential velocity moments

    contains
    !--------------------------------------------------
    !>calculate flux of inner interface
    !>@param[in]    cell_L :cell left to the target interface
    !>@param[inout] face   :the target interface
    !>@param[in]    cell_R :cell right to the target interface
    !--------------------------------------------------
    subroutine calc_flux(cell_L,face,cell_R)
    type(cell_center),intent(in) :: cell_L,cell_R
    type(cell_interface),intent(inout) :: face
    real(kind=RKD) :: w(3),prim(3)
    real(kind=RKD) :: w_L(3),prim_L(3)
    real(kind=RKD) :: w_R(3),prim_R(3)
    real(kind=RKD) :: sw(3)
    real(kind=RKD) :: aL(3),aR(3),aT(3)
    real(kind=RKD) :: Mu(0:MNUM),Mu_L(0:MNUM),Mu_R(0:MNUM),Mxi(0:2)
    real(kind=RKD) :: Mau_0(3),Mau_L(3),Mau_R(3),Mau_T(3)
    real(kind=RKD) :: tau
    real(kind=RKD) :: Mt(5)
    integer :: i,j

    !--------------------------------------------------
    !obtain macroscopic variables
    !--------------------------------------------------
    !conservative variables at interface
    w_L=cell_L%w+0.5*cell_L%sw*cell_L%length
    prim_L = get_primary(w_L)
    call calc_moment_u(prim_L,Mu,Mxi,Mu_L,Mu_R)
    w=prim_L(1)*moment_uv(Mu_L,Mxi,0,0)
    w_R=cell_R%w-0.5*cell_R%sw*cell_R%length
    prim_R = get_primary(w_R)
    call calc_moment_u(prim_R,Mu,Mxi,Mu_L,Mu_R)
    w=w+prim_R(1)*moment_uv(Mu_R,Mxi,0,0)
    if(prim_L(3)<0 .or. prim_R(3)<0)then
        prim_L = get_primary(cell_L%w)
        call calc_moment_u(prim_L,Mu,Mxi,Mu_L,Mu_R)
        w=prim_L(1)*moment_uv(Mu_L,Mxi,0,0)
        prim_R = get_primary(cell_R%w)
        call calc_moment_u(prim_R,Mu,Mxi,Mu_L,Mu_R)
        w=w+prim_R(1)*moment_uv(Mu_R,Mxi,0,0)
        !convert to primary variables
        prim = get_primary(w)

        !--------------------------------------------------
        !calculate collision time and some time integration terms
        !--------------------------------------------------
        tau = get_tau(prim)

        Mt(4) = tau*(1.0-exp(-dt/tau))
        Mt(5) = -tau*dt*exp(-dt/tau)+tau*Mt(4)
        Mt(1) = dt-Mt(4)
        Mt(2) = -tau*Mt(1)+Mt(5)
        Mt(3) = dt**2/2.0-tau*Mt(1)

        prim_L = get_primary(cell_L%w)
        call calc_moment_u(prim_L,Mu,Mxi,Mu_L,Mu_R)
        face%flux = Mt(1)*prim_L(1)*moment_uv(Mu_L,Mxi,1,0)
        prim_R = get_primary(cell_R%w)
        call calc_moment_u(prim_R,Mu,Mxi,Mu_L,Mu_R)
        face%flux = face%flux+Mt(1)*prim_R(1)*moment_uv(Mu_R,Mxi,1,0)
        do i=1,3
            if(isnan(face%flux(i)))then
                write(*,*) 'flux'
            endif
        enddo
    else

        !convert to primary variables
        prim = get_primary(w)

        !--------------------------------------------------
        !calculate a^L,a^R
        !--------------------------------------------------
        sw = (w-cell_L%w)/(0.5*cell_L%length) !left slope of W
        aL = micro_slope(prim,sw) !calculate a^L

        sw = (cell_R%w-w)/(0.5*cell_R%length) !right slope of W
        aR = micro_slope(prim,sw) !calculate a^R

        !--------------------------------------------------
        !calculate time slope of W and A
        !--------------------------------------------------
        !<u^n>,<\xi^l>,<u^n>_{>0},<u^n>_{<0}
        call calc_moment_u(prim,Mu,Mxi,Mu_L,Mu_R)

        Mau_L = moment_au(aL,Mu_L,Mxi,1) !<aL*u*\psi>_{>0}
        Mau_R = moment_au(aR,Mu_R,Mxi,1) !<aR*u*\psi>_{<0}

        sw = -prim(1)*(Mau_L+Mau_R) !time slope of W
        aT = micro_slope(prim,sw) !calculate A

        !--------------------------------------------------
        !calculate collision time and some time integration terms
        !--------------------------------------------------
        tau = get_tau(prim)!+30*dt*abs(prim_L(1)/prim_L(3)-prim_R(1)/prim_R(3))/(prim_L(1)/prim_L(3)+prim_R(1)/prim_R(3))

        Mt(4) = tau*(1.0-exp(-dt/tau))
        Mt(5) = -tau*dt*exp(-dt/tau)+tau*Mt(4)
        Mt(1) = dt-Mt(4)
        Mt(2) = -tau*Mt(1)+Mt(5)
        Mt(3) = dt**2/2.0-tau*Mt(1)

        !--------------------------------------------------
        !calculate the flux of conservative variables related to g0
        !--------------------------------------------------
        Mau_0 = moment_uv(Mu,Mxi,1,0) !<u*\psi>
        Mau_L = moment_au(aL,Mu_L,Mxi,2) !<aL*u^2*\psi>_{>0}
        Mau_R = moment_au(aR,Mu_R,Mxi,2) !<aR*u^2*\psi>_{<0}
        Mau_T = moment_au(aT,Mu,Mxi,1) !<A*u*\psi>

        face%flux = Mt(1)*prim(1)*Mau_0+Mt(2)*prim(1)*(Mau_L+Mau_R)+Mt(3)*prim(1)*Mau_T
    endif
    do i=1,3
        if(isnan(face%flux(i)))then
            write(*,*) 'flux'
        endif
    enddo
    !--------------------------------------------------
    !Flux_w_wave
    !--------------------------------------------------
    w_L=cell_L%w_wave+0.5*cell_L%sw_wave*cell_L%length
    prim_L = get_primary(w_L)
    if(prim_L(1)<1.0d-20 .or. prim_L(3)<0)then
        prim_L = get_primary(cell_L%w_wave)
        call calc_moment_u(prim_L,Mu,Mxi,Mu_L,Mu_R)
        Mau_0 = moment_uv(Mu_L,Mxi,1,0)
        face%flux= face%flux+Mt(4)*prim_L(1)*Mau_0
    else
        call calc_moment_u(prim_L,Mu,Mxi,Mu_L,Mu_R)
        sw = cell_L%sw_wave
        aL = micro_slope(prim,sw)
        Mau_0 = moment_uv(Mu_L,Mxi,1,0)
        Mau_L = moment_au(aL,Mu_L,Mxi,2)
        face%flux= face%flux+Mt(4)*prim_L(1)*Mau_0-Mt(5)*prim_L(1)*Mau_L
    endif

    do i=1,3
        if(isnan(face%flux(i)))then
            write(*,*) 'flux'
        endif
    enddo

    w_R=cell_R%w_wave-0.5*cell_R%sw_wave*cell_L%length
    prim_R = get_primary(w_R)
    if(prim_R(1)<1.0d-20 .or. prim_R(3)<0)then
        prim_R = get_primary(cell_R%w_wave)
        if(prim_R(3)<0)then
            prim_R(3)=1.0d8
        endif
        call calc_moment_u(prim_R,Mu,Mxi,Mu_L,Mu_R)
        Mau_0 = moment_uv(Mu_R,Mxi,1,0)
        face%flux= face%flux+Mt(4)*prim_R(1)*Mau_0
    else
        call calc_moment_u(prim_R,Mu,Mxi,Mu_L,Mu_R)
        sw = cell_R%sw_wave
        aR = micro_slope(prim,sw)
        Mau_0 = moment_uv(Mu_R,Mxi,1,0)
        Mau_R = moment_au(aR,Mu_R,Mxi,2)
        face%flux= face%flux+Mt(4)*prim_R(1)*Mau_0-Mt(5)*prim_R(1)*Mau_R
    endif
    do i=1,3
        if(isnan(face%flux(i)))then
            write(*,*) 'flux'
        endif
    enddo
    end subroutine calc_flux

    !--------------------------------------------------
    !>calculate micro slope of Maxwellian distribution
    !>@param[in] prim        :primary variables
    !>@param[in] sw          :slope of W
    !>@return    micro_slope :slope of Maxwellian distribution
    !--------------------------------------------------
    function micro_slope(prim,sw)
    real(kind=RKD),intent(in) :: prim(3),sw(3)
    real(kind=RKD) :: micro_slope(3)

    micro_slope(3) = 4.0*prim(3)**2/(ck+1)/prim(1)*(2.0*sw(3)-2.0*prim(2)*sw(2)+sw(1)*(prim(2)**2-0.5*(ck+1)/prim(3)))

    micro_slope(2) = 2.0*prim(3)/prim(1)*(sw(2)-prim(2)*sw(1))-prim(2)*micro_slope(3)
    micro_slope(1) = sw(1)/prim(1)-prim(2)*micro_slope(2)-0.5*(prim(2)**2+0.5*(ck+1)/prim(3))*micro_slope(3)
    end function micro_slope

    !--------------------------------------------------
    !>calculate moments of velocity
    !>@param[in] prim :primary variables
    !>@param[out] Mu        :<u^n>
    !>@param[out] Mxi       :<\xi^l>
    !>@param[out] Mu_L,Mu_R :<u^n>_{>0},<u^n>_{<0}
    !--------------------------------------------------
    subroutine calc_moment_u(prim,Mu,Mxi,Mu_L,Mu_R)
    real(kind=RKD),intent(in) :: prim(3)
    real(kind=RKD),intent(out) :: Mu(0:MNUM),Mu_L(0:MNUM),Mu_R(0:MNUM)
    real(kind=RKD),intent(out) :: Mxi(0:2)
    integer :: i

    !moments of normal velocity
    Mu_L(0) = 0.5*erfc(-sqrt(prim(3))*prim(2))
    Mu_L(1) = prim(2)*Mu_L(0)+0.5*exp(-prim(3)*prim(2)**2)/sqrt(PI*prim(3))
    Mu_R(0) = 0.5*erfc(sqrt(prim(3))*prim(2))
    Mu_R(1) = prim(2)*Mu_R(0)-0.5*exp(-prim(3)*prim(2)**2)/sqrt(PI*prim(3))

    do i=2,MNUM
        Mu_L(i) = prim(2)*Mu_L(i-1)+0.5*(i-1)*Mu_L(i-2)/prim(3)
        Mu_R(i) = prim(2)*Mu_R(i-1)+0.5*(i-1)*Mu_R(i-2)/prim(3)
    end do

    Mu = Mu_L+Mu_R

    !moments of \xi
    Mxi(0) = 1.0 !<\xi^0>
    Mxi(1) = 0.5*ck/prim(3) !<\xi^2>
    Mxi(2) = (ck**2+2.0*ck)/(4.0*prim(3)**2) !<\xi^4>
    end subroutine calc_moment_u

    !--------------------------------------------------
    !>calculate <u^\alpha*\xi^\delta*\psi>
    !>@param[in] Mu        :<u^\alpha>
    !>@param[in] Mxi       :<\xi^l>
    !>@param[in] alpha     :exponential index of u
    !>@param[in] delta     :exponential index of \xi
    !>@return    moment_uv :moment of <u^\alpha*\xi^\delta*\psi>
    !--------------------------------------------------
    function moment_uv(Mu,Mxi,alpha,delta)
    real(kind=RKD),intent(in) :: Mu(0:MNUM),Mxi(0:2)
    integer,intent(in) :: alpha,delta
    real(kind=RKD) :: moment_uv(3)

    moment_uv(1) = Mu(alpha)*Mxi(delta/2)
    moment_uv(2) = Mu(alpha+1)*Mxi(delta/2)
    moment_uv(3) = 0.5*(Mu(alpha+2)*Mxi(delta/2)+Mu(alpha)*Mxi((delta+2)/2))
    end function moment_uv

    !--------------------------------------------------
    !>calculate <a*u^\alpha*\psi>
    !>@param[in] a         :micro slope of Maxwellian
    !>@param[in] Mu        :<u^\alpha>
    !>@param[in] Mxi       :<\xi^l>
    !>@param[in] alpha     :exponential index of u
    !>@return    moment_au :moment of <a*u^\alpha*\psi>
    !--------------------------------------------------
    function moment_au(a,Mu,Mxi,alpha)
    real(kind=RKD),intent(in) :: a(3)
    real(kind=RKD),intent(in) :: Mu(0:MNUM),Mxi(0:2)
    integer,intent(in) :: alpha
    real(kind=RKD) :: moment_au(3)

    moment_au = a(1)*moment_uv(Mu,Mxi,alpha+0,0)+&
        a(2)*moment_uv(Mu,Mxi,alpha+1,0)+&
        0.5*a(3)*moment_uv(Mu,Mxi,alpha+2,0)+&
        0.5*a(3)*moment_uv(Mu,Mxi,alpha+0,2)
    end function moment_au
    end module flux

    !--------------------------------------------------
    !>UGKS solver
    !--------------------------------------------------
    module solver
    use global_data
    use tools
    use flux
    implicit none
    contains
    !--------------------------------------------------
    !>calculate time step
    !--------------------------------------------------
    subroutine timestep()
    real(kind=RKD) :: tmax !max 1/dt allowed
    real(kind=RKD) :: sos !speed of sound
    real(kind=RKD) :: prim(3) !primary variables
    integer :: i

    !set initial value
    tmax = 0.0

    !$omp parallel
    !$omp do private(i,sos,prim) reduction(max:tmax)
    do i=ixmin,ixmax
        !convert conservative variables to primary variables
        prim = get_primary(ctr(i)%w)

        !sound speed
        sos = get_sos(prim)

        !maximum velocity
        prim(2) = abs(prim(2))+3*sos

        !maximum 1/dt allowed
        tmax = max(tmax,prim(2)/ctr(i)%length)
    end do
    !$omp end do
    !$omp end parallel

    !time step
    dt = cfl/tmax
    end subroutine timestep

    !--------------------------------------------------
    !>calculate the slope of distribution function
    !--------------------------------------------------
    subroutine interpolation()
    integer :: i

    !$omp parallel
    !$omp do
    do i=ixmin-1,ixmax+1
        call interp_inner(ctr(i-1),ctr(i),ctr(i+1))
    end do
    !$omp end do nowait
    !$omp end parallel
    end subroutine interpolation

    !--------------------------------------------------
    !>calculate the flux across the interfaces
    !--------------------------------------------------
    subroutine evolution()
    integer :: i

    !$omp parallel
    !$omp do
    do i=ixmin,ixmax+1 !with ghost cell
        call calc_flux(ctr(i-1),vface(i),ctr(i))
    end do
    !$omp end do nowait
    !$omp end parallel
    end subroutine evolution

    !--------------------------------------------------
    !>stream particles
    !--------------------------------------------------
    subroutine stream()
    integer :: i

    !$omp parallel
    !$omp do
    do i=1,no_particle_noflux
        particle_noflux(i)%x=particle_noflux(i)%x+particle_noflux(i)%v*particle_noflux(i)%tc
        particle_noflux(i)%cellno_new=ceiling(particle_noflux(i)%x/dx)
        if(particle_noflux(i)%cellno.ge.ixmin .and. particle_noflux(i)%cellno.le.ixmax)then
            if(particle_noflux(i)%cellno_new.ge.ixmin .and. particle_noflux(i)%cellno_new.le.ixmax)then
                particle_noflux(i)%flag=2
            else
                particle_noflux(i)%flag=3
            endif
        else
            if(particle_noflux(i)%cellno_new.ge.ixmin .and. particle_noflux(i)%cellno_new.le.ixmax)then
                particle_noflux(i)%flag=1
            else
                particle_noflux(i)%flag=0
            endif
        endif
    enddo
    !$omp end do nowait

    !$omp do
    do i=1,no_particle
        particle(i)%x=particle(i)%x+particle(i)%v*particle(i)%tc
        particle(i)%cellno_new=ceiling(particle(i)%x/dx)
        if(particle(i)%cellno.ge.ixmin .and. particle(i)%cellno.le.ixmax)then
            if(particle(i)%cellno_new.ge.ixmin .and. particle(i)%cellno_new.le.ixmax)then
                particle(i)%flag=2
            else
                particle(i)%flag=3
            endif
        else
            if(particle(i)%cellno_new.ge.ixmin .and. particle(i)%cellno_new.le.ixmax)then
                particle(i)%flag=1
            else
                particle(i)%flag=0
            endif
        endif
    enddo
    !$omp end do nowait

    !$omp do
    do i=1,no_particle_wave
        particle_wave(i)%x=particle_wave(i)%x+particle_wave(i)%v*particle_wave(i)%tc
        particle_wave(i)%cellno_new=ceiling(particle_wave(i)%x/dx)
        if(particle_wave(i)%cellno.ge.ixmin .and. particle_wave(i)%cellno.le.ixmax)then
            if(particle_wave(i)%cellno_new.ge.ixmin .and. particle_wave(i)%cellno_new.le.ixmax)then
                particle_wave(i)%flag=2
            else
                particle_wave(i)%flag=3
            endif
        else
            if(particle_wave(i)%cellno_new.ge.ixmin .and. particle_wave(i)%cellno_new.le.ixmax)then
                particle_wave(i)%flag=1
            else
                particle_wave(i)%flag=0
            endif
        endif
    enddo
    !$omp end do nowait
    !$omp end parallel

    !first kind particle flux
    do i=1,no_particle
        if(particle(i)%flag==2)then
            if(particle(i)%cellno_new .ne. particle(i)%cellno)then
                ctr(particle(i)%cellno)%flux_particle(1)=ctr(particle(i)%cellno)%flux_particle(1)-particle(i)%mass
                ctr(particle(i)%cellno)%flux_particle(2)=ctr(particle(i)%cellno)%flux_particle(2)-particle(i)%mass*particle(i)%v
                ctr(particle(i)%cellno)%flux_particle(3)=ctr(particle(i)%cellno)%flux_particle(3)-particle(i)%mass*(0.5*particle(i)%v**2+particle(i)%Eyz)
                ctr(particle(i)%cellno_new)%flux_particle(1)=ctr(particle(i)%cellno_new)%flux_particle(1)+particle(i)%mass
                ctr(particle(i)%cellno_new)%flux_particle(2)=ctr(particle(i)%cellno_new)%flux_particle(2)+particle(i)%mass*particle(i)%v
                ctr(particle(i)%cellno_new)%flux_particle(3)=ctr(particle(i)%cellno_new)%flux_particle(3)+particle(i)%mass*(0.5*particle(i)%v**2+particle(i)%Eyz)
            endif
        elseif(particle(i)%flag==1)then
            ctr(particle(i)%cellno_new)%flux_particle(1)=ctr(particle(i)%cellno_new)%flux_particle(1)+particle(i)%mass
            ctr(particle(i)%cellno_new)%flux_particle(2)=ctr(particle(i)%cellno_new)%flux_particle(2)+particle(i)%mass*particle(i)%v
            ctr(particle(i)%cellno_new)%flux_particle(3)=ctr(particle(i)%cellno_new)%flux_particle(3)+particle(i)%mass*(0.5*particle(i)%v**2+particle(i)%Eyz)
        elseif(particle(i)%flag==3)then
            ctr(particle(i)%cellno)%flux_particle(1)=ctr(particle(i)%cellno)%flux_particle(1)-particle(i)%mass
            ctr(particle(i)%cellno)%flux_particle(2)=ctr(particle(i)%cellno)%flux_particle(2)-particle(i)%mass*particle(i)%v
            ctr(particle(i)%cellno)%flux_particle(3)=ctr(particle(i)%cellno)%flux_particle(3)-particle(i)%mass*(0.5*particle(i)%v**2+particle(i)%Eyz)
        endif
    enddo

    !2,3 kind particle flux
    do i=1,no_particle_wave
        if(particle_wave(i)%flag==2)then
            if(particle_wave(i)%cellno_new .ne. particle_wave(i)%cellno)then
                ctr(particle_wave(i)%cellno)%flux_particle_wave(1)=ctr(particle_wave(i)%cellno)%flux_particle_wave(1)-particle_wave(i)%mass
                ctr(particle_wave(i)%cellno)%flux_particle_wave(2)=ctr(particle_wave(i)%cellno)%flux_particle_wave(2)-particle_wave(i)%mass*particle_wave(i)%v
                ctr(particle_wave(i)%cellno)%flux_particle_wave(3)=ctr(particle_wave(i)%cellno)%flux_particle_wave(3)-particle_wave(i)%mass*(0.5*particle_wave(i)%v**2+particle_wave(i)%Eyz)
                ctr(particle_wave(i)%cellno_new)%flux_particle_wave(1)=ctr(particle_wave(i)%cellno_new)%flux_particle_wave(1)+particle_wave(i)%mass
                ctr(particle_wave(i)%cellno_new)%flux_particle_wave(2)=ctr(particle_wave(i)%cellno_new)%flux_particle_wave(2)+particle_wave(i)%mass*particle_wave(i)%v
                ctr(particle_wave(i)%cellno_new)%flux_particle_wave(3)=ctr(particle_wave(i)%cellno_new)%flux_particle_wave(3)+particle_wave(i)%mass*(0.5*particle_wave(i)%v**2+particle_wave(i)%Eyz)
            endif
        elseif(particle_wave(i)%flag==1)then
            ctr(particle_wave(i)%cellno_new)%flux_particle_wave(1)=ctr(particle_wave(i)%cellno_new)%flux_particle_wave(1)+particle_wave(i)%mass
            ctr(particle_wave(i)%cellno_new)%flux_particle_wave(2)=ctr(particle_wave(i)%cellno_new)%flux_particle_wave(2)+particle_wave(i)%mass*particle_wave(i)%v
            ctr(particle_wave(i)%cellno_new)%flux_particle_wave(3)=ctr(particle_wave(i)%cellno_new)%flux_particle_wave(3)+particle_wave(i)%mass*(0.5*particle_wave(i)%v**2+particle_wave(i)%Eyz)
        elseif(particle_wave(i)%flag==3)then
            ctr(particle_wave(i)%cellno)%flux_particle_wave(1)=ctr(particle_wave(i)%cellno)%flux_particle_wave(1)-particle_wave(i)%mass
            ctr(particle_wave(i)%cellno)%flux_particle_wave(2)=ctr(particle_wave(i)%cellno)%flux_particle_wave(2)-particle_wave(i)%mass*particle_wave(i)%v
            ctr(particle_wave(i)%cellno)%flux_particle_wave(3)=ctr(particle_wave(i)%cellno)%flux_particle_wave(3)-particle_wave(i)%mass*(0.5*particle_wave(i)%v**2+particle_wave(i)%Eyz)
        endif
    enddo
    end subroutine stream
    !--------------------------------------------------
    !>update cell averaged values
    !--------------------------------------------------
    subroutine update()
    real(kind=RKD) :: randx,randv(2)
    real(kind=RKD) :: randt,tc
    real(kind=RKD) :: prim(3)
    integer :: particle_loop
    integer :: i,j

    !$omp parallel
    !$omp do
    do i=ixmin,ixmax
        ctr(i)%w = ctr(i)%w+(ctr(i)%flux_particle+ctr(i)%flux_particle_wave+vface(i)%flux-vface(i+1)%flux)/ctr(i)%length
        ctr(i)%prim=get_primary(ctr(i)%w)
        if(ctr(i)%prim(1)<0) then
            ctr(i)%prim(1)=1.0d-8
            ctr(i)%w = get_conserved(ctr(i)%prim)
            !stop
        endif
        if(ctr(i)%prim(3)<0)then
            ctr(i)%prim(3)=1.0d8
            ctr(i)%w = get_conserved(ctr(i)%prim)
            !stop
        endif
        ctr(i)%tau=get_tau(ctr(i)%prim)
        ctr(i)%w_wave=ctr(i)%w
        !reset flux
        ctr(i)%flux_particle=0
        ctr(i)%flux_particle_wave=0
        ctr(i)%no_particle=0
    end do
    !$omp end do nowait
    !$omp end parallel

    particle_loop=no_particle
    no_particle=0
    no_particle_wave=0

    do i=1,particle_loop
        if(particle(i)%flag==1 .or. particle(i)%flag==2)then
            ctr(particle(i)%cellno_new)%no_particle=ctr(particle(i)%cellno_new)%no_particle+1
            ctr(particle(i)%cellno_new)%w_wave(1) = ctr(particle(i)%cellno_new)%w_wave(1)-particle(i)%mass/ctr(particle(i)%cellno_new)%length
            ctr(particle(i)%cellno_new)%w_wave(2) = ctr(particle(i)%cellno_new)%w_wave(2)-particle(i)%mass*particle(i)%v/ctr(particle(i)%cellno_new)%length
            ctr(particle(i)%cellno_new)%w_wave(3) = ctr(particle(i)%cellno_new)%w_wave(3)-particle(i)%mass*(0.5*particle(i)%v**2+particle(i)%Eyz)/ctr(particle(i)%cellno_new)%length
            call RANDOM_NUMBER(randt)
            tc=-ctr(particle(i)%cellno_new)%tau*log(randt)
            if(tc.ge.dt)then
                no_particle=no_particle+1
                particle(no_particle)%mass=particle(i)%mass
                particle(no_particle)%x=particle(i)%x
                particle(no_particle)%v=particle(i)%v
                particle(no_particle)%Eyz=particle(i)%Eyz
                particle(no_particle)%tc=dt
                particle(no_particle)%cellno=particle(i)%cellno_new
                particle(no_particle)%flag=2
            else
                no_particle_wave=no_particle_wave+1
                particle_wave(no_particle_wave)%mass=particle(i)%mass
                particle_wave(no_particle_wave)%x=particle(i)%x
                particle_wave(no_particle_wave)%v=particle(i)%v
                particle_wave(no_particle_wave)%Eyz=particle(i)%Eyz
                particle_wave(no_particle_wave)%tc=tc
                particle_wave(no_particle_wave)%cellno=particle(i)%cellno_new
                particle_wave(no_particle_wave)%flag=2
            endif
        endif
    enddo

    do i=1,no_particle_noflux
        if(particle_noflux(i)%flag==1 .or. particle_noflux(i)%flag==2)then
            ctr(particle_noflux(i)%cellno_new)%no_particle=ctr(particle_noflux(i)%cellno_new)%no_particle+1
            ctr(particle_noflux(i)%cellno_new)%w_wave(1) = ctr(particle_noflux(i)%cellno_new)%w_wave(1)-particle_noflux(i)%mass/dx
            ctr(particle_noflux(i)%cellno_new)%w_wave(2) = ctr(particle_noflux(i)%cellno_new)%w_wave(2)-particle_noflux(i)%mass*particle_noflux(i)%v/dx
            ctr(particle_noflux(i)%cellno_new)%w_wave(3) = ctr(particle_noflux(i)%cellno_new)%w_wave(3)-particle_noflux(i)%mass*(0.5*particle_noflux(i)%v**2+particle_noflux(i)%Eyz)/dx
            call RANDOM_NUMBER(randt)
            tc=-ctr(particle_noflux(i)%cellno_new)%tau*log(randt)
            if(tc.ge.dt)then
                no_particle=no_particle+1
                particle(no_particle)%mass=particle_noflux(i)%mass
                particle(no_particle)%x=particle_noflux(i)%x
                particle(no_particle)%v=particle_noflux(i)%v
                particle(no_particle)%Eyz=particle_noflux(i)%Eyz
                particle(no_particle)%tc=dt
                particle(no_particle)%cellno=particle_noflux(i)%cellno_new
                particle(no_particle)%flag=2
            else
                no_particle_wave=no_particle_wave+1
                particle_wave(no_particle_wave)%mass=particle_noflux(i)%mass
                particle_wave(no_particle_wave)%x=particle_noflux(i)%x
                particle_wave(no_particle_wave)%v=particle_noflux(i)%v
                particle_wave(no_particle_wave)%Eyz=particle_noflux(i)%Eyz
                particle_wave(no_particle_wave)%tc=tc
                particle_wave(no_particle_wave)%cellno=particle_noflux(i)%cellno_new
                particle_wave(no_particle_wave)%flag=2
            endif
        endif
    enddo
    
    no_particle_noflux=0
    !sample first kind particle
    do i=ixmin,ixmax
        prim(1)=max(ctr(i)%w_wave(1),1.0d-20)
        prim(2:3)=ctr(i)%prim(2:3)
        ctr(i)%w_wave=get_conserved(prim)
        ctr(i)%w_particle=ctr(i)%w_wave*(exp(-dt/ctr(i)%tau))
        if(ctr(i)%w_particle(1)>1.0d-8)then
            particle_loop=max(int(ctr(i)%w_particle(1)*ctr(i)%length/ref_mass),(minimum_particle_number-ctr(i)%no_particle))
            do j=1,particle_loop
                no_particle_noflux=no_particle_noflux+1
                particle_noflux(no_particle_noflux)%mass=ctr(i)%w_particle(1)*ctr(i)%length/particle_loop
                call RANDOM_NUMBER(randx)
                call RANDOM_NUMBER(randv)
                particle_noflux(no_particle_noflux)%x=ctr(i)%x+(randx-0.5)*dx
                particle_noflux(no_particle_noflux)%v=sqrt(-log(randv(1))/ctr(i)%prim(3))*sin(2.0d0*pi*randv(2))+ctr(i)%prim(2)
                particle_noflux(no_particle_noflux)%Eyz=1.0/(2.0*ctr(i)%prim(3))
                particle_noflux(no_particle_noflux)%tc=dt
                particle_noflux(no_particle_noflux)%cellno=i
                particle_noflux(no_particle_noflux)%flag=2
            enddo
        endif
    enddo

    !boundary condition
    do i=1,2
        particle_loop=max(int(ctr(ixmin-i)%w_particle(1)*ctr(ixmin-i)%length/ref_mass),minimum_particle_number)
        do j=1,particle_loop
            no_particle_noflux=no_particle_noflux+1
            particle_noflux(no_particle_noflux)%mass=ctr(ixmin-i)%w_particle(1)*ctr(ixmin-i)%length/particle_loop
            call RANDOM_NUMBER(randx)
            call RANDOM_NUMBER(randv)
            particle_noflux(no_particle_noflux)%x=ctr(ixmin-i)%x+(randx-0.5)*dx
            particle_noflux(no_particle_noflux)%v=sqrt(-log(randv(1))/ctr(ixmin-i)%prim(3))*sin(2.0d0*pi*randv(2))+ctr(ixmin-i)%prim(2)
            particle_noflux(no_particle_noflux)%Eyz=1.0/(2.0*ctr(ixmin-i)%prim(3))
            particle_noflux(no_particle_noflux)%tc=dt
            particle_noflux(no_particle_noflux)%cellno=ixmin-i
            particle_noflux(no_particle_noflux)%flag=2
        enddo
        particle_loop=max(int(ctr(ixmax+i)%w_particle(1)*ctr(ixmax+i)%length/ref_mass),minimum_particle_number)
        do j=1,particle_loop
            no_particle_noflux=no_particle_noflux+1
            particle_noflux(no_particle_noflux)%mass=ctr(ixmax+i)%w_particle(1)*ctr(ixmax+i)%length/particle_loop
            call RANDOM_NUMBER(randx)
            call RANDOM_NUMBER(randv)
            particle_noflux(no_particle_noflux)%x=ctr(ixmax+i)%x+(randx-0.5)*dx
            particle_noflux(no_particle_noflux)%v=sqrt(-log(randv(1))/ctr(ixmax+i)%prim(3))*sin(2.0d0*pi*randv(2))+ctr(ixmax+i)%prim(2)
            particle_noflux(no_particle_noflux)%Eyz=1.0/(2.0*ctr(ixmax+i)%prim(3))
            particle_noflux(no_particle_noflux)%tc=dt
            particle_noflux(no_particle_noflux)%cellno=ixmax+i
            particle_noflux(no_particle_noflux)%flag=2
        enddo
    enddo
    end subroutine update

    !--------------------------------------------------
    !>interpolation of the inner cells
    !>@param[in]    cell_L :the left cell
    !>@param[inout] cell_N :the target cell
    !>@param[in]    cell_R :the right cell
    !--------------------------------------------------
    subroutine interp_inner(cell_L,cell_N,cell_R)
    type(cell_center),intent(in) :: cell_L,cell_R
    type(cell_center),intent(inout) :: cell_N
    real(kind=RKD) :: swL(3),swR(3)

    swL = (cell_N%w-cell_L%w)/(0.5*cell_N%length+0.5*cell_L%length)
    swR = (cell_R%w-cell_N%w)/(0.5*cell_R%length+0.5*cell_N%length)
    cell_N%sw = (sign(UP,swR)+sign(UP,swL))*abs(swR)*abs(swL)/(abs(swR)+abs(swL)+SMV)

    swL = (cell_N%w_wave-cell_L%w_wave)/(0.5*cell_N%length+0.5*cell_L%length)
    swR = (cell_R%w_wave-cell_N%w_wave)/(0.5*cell_R%length+0.5*cell_N%length)
    cell_N%sw_wave = (sign(UP,swR)+sign(UP,swL))*abs(swR)*abs(swL)/(abs(swR)+abs(swL)+SMV)
    end subroutine interp_inner
    end module solver

    !--------------------------------------------------
    !>input and output
    !--------------------------------------------------
    module io
    use global_data
    use tools
    implicit none
    contains
    !--------------------------------------------------
    !>main initialization subroutine
    !--------------------------------------------------
    subroutine init()
    real(kind=RKD) :: init_gas(3) !initial condition
    real(kind=RKD) :: alpha_ref,omega_ref !molecule model coefficient in referece state
    real(kind=RKD) :: kn !Knudsen number in reference state
    real(kind=RKD) :: xlength !length of computational domain
    real(kind=RKD) :: xscale !cell size/mean free path

    !control
    cfl = 0.95 !CFL number
    max_time = 0.2 !output time interval

    !gas
    dt=1d-3
    ck = 2 !internal degree of freedom
    gam = get_gamma(ck) !ratio of specific heat
    omega = 0.72 !temperature dependence index in VHS model
    kn = 1.0d-4 !Knudsen number in reference state
    alpha_ref = 1.0 !coefficient in HS model
    omega_ref = 0.5 !coefficient in HS model
    mu_ref = get_mu(kn,alpha_ref,omega_ref) !reference viscosity coefficient

    !geometry
    xlength = 3.0
    xscale = 1.0d-2

    !particle
    minimum_particle_number=100
    ref_mass=1.0d-4

    call init_geometry(xlength,xscale) !initialize the geometry
    call init_flow_field() !set the initial condition
    end subroutine init

    !--------------------------------------------------
    !>initialize the geometry
    !>@param[inout] xlength :domain length
    !>@param[in]    xscale  :cell size/mean free path
    !--------------------------------------------------
    subroutine init_geometry(xlength,xscale)
    real(kind=RKD),intent(inout) :: xlength
    real(kind=RKD),intent(in) :: xscale
    real(kind=RKD) :: xnum !number of cells
    integer :: particle_number_total
    integer :: i

    !adjust values
    xnum = xlength/xscale
    xlength = xnum*xscale

    !cell index range
    ixmin = 1
    ixmax = xnum

    !allocation
    allocate(ctr(ixmin-2:ixmax+2)) !cell center (with ghost cell)
    allocate(vface(ixmin:ixmax+1)) !vertical and horizontal cell interface
    particle_number_total=max(20.0*minimum_particle_number*(ixmax-ixmin+1),20.0*1.0*1.0/ref_mass)
    allocate(particle(particle_number_total))
    allocate(particle_noflux(particle_number_total))
    allocate(particle_wave(particle_number_total))

    !cell length and area
    dx = xlength/(ixmax-ixmin+1)

    !cell center (with ghost cell)
    forall(i=ixmin-2:ixmax+2)
        ctr(i)%x = (i-0.5)*dx
        ctr(i)%length = dx
    end forall
    end subroutine init_geometry

    !--------------------------------------------------
    !>set the initial condition
    !>@param[in] Ma_L :Mach number in front of shock
    !--------------------------------------------------
    subroutine init_flow_field()
    real(kind=RKD) :: prim_L(3), prim_R(3) !primary variables before and after shock
    real(kind=RKD) :: w_L(3), w_R(3) !conservative variables before and after shock
    real(kind=RKD) :: randx,randv(2)
    integer :: particle_loop
    integer :: i,j

    !initial flow field
    prim_L(1) = 1.0
    prim_L(2) = 0.0
    prim_L(3) = 0.5
    prim_R(1) = 0.125
    prim_R(2) = 0
    prim_R(3) = 0.625
    w_L = get_conserved(prim_L)
    w_R = get_conserved(prim_R)

    !initialize field (with ghost cell)
    do i=ixmin-2,(ixmin+ixmax)/2
        ctr(i)%w = w_L
        ctr(i)%prim = prim_L
        ctr(i)%tau=get_tau(ctr(i)%prim)
        ctr(i)%w_wave=ctr(i)%w
        ctr(i)%w_particle=ctr(i)%w*exp(-dt/ctr(i)%tau)
    enddo
    do  i=(ixmin+ixmax)/2+1,ixmax+2
        ctr(i)%w = w_R
        ctr(i)%prim = prim_R
        ctr(i)%tau=get_tau(ctr(i)%prim)
        ctr(i)%w_wave=ctr(i)%w
        ctr(i)%w_particle=ctr(i)%w*exp(-dt/ctr(i)%tau)
    enddo

    no_particle=0
    no_particle_noflux=0
    no_particle_wave=0

    !sample first kind particle
    do i=ixmin-2,ixmax+2
        particle_loop=max(int(ctr(i)%w_particle(1)*ctr(i)%length/ref_mass),minimum_particle_number)
        do j=1,particle_loop
            no_particle_noflux=no_particle_noflux+1
            particle_noflux(no_particle_noflux)%mass=ctr(i)%w_particle(1)*ctr(i)%length/particle_loop
            call RANDOM_NUMBER(randx)
            call RANDOM_NUMBER(randv)
            particle_noflux(no_particle_noflux)%x=ctr(i)%x+(randx-0.5)*dx
            particle_noflux(no_particle_noflux)%v=sqrt(-log(randv(1))/ctr(i)%prim(3))*sin(2.0d0*pi*randv(2))+ctr(i)%prim(2)
            particle_noflux(no_particle_noflux)%Eyz=1.0/(2.0*ctr(i)%prim(3))
            particle_noflux(no_particle_noflux)%tc=dt
            particle_noflux(no_particle_noflux)%cellno=i
            particle_noflux(no_particle_noflux)%flag=2
        enddo
    enddo
    end subroutine init_flow_field

    !--------------------------------------------------
    !>write result
    !--------------------------------------------------
    subroutine output()
    integer :: i

    do i=ixmin,ixmax
        ctr(i)%primary(1,average_index)=ctr(i)%prim(1)
        ctr(i)%primary(2,average_index)=ctr(i)%prim(2)
        ctr(i)%primary(3,average_index)=1.0/ctr(i)%prim(3)
        ctr(i)%primary(4,average_index)=0.5*ctr(i)%prim(1)/ctr(i)%prim(3)
    enddo

    open(unit=RSTFILE,file=RSTFILENAME1,status="replace",action="write")
    write(RSTFILE,*) "VARIABLES = X, RHO, U, T, P"
    do i=ixmin,ixmax
        write(RSTFILE,"(5(ES23.16,2X))") ctr(i)%x,ctr(i)%prim(1),ctr(i)%prim(2),1.0/ctr(i)%prim(3),0.5*ctr(i)%prim(1)/ctr(i)%prim(3)
    enddo

    open(unit=RSTFILE,file=RSTFILENAME2,status="replace",action="write")
    write(RSTFILE,*) "VARIABLES = X, RHO, U, T, P"
    do i=ixmin,ixmax
        write(RSTFILE,"(5(ES23.16,2X))") ctr(i)%x,&
            sum(ctr(i)%primary(1,1:average_index))/average_index,&
            sum(ctr(i)%primary(2,1:average_index))/average_index,&
            sum(ctr(i)%primary(3,1:average_index))/average_index,&
            sum(ctr(i)%primary(4,1:average_index))/average_index
    enddo

    !close file
    close(RSTFILE)
    end subroutine output
    end module io

    !--------------------------------------------------
    !>main program
    !--------------------------------------------------
    program main
    use global_data
    use tools
    use solver
    use flux
    use io
    implicit none

    !open file and write header
    open(unit=HSTFILE,file=HSTFILENAME,status="replace",action="write") !open history file
    write(HSTFILE,*) "VARIABLES = iter, sim_time, dt" !write header
    call init()
    average_index=1
    
    do average_index=1,100
        !initialization
        CALL RANDOM_SEED
        call init_flow_field()

        !set initial value
        iter = 1 !number of iteration
        sim_time = 0.0 !simulation time

        !iteration
        do while(.true.)
            call stream()
            call interpolation() !calculate the slope of distribution function
            call evolution() !calculate flux across the interfaces
            call update() !update cell averaged value

            !check if output
            if (sim_time>=max_time) exit

            !write iteration situation every 10 iterations
            if (mod(iter,100)==0) then
                write(*,"(A18,I15,2E15.7)") "iter,sim_time,dt:",iter,sim_time,dt
                write(HSTFILE,"(I15,2E15.7)") iter,sim_time,dt
            end if

            iter = iter+1
            sim_time = sim_time+dt
        end do

        !output solution
        call output()
        write(*,*) "----------average_index=",average_index,"------------"
    enddo
    !close history file
    close(HSTFILE)
    end program main