# ---------------------------------
# A toy model for entropy closure
# ---------------------------------

using LinearAlgebra, Optim, DiffEqFlux, Flux

nquad = 16
nentry = 5

m = randn(nentry, nquad)
α = randn(nentry)
ω = ones(nquad) ./ nquad
u = rand(nentry)

function eta(f)
    exp(-f)
end

#--- classical optimizer --#
function loss(α)
    sum(eta.(α' * m) .* ω) - dot(α, u)
end

res = optimize(loss, α, Newton())
res.minimum

#--- NN optimizer --#
function loss2(p)
    loss = sum(eta.(permutedims(p) * m) .* ω) - dot(p, u)
    return loss
end
  
cb = function (p, l)
    display(l)
    return false
end
  
res2 = DiffEqFlux.sciml_train(loss2, α, ADAM(0.1), cb = cb, maxiters = 100)
res2.minimum