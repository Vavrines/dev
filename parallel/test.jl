using Distributed
addprocs(4)
@everywhere using DistributedArrays
@everywhere using KitBase

_cell = ControlVolume1D(1.0, 1e-2, 1., [1., 1., 1.])
ctr = dfill(_cell, 100, 100)

[@fetchfrom p localindices(ctr) for p in workers()]
procs(ctr)



pids = sort(vec(procs(ctr)))
mylp = ctr[:L]

sendto(pids[1], mylp[2])





for i in 1:100, j in 1:100
    ctr[i, j].x = 10.0 * i + j
end

for i in 1:100, j in 1:100
    @show ctr[i, j].x
end

DArray(size(ctr),procs(ctr)) do I
    top   = mod(first(I[1])-2,size(ctr,1))+1
end