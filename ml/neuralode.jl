using DiffEqFlux, Flux, OrdinaryDiffEq, Plots, Solaris
using DiffEqFlux.Optim, DiffEqFlux.GalacticOptim
using DiffEqFlux: sciml_train

function lotka_volterra!(du, u, p, t)
    α, β, γ, δ = p
    du[1] = α * u[1] - β * u[2] * u[1]
    du[2] = γ * u[1] * u[2] - δ * u[2]
end

# experimental parameters
tspan = (0.0f0, 3.0f0)
tsteps = tspan[1]:0.1f0:tspan[2]
u0 = Float32[0.44249296, 4.6280594]
p0 = Float32[1.3, 0.9, 0.8, 1.8]
prob0 = ODEProblem(lotka_volterra!, u0, tspan, p0)
sol0 = solve(prob0, Tsit5(), saveat = 0.1)
data0 = Array(sol0)

cb = function (p, l)
    display(l)
    return false
end

# neural ODE model
nn = FastChain(FastDense(2, 50, tanh), FastDense(50, 2))
θ0 = initial_params(nn)

function rhs_node(u, p, t)
    return nn(u, p)
end
prob_node = ODEProblem(rhs_node, u0, tspan, θ0)

function loss(p)
    pred = solve(prob_node, Midpoint(), u0 = u0, p = p, saveat = tsteps)
    loss = sum(abs2, data0 .- pred)
    return loss
end

res = sci_train(loss, θ0, ADAM(); callback = cb, maxiters = 500)
res = sci_train(loss, res.u, LBFGS(); callback = cb, maxiters = 200)
res = sci_train(loss, res.u, ADAM(0.01); callback = cb, maxiters = 500)

# uniersal ODE model
nn1 = FastChain(FastDense(2, 50, tanh), FastDense(50, 1))
θ1 = initial_params(nn1)

function rhs_uode(du, u, p, t)
    du[1] = 1.3 * u[1] - 0.9 * u[2] * u[1]
    du[2] = 0.8 * u[1] * u[2] - first(nn1(u, p))#1.8 * u[2]
end
prob_uode = ODEProblem(rhs_uode, u0, tspan, θ1)

function loss1(p)
    pred = solve(prob_uode, Midpoint(), u0 = u0, p = p, saveat = tsteps)
    loss = sum(abs2, data0 .- pred)
    return loss
end

res1 = sci_train(loss1, θ1, ADAM(0.01); callback = cb, maxiters = 500)
res1 = sci_train(loss1, res1.u, LBFGS(); callback = cb, maxiters = 200)
res1 = sci_train(loss1, res1.u, ADAM(); callback = cb, maxiters = 500)

# extrapolation
tspan1 = (0.0f0, 10.0f0)
prob1 = ODEProblem(lotka_volterra!, u0, tspan1, p0)
sol1 = solve(prob1, Tsit5(), saveat = 0.05) |> Array

prob_node1 = ODEProblem(rhs_node, u0, tspan1, res.u)
pred = solve(prob_node1, Midpoint(), u0 = u0, p = res.u, saveat = 0.05) |> Array

prob_uode1 = ODEProblem(rhs_uode, u0, tspan1, res1.u)
pred1 = solve(prob_uode1, Midpoint(), u0 = u0, p = res1.u, saveat = 0.05) |> Array

begin
    plot(tspan1[1]:0.05:tspan1[2], sol1[1, :], lw=1.5, label="u₁ (ref)", xlabel="t",  ylims=(-0.1,9))
    plot!(tspan1[1]:0.05:tspan1[2], sol1[2, :], lw=1.5, label="u₂ (ref)")
    plot!(tspan1[1]:0.05:tspan1[2], pred[1, :], lw=1.5, label="u₁ (NODE)", line=:dash)
    plot!(tspan1[1]:0.05:tspan1[2], pred[2, :], lw=1.5, label="u₁ (NODE)", line=:dash)
    scatter!(tspan1[1]:0.05:tspan1[2], pred1[1, :], label="u₁ (UODE)", line=:dash, alpha=0.6)
    scatter!(tspan1[1]:0.05:tspan1[2], pred1[2, :], label="u₁ (UODE)", line=:dash, alpha=0.6)
end

savefig("lotka_volterra.pdf")
