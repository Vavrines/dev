using Flux, Statistics
using Flux.Data: DataLoader
using Flux: onehotbatch, onecold, logitcrossentropy, throttle, @epochs
using Base.Iterators: repeated
using Parameters: @with_kw
using CUDA
using MLDatasets

include("/home/vavrines/Coding/KitML.jl/src/KitML.jl")
import .KitML

@with_kw mutable struct Args
    η::Float64 = 3e-4       # learning rate
    batchsize::Int = 1024   # batch size
    epochs::Int = 10        # number of epochs
    device::Function = cpu  # set as gpu, if gpu available
end

function getdata(args)
    # Loading Dataset	
    xtrain, ytrain = MLDatasets.MNIST.traindata(Float32)
    xtest, ytest = MLDatasets.MNIST.testdata(Float32)
	
    # Reshape Data for flatten the each image into linear array
    xtrain = Flux.flatten(xtrain)
    xtest = Flux.flatten(xtest)

    # One-hot-encode the labels
    ytrain, ytest = onehotbatch(ytrain, 0:9), onehotbatch(ytest, 0:9)

    # Batching
    train_data = DataLoader(xtrain, ytrain, batchsize=args.batchsize, shuffle=true)
    test_data = DataLoader(xtest, ytest, batchsize=args.batchsize)

    return train_data, test_data
end

function loss_all(dataloader, model)
    l = 0f0
    for (x,y) in dataloader
        l += logitcrossentropy(model(x), y)
    end
    l/length(dataloader)
end

function accuracy(data_loader, model)
    acc = 0
    for (x,y) in data_loader
        acc += sum(onecold(cpu(model(x))) .== onecold(cpu(y)))*1 / size(x,2)
    end
    acc/length(data_loader)
end

# train
args = Args()
train_data, test_data = getdata(args)
imgsize = (28, 28, 1)
nclasses = 10

#m = Chain(Dense(prod(imgsize), 32, relu), Dense(32, nclasses))
m = Chain(Dense(prod(imgsize), 24, relu), KitML.Shortcut(Chain(Dense(24, 24), Dense(24, 24))), Dense(24, nclasses))

train_data = args.device.(train_data)
test_data = args.device.(train_data)
m = args.device(m)

loss(x,y) = logitcrossentropy(m(x), y)
evalcb = () -> @show(loss_all(train_data, m))
opt = ADAM(args.η)

@epochs 5 Flux.train!(loss, params(m), train_data, opt, cb = evalcb)

accuracy(train_data, m)
accuracy(test_data, m)
