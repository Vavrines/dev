module Kinetic

implicit none

real(kind=8), parameter :: PI = 3.141592654d0
integer, parameter :: MNUM = 6 !number of normal moments
integer, parameter :: MTUM = 6 !number of tangential moments

contains

function conserve_prim_1d(gamma)

    real(kind=8), intent(in) :: gamma
    real(kind=8) :: conserve_prim_1d

    conserve_prim_1d = gamma**2

end

end