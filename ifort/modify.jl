module Foo

const COMPILER = "gfortran"

end

using FOO

Foo.COMPILER = "ifort"

@show Foo.COMPILER